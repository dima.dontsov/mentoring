$(function () {
    $('.delete-btn').click(function () {
        var form = $('.delete-form');
        var prefix_path = form.attr('action');
        form.attr('action', prefix_path + $(this).val());
    });
});
