package com.epam.mentoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportBettingApplication {
    public static void main(String[] args) {
        SpringApplication.run(SportBettingApplication.class, args);
    }
}
