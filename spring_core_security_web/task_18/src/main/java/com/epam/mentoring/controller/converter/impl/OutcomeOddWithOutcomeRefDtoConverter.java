package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.controller.dto.OutcomeOddWithOutcomeRefDto;
import com.epam.mentoring.service.util.DateUtils;
import com.epam.mentoring.controller.converter.ReversibleDtoConverter;
import com.epam.mentoring.controller.converter.ReversibleListDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OutcomeOddWithOutcomeRefDtoConverter implements ReversibleDtoConverter<OutcomeOdd, OutcomeOddWithOutcomeRefDto>, ReversibleListDtoConverter<OutcomeOdd, OutcomeOddWithOutcomeRefDto> {

    private DateUtils dateUtils;

    @Autowired
    public OutcomeOddWithOutcomeRefDtoConverter(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    @Override
    public OutcomeOdd reverseConvert(OutcomeOddWithOutcomeRefDto d) {
        OutcomeOdd odd = new OutcomeOdd();

        odd.setId(d.getId());
        odd.setValidFrom(dateUtils.asLocalDateTime(d.getValidFrom()));
        odd.setValidTo(dateUtils.asLocalDateTime(d.getValidTo()));
        odd.setValue(d.getValue());

        Outcome outcome = new Outcome();
        outcome.setId(d.getOutcomeId());
        outcome.addOdd(odd);

        return odd;
    }

    @Override
    public OutcomeOddWithOutcomeRefDto convert(OutcomeOdd e) {
        OutcomeOddWithOutcomeRefDto oddApiDto = new OutcomeOddWithOutcomeRefDto();

        oddApiDto.setId(e.getId());
        oddApiDto.setValue(e.getValue());
        oddApiDto.setValidFrom(dateUtils.asDate(e.getValidFrom()));
        oddApiDto.setValidTo(dateUtils.asDate(e.getValidTo()));
        oddApiDto.setOutcomeId(e.getOutcome().getId());

        return oddApiDto;
    }

    @Override
    public List<OutcomeOdd> reverseConvert(List<OutcomeOddWithOutcomeRefDto> ds) {
        return ds.stream().map(this::reverseConvert).collect(Collectors.toList());
    }

    @Override
    public List<OutcomeOddWithOutcomeRefDto> convert(List<OutcomeOdd> es) {
        return es.stream().map(this::convert).collect(Collectors.toList());
    }
}
