package com.epam.mentoring.api.controller;

import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.controller.dto.OutcomeOddWithOutcomeRefDto;
import com.epam.mentoring.service.OutcomeOddService;
import com.epam.mentoring.controller.converter.impl.OutcomeOddWithOutcomeRefDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/odd", produces = "application/json")
public class OutcomeOddApiController {

    private OutcomeOddService outcomeOddService;
    private OutcomeOddWithOutcomeRefDtoConverter oddApiDtoConverter;

    @Autowired
    public OutcomeOddApiController(OutcomeOddService outcomeOddService, OutcomeOddWithOutcomeRefDtoConverter oddApiDtoConverter) {
        this.outcomeOddService = outcomeOddService;
        this.oddApiDtoConverter = oddApiDtoConverter;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public OutcomeOddWithOutcomeRefDto postOutcomeOdd(@Valid @RequestBody OutcomeOddWithOutcomeRefDto outcomeOddWithOutcomeRefDto) {
        OutcomeOdd outcomeOdd = oddApiDtoConverter.reverseConvert(outcomeOddWithOutcomeRefDto);
        OutcomeOdd saved = outcomeOddService.save(outcomeOdd);
        return oddApiDtoConverter.convert(saved);
    }

    @GetMapping("/{id}")
    public OutcomeOddWithOutcomeRefDto outcomeOddById(@PathVariable("id") Long id) {
        Optional<OutcomeOdd> optOutcomeOdd = outcomeOddService.findById(id);
        return optOutcomeOdd.map(outcomeOdd -> oddApiDtoConverter.convert(outcomeOdd)).orElse(null);
    }
}
