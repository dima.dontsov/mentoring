package com.epam.mentoring.api.controller.authentication.dto;

import java.io.Serializable;

public class AuthenticationRequestDto implements Serializable {
    private String username;
    private String password;

    public AuthenticationRequestDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public AuthenticationRequestDto() {
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}