package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.controller.converter.ReversibleDtoConverter;
import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.controller.dto.OutcomeOddDto;
import com.epam.mentoring.service.util.DateUtils;
import com.epam.mentoring.controller.converter.ReversibleListDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OutcomeOddDtoConverter implements ReversibleDtoConverter<OutcomeOdd, OutcomeOddDto>, ReversibleListDtoConverter<OutcomeOdd, OutcomeOddDto> {

    private DateUtils dateUtils;

    @Autowired
    public OutcomeOddDtoConverter(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    @Override
    public OutcomeOdd reverseConvert(OutcomeOddDto d) {
        OutcomeOdd outcomeOdd = new OutcomeOdd();

        outcomeOdd.setId(d.getId());
        outcomeOdd.setValidFrom(dateUtils.asLocalDateTime(d.getValidFrom()));
        outcomeOdd.setValidTo(dateUtils.asLocalDateTime(d.getValidTo()));
        outcomeOdd.setValue(d.getValue());

        return outcomeOdd;
    }

    @Override
    public OutcomeOddDto convert(OutcomeOdd e) {
        OutcomeOddDto dto = new OutcomeOddDto();

        dto.setId(e.getId());
        dto.setValue(e.getValue());
        dto.setValidFrom(dateUtils.asDate(e.getValidFrom()));
        dto.setValidTo(dateUtils.asDate(e.getValidTo()));

        return dto;
    }

    @Override
    public List<OutcomeOdd> reverseConvert(List<OutcomeOddDto> ds) {
        return ds.stream().map(this::reverseConvert).collect(Collectors.toList());
    }

    @Override
    public List<OutcomeOddDto> convert(List<OutcomeOdd> es) {
        return es.stream().map(this::convert).collect(Collectors.toList());
    }
}
