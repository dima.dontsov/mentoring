package com.epam.mentoring.repository;

import com.epam.mentoring.domain.sportevent.SportEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SportEventRepository extends CrudRepository<SportEvent, Long> {
    List<SportEvent> findAll();
    Optional<SportEvent> findByBetsId(long id);
}
