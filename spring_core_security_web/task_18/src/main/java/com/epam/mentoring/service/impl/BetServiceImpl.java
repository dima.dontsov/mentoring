package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.bet.Bet;
import com.epam.mentoring.repository.BetRepository;
import com.epam.mentoring.service.BetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BetServiceImpl implements BetService {

    private BetRepository betRepository;

    @Autowired
    public BetServiceImpl(BetRepository betRepository) {
        this.betRepository = betRepository;
    }

    @Override
    public Optional<Bet> getById(long id) {
        return betRepository.findById(id);
    }
}
