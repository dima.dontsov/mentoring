package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.domain.bet.Bet;
import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.domain.sportevent.SportEventType;
import com.epam.mentoring.controller.dto.SportEventDto;
import com.epam.mentoring.service.util.DateUtils;
import com.epam.mentoring.controller.converter.ReversibleDtoConverter;
import com.epam.mentoring.controller.converter.ReversibleListDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SportEventDtoConverter implements ReversibleDtoConverter<SportEvent, SportEventDto>, ReversibleListDtoConverter<SportEvent, SportEventDto> {

    private BetDtoConverter betDtoConverter;
    private DateUtils dateUtils;

    @Autowired
    public SportEventDtoConverter(BetDtoConverter betDtoConverter, DateUtils dateUtils) {
        this.betDtoConverter = betDtoConverter;
        this.dateUtils = dateUtils;
    }

    @Override
    public SportEvent reverseConvert(SportEventDto d) {
        SportEvent sportEvent = new SportEvent();

        sportEvent.setId(d.getId());
        sportEvent.setTitle(d.getEventTitle());
        sportEvent.setStartDate(dateUtils.asLocalDateTime(d.getStartDate()));
        sportEvent.setEndDate(dateUtils.asLocalDateTime(d.getEndDate()));
        sportEvent.setType(SportEventType.valueOf(d.getEventType()));

        List<Bet> betsToAdd = betDtoConverter.reverseConvert(d.getBets());
        betsToAdd.forEach(sportEvent::addBet);

        return sportEvent;
    }

    @Override
    public SportEventDto convert(SportEvent e) {
        SportEventDto dto = new SportEventDto();

        dto.setId(e.getId());
        dto.setEventTitle(e.getTitle());
        dto.setStartDate(dateUtils.asDate(e.getStartDate()));
        dto.setEndDate(dateUtils.asDate(e.getEndDate()));
        dto.setEventType(e.getType().toString());
        dto.setBets(betDtoConverter.convert(e.getBets()));

        return dto;
    }

    @Override
    public List<SportEvent> reverseConvert(List<SportEventDto> ds) {
        return ds.stream().map(this::reverseConvert).collect(Collectors.toList());
    }

    @Override
    public List<SportEventDto> convert(List<SportEvent> es) {
        return es.stream().map(this::convert).collect(Collectors.toList());
    }
}
