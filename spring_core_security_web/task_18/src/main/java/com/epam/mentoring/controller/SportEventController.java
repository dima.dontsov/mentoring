package com.epam.mentoring.controller;

import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.controller.dto.SportEventDto;
import com.epam.mentoring.service.SportEventService;
import com.epam.mentoring.controller.converter.impl.SportEventDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Optional;

@Controller
public class SportEventController {

    private SportEventService sportEventService;

    private SportEventDtoConverter sportEventDtoConverter;

    @Autowired
    public SportEventController(SportEventService eventService, SportEventDtoConverter sportEventDtoConverter) {
        this.sportEventService = eventService;
        this.sportEventDtoConverter = sportEventDtoConverter;
    }

    @GetMapping(path = "/events")
    public String toEventsPage(Model m) {
        List<SportEvent> sportEvents = sportEventService.getEvents();
        List<SportEventDto> eventDto = sportEventDtoConverter.convert(sportEvents);

        m.addAttribute("events", eventDto);

        return "events";
    }

    @PostMapping(path = "/event/{id}/bets")
    public String toEventBets(@PathVariable long id, Model m) {

        Optional<SportEvent> sportEvent = sportEventService.getById(id);
        SportEventDto sportEventDto;
        if (sportEvent.isPresent()) {
            sportEventDto = sportEventDtoConverter.convert(sportEvent.get());
        } else {
            sportEventDto = new SportEventDto();
        }
        m.addAttribute("event", sportEventDto);

        return "bets";
    }
}
