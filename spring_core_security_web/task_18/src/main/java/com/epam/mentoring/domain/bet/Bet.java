package com.epam.mentoring.domain.bet;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.sportevent.SportEvent;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "bet")
public class Bet {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private BetType type;

    @ManyToOne
    @JoinColumn(name = "sport_event_id")
    private SportEvent sportEvent;

    @OneToMany(mappedBy = "bet", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Outcome> outcomes = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BetType getType() {
        return type;
    }

    public void setType(BetType type) {
        this.type = type;
    }

    public SportEvent getSportEvent() {
        return sportEvent;
    }

    public void setSportEvent(SportEvent sportEvent) {
        this.sportEvent = sportEvent;
    }

    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    public void addOutcome(Outcome o) {
        if (Objects.isNull(o)) {
            throw new NullPointerException("Attempt to add a nullable outcome");
        }
        if (Objects.nonNull(o.getBet())) {
            throw new IllegalStateException("Attempt to add an outcome that already has a related sport event");
        }

        outcomes.add(o);
        o.setBet(this);
    }
}
