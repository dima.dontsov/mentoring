package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.repository.OutcomeOddRepository;
import com.epam.mentoring.service.OutcomeOddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OutcomeOddServiceImpl implements OutcomeOddService {

    private OutcomeOddRepository outcomeOddRepository;

    @Autowired
    public OutcomeOddServiceImpl(OutcomeOddRepository outcomeOddRepository) {
        this.outcomeOddRepository = outcomeOddRepository;
    }

    @Override
    public Optional<OutcomeOdd> getById(long id) {
        return Optional.empty();
    }

    @Override
    public OutcomeOdd save(OutcomeOdd outcomeOdd) {
        return outcomeOddRepository.save(outcomeOdd);
    }

    @Override
    public Optional<OutcomeOdd> findById(Long id) {
        return outcomeOddRepository.findById(id);
    }
}
