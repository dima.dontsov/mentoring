package com.epam.mentoring.service.util;

import java.math.BigDecimal;

public interface CalculationUtils {
    BigDecimal subtract(BigDecimal init, BigDecimal subtract);
    BigDecimal add(BigDecimal init, BigDecimal add);
    BigDecimal multiply(BigDecimal val, Double multiplicand);
}
