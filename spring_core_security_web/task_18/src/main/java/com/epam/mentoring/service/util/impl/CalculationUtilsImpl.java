package com.epam.mentoring.service.util.impl;

import com.epam.mentoring.service.util.CalculationUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CalculationUtilsImpl implements CalculationUtils {

    @Override
    public BigDecimal subtract(BigDecimal init, BigDecimal subtract) {
        return init.subtract(subtract);
    }

    @Override
    public BigDecimal add(BigDecimal init, BigDecimal add) {
        return init.add(add);
    }

    @Override
    public BigDecimal multiply(BigDecimal val, Double multiplicand) {
        return val.multiply(new BigDecimal(multiplicand));
    }
}
