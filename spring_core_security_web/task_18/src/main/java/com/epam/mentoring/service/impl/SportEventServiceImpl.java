package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.repository.SportEventRepository;
import com.epam.mentoring.service.SportEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class SportEventServiceImpl implements SportEventService {

    private SportEventRepository sportEventRepository;

    @Autowired
    public SportEventServiceImpl(SportEventRepository sportEventRepository) {
        this.sportEventRepository = sportEventRepository;
    }

    @Override
    public List<SportEvent> getEvents() {
        List<SportEvent> wagers = sportEventRepository.findAll();
        if (Objects.isNull(wagers)) {
            return new ArrayList<>();
        }

        return wagers;
    }

    @Override
    public Optional<SportEvent> getById(long id) {
        return sportEventRepository.findById(id);
    }

    @Override
    public Optional<SportEvent> getByBetId(long id) {
        return sportEventRepository.findByBetsId(id);
    }

    @Override
    public SportEvent save(SportEvent sportEvent) {
        return sportEventRepository.save(sportEvent);
    }
}
