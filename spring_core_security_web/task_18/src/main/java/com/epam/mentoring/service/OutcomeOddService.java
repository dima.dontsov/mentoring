package com.epam.mentoring.service;

import com.epam.mentoring.domain.OutcomeOdd;

import java.util.Optional;

public interface OutcomeOddService {
    Optional<OutcomeOdd> getById(long id);
    OutcomeOdd save(OutcomeOdd outcomeOdd);

    Optional<OutcomeOdd> findById(Long id);
}
