package com.epam.mentoring.controller.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import javax.validation.constraints.*;

public class OutcomeOddWithOutcomeRefDto {

    @Null(message = "You can not change or specify id of persistent entity")
    private Long id;

    @PositiveOrZero(message = "odd value have to be a positive number")
    private double value;

    @FutureOrPresent
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date validFrom;

    @Future
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date validTo;

    @NotNull
    @PositiveOrZero
    private Long outcomeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public long getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(long outcomeId) {
        this.outcomeId = outcomeId;
    }
}
