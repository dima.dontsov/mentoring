package com.epam.mentoring.api.controller.authentication.dto;

public class AuthenticationTokenDto {
    private String token;

    public AuthenticationTokenDto(String token) {
        this.token = token;
    }

    public AuthenticationTokenDto() {
    }

    public String getToken() {
        return token;
    }
}
