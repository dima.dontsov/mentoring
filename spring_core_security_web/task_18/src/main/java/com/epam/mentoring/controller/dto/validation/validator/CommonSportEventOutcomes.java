package com.epam.mentoring.controller.dto.validation.validator;

import com.epam.mentoring.controller.dto.validation.annotation.AllOutcomesRelatesToSameSportEvent;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CommonSportEventOutcomes implements ConstraintValidator<AllOutcomesRelatesToSameSportEvent, Object> {
   public void initialize(AllOutcomesRelatesToSameSportEvent constraint) {
   }

   public boolean isValid(Object obj, ConstraintValidatorContext context) {
      //TODO how to get if they are relate to same sport event?
      return true;
   }
}
