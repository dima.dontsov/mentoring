package com.epam.mentoring.domain;

import com.epam.mentoring.domain.bet.Bet;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "outcome")
public class Outcome {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String value;

    @ManyToOne
    @JoinColumn(name = "bet_id")
    private Bet bet;

    @OneToMany(mappedBy = "outcome", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OutcomeOdd> outcomeOdds = new ArrayList<>();

    public String getValue() {
        return value;
    }

    public Bet getBet() {
        return bet;
    }

    public List<OutcomeOdd> getOutcomeOdds() {
        return outcomeOdds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    public void addOdd(OutcomeOdd o) {
        if (Objects.isNull(o)) {
            throw new NullPointerException("Attempt to add a nullable odd");
        }
        if (Objects.nonNull(o.getOutcome())) {
            throw new IllegalStateException("Attempt to add an odd that already has a related outcome");
        }

        outcomeOdds.add(o);
        o.setOutcome(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Outcome outcome = (Outcome) o;
        return Objects.equals(id, outcome.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
