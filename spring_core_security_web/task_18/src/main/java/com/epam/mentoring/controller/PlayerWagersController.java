package com.epam.mentoring.controller;

import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.bet.Bet;
import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.controller.dto.*;
import com.epam.mentoring.controller.exception.UserNotFoundException;
import com.epam.mentoring.service.BetService;
import com.epam.mentoring.service.SportEventService;
import com.epam.mentoring.service.UserService;
import com.epam.mentoring.service.WagerService;
import com.epam.mentoring.controller.converter.impl.BetDtoConverter;
import com.epam.mentoring.controller.converter.impl.SportEventDtoConverter;
import com.epam.mentoring.controller.converter.impl.UserDtoConverter;
import com.epam.mentoring.controller.converter.impl.WagerDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class PlayerWagersController {

    private WagerService wagerService;
    private UserService userService;
    private BetService betService;
    private SportEventService sportEventService;

    private WagerDtoConverter wagerDtoConverter;
    private UserDtoConverter userDtoConverter;
    private BetDtoConverter betDtoConverter;
    private SportEventDtoConverter sportEventDtoConverter;

    @Autowired
    public PlayerWagersController(WagerService wagerService, UserService userService, BetService betService, SportEventService sportEventService, WagerDtoConverter wagerDtoConverter, UserDtoConverter userDtoConverter, BetDtoConverter betDtoConverter, SportEventDtoConverter sportEventDtoConverter) {
        this.wagerService = wagerService;
        this.userService = userService;
        this.betService = betService;
        this.sportEventService = sportEventService;
        this.wagerDtoConverter = wagerDtoConverter;
        this.userDtoConverter = userDtoConverter;
        this.betDtoConverter = betDtoConverter;
        this.sportEventDtoConverter = sportEventDtoConverter;
    }

    @GetMapping(path = "/wagers")
    public String toPlayerWagersPage(Model m, Authentication auth) {

        //TODO move to filter or aop
        String userName = ((UserDetails) auth.getPrincipal()).getUsername();
        User user = userService.getByName(userName);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException();
        }

        List<Wager> wagers = wagerService.getPlayerWagers(user);
        List<WagerDto> wagersDto = wagerDtoConverter.convert(wagers);

        m.addAttribute("wagers", wagersDto);

        return "wagers";
    }

    @PostMapping(path = "/wagers/delete/{id}")
    public String deleteWager(@PathVariable long id) {

        Optional<Wager> wager = wagerService.getById(id);
        wager.ifPresent(w -> wagerService.delete(w));

        return "redirect:/wagers";
    }

    @GetMapping(path = {"/wagers/create"})
    public String toCreateWagerPage(@RequestParam(value = "bet_id") String betId, Model model, Authentication auth) {

        String userName = ((UserDetails) auth.getPrincipal()).getUsername();
        User user = userService.getByName(userName);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException();
        }


        Optional<Bet> bet = betService.getById(Long.parseLong(betId));
        BetDto betDto;
        List<OutcomeOddDto> outcomeOddDtos;
        if (bet.isPresent()) {
            betDto = betDtoConverter.convert(bet.get());
            outcomeOddDtos = betDto.getOutcomes()
                .stream()
                .flatMap(t -> t.getOutcomeOdds().stream())
                .collect(Collectors.toList());


        } else {
            betDto = new BetDto();
            outcomeOddDtos = new ArrayList<>();
        }


        Optional<SportEvent> sportEvent = sportEventService.getByBetId(Long.parseLong(betId));
        SportEventDto sportEventDto;
        if (sportEvent.isPresent()) {
            sportEventDto = sportEventDtoConverter.convert(sportEvent.get());
        } else {
            sportEventDto = new SportEventDto();
        }

        model.addAttribute("bet", betDto);
        model.addAttribute("sportEvent", sportEventDto);
        model.addAttribute("wager", new WagerDto());
        model.addAttribute("outcomeOdd", outcomeOddDtos);

        UserDto userDto = userDtoConverter.convert(user);
        model.addAttribute("user", userDto);

        return "new_wager";
    }

    @PostMapping(value = "/wagers/create")
    public String makeWager(@Valid @ModelAttribute(name = "wager") WagerDto dto, Authentication auth) {

        //TODO move to filter or aop
        String userName = ((UserDetails) auth.getPrincipal()).getUsername();
        User user = userService.getByName(userName);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException();
        }

        Wager w = new Wager();
        w.setAmount(BigDecimal.valueOf(dto.getAmount()));
        w.setCurrency(user.getCurrency());
        w.setPlayer(user);
        w.setTimestamp(LocalDateTime.now());

        OutcomeOdd o = new OutcomeOdd();
        o.setId(dto.getOutcomeOdd().getId());
        w.setOutcomeOdd(o);

        wagerService.create(w);

        return "redirect:/wagers";
    }
}
