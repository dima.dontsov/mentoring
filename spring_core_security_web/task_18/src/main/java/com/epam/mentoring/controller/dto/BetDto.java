package com.epam.mentoring.controller.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

public class BetDto {

    @Null(message = "You can not change or specify id of persistent entity")
    private Long id;

    @Pattern(regexp = "^(WINNER|GOALS|SCORES)")
    private String betType;

    @NotBlank
    private String betDescription;

    private List<OutcomeDto> outcomes = new ArrayList<>();

    public List<OutcomeDto> getOutcomes() {
        return outcomes;
    }

    public void setOutcomeOdds(List<OutcomeDto> outcomeOdds) {
        this.outcomes = outcomeOdds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBetType() {
        return betType;
    }

    public void setBetType(String betType) {
        this.betType = betType;
    }

    public String getBetDescription() {
        return betDescription;
    }

    public void setBetDescription(String betDescription) {
        this.betDescription = betDescription;
    }
}
