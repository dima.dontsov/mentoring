package com.epam.mentoring.repository;

import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WagerRepository extends CrudRepository<Wager, Long> {
    List<Wager> findByPlayer(User player);
    List<Wager> findByOutcomeOdd(OutcomeOdd outcomeOdd);
}
