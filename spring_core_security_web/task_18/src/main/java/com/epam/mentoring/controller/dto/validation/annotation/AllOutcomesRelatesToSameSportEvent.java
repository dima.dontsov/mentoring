package com.epam.mentoring.controller.dto.validation.annotation;

import com.epam.mentoring.controller.dto.validation.validator.CommonSportEventOutcomes;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = CommonSportEventOutcomes.class)
@Documented
public @interface AllOutcomesRelatesToSameSportEvent {
    String message() default "Result Outcomes does not relate to same sport event";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

