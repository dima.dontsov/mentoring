package com.epam.mentoring.configuration;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.bet.Bet;
import com.epam.mentoring.domain.bet.BetType;
import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.domain.sportevent.SportEventType;
import com.epam.mentoring.domain.user.Currency;
import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.repository.SportEventRepository;
import com.epam.mentoring.repository.UserRepository;
import com.epam.mentoring.repository.WagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Configuration
public class DemoDataConfiguration {

    private UserRepository userRepository;
    private SportEventRepository eventRepository;
    private WagerRepository wagerRepository;

    @Autowired
    public DemoDataConfiguration(UserRepository userRepository, SportEventRepository eventRepository, WagerRepository wagerRepository) {
        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
        this.wagerRepository = wagerRepository;
    }

    @Bean
    CommandLineRunner initDatabase() {
        return args -> fillDatabaseWithDemoData();
    }

    private void fillDatabaseWithDemoData() {
        User player = createDemoPlayer("d");
        SportEvent event = createDemoSportEventWithOneBetWithTwoOutcomesWithTwoOddsEach();

        Bet bet = event.getBets().get(0);
        Outcome outcome = bet.getOutcomes().get(0);

        List<OutcomeOdd> odds = outcome.getOutcomeOdds();

        createDemoPlayerWagers(player, odds);
        createDemoPlayer("f");
    }

    private void createDemoPlayerWagers(User player, List<OutcomeOdd> odds) {
        for (OutcomeOdd odd : odds) {
            Wager w = new Wager();
            w.setAmount(new BigDecimal(10));
            w.setCurrency(player.getCurrency());
            w.setPlayer(player);
            w.setProcessed(false);
            w.setWin(false);
            w.setTimestamp(LocalDateTime.now());
            w.setOutcomeOdd(odd);

            wagerRepository.save(w);
        }
    }

    private SportEvent createDemoSportEventWithOneBetWithTwoOutcomesWithTwoOddsEach() {
        SportEvent event = new SportEvent();
        event.setType(SportEventType.FOOTBALL);
        event.setTitle("Southampton v Bournemoth");
        event.setStartDate(LocalDateTime.of(2019, 10, 27, 19, 0));
        event.setEndDate(LocalDateTime.of(2019, 10, 27, 21, 0));

        Bet bet = createDemoWinnerBet();
        event.addBet(bet);

        return eventRepository.save(event);
    }

    private User createDemoPlayer(String id) {
        User demo = new User();

        demo.setName(id);
        demo.setPassword(id);
        demo.setDateOfBirth(LocalDate.of(1997, 1, 8));
        demo.setBalance(new BigDecimal(1000));
        demo.setCurrency(Currency.UAH);
        demo.setAccount(new Random().nextLong());

        return userRepository.save(demo);
    }

    private Bet createDemoWinnerBet() {
        Bet bet = new Bet();

        bet.setDescription("Super ball");
        bet.setType(BetType.WINNER);

        Outcome outcome1 = new Outcome();
        outcome1.setValue("Southampton");

        OutcomeOdd outcome1odd1 = new OutcomeOdd(4d,
                LocalDateTime.of(2019, 9, 27, 19, 0),
                LocalDateTime.of(2019, 9, 30, 18, 59));

        OutcomeOdd outcome1odd2 = new OutcomeOdd(5d,
                LocalDateTime.of(2019, 9, 30, 19, 0),
                LocalDateTime.of(2019, 10, 30, 19, 0));

        Outcome outcome2 = new Outcome();
        outcome2.setValue("Bournemoth");

        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.7,
                LocalDateTime.of(2019, 9, 27, 19, 0),
                LocalDateTime.of(2019, 9, 30, 18, 59));

        OutcomeOdd outcome2odd2 = new OutcomeOdd(1.5,
                LocalDateTime.of(2019, 9, 30, 19, 0),
                LocalDateTime.of(2019, 10, 30, 19, 0));

        outcome1.addOdd(outcome1odd1);
        outcome1.addOdd(outcome1odd2);

        outcome2.addOdd(outcome2odd1);
        outcome2.addOdd(outcome2odd2);

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);

        return bet;
    }
}
