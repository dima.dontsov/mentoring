package com.epam.mentoring.api.controller;

import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.controller.dto.UserDto;
import com.epam.mentoring.service.UserService;
import com.epam.mentoring.controller.converter.impl.UserDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/user", produces = "application/json")
public class UserApiController {

    private UserService userService;
    private UserDtoConverter userDtoConverter;

    @Autowired
    public UserApiController(UserService userService, UserDtoConverter userDtoConverter) {
        this.userService = userService;
        this.userDtoConverter = userDtoConverter;
    }

    @GetMapping("/{id}")
    public UserDto outcomeOddById(@PathVariable("id") Long id) {
        Optional<User> optUser = userService.getById(id);
        return optUser.map(user -> userDtoConverter.convert(user)).orElse(null);
    }
}
