package com.epam.mentoring.domain.user;

public enum Currency
{
    UAH, USD
}
