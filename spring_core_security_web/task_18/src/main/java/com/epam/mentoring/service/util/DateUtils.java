package com.epam.mentoring.service.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public interface DateUtils {
    Date asDate(LocalDate ld);

    LocalDate asLocalDate(Date d);

    Date asDate(LocalDateTime l);

    LocalDateTime asLocalDateTime(Date d);
}
