package com.epam.mentoring.api.controller;

import com.epam.mentoring.controller.converter.impl.ResultDtoConverter;
import com.epam.mentoring.controller.dto.ResultDto;
import com.epam.mentoring.domain.Result;
import com.epam.mentoring.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/result")
public class ResultApiController {

    private ResultService resultService;
    private ResultDtoConverter resultConverter;

    @Autowired
    public ResultApiController(ResultService resultService, ResultDtoConverter resultConverter) {
        this.resultService = resultService;
        this.resultConverter = resultConverter;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void saveResult(@RequestBody @Valid ResultDto dto) {
        Result result = resultConverter.reverseConvert(dto);
        resultService.process(result);
    }
}
