package com.epam.mentoring.service;

import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.user.User;

import java.util.Optional;

public interface UserService {
    User getByName(String name);

    User update(User user);

    Optional<User> getById(long accountNumber);

    void processPlayerWagerResult(User player, Wager wager);
}
