package com.epam.mentoring.controller.converter;

import java.util.List;

public interface ListDtoConverter<E, D> {
    List<D> convert(List<E> es);
}
