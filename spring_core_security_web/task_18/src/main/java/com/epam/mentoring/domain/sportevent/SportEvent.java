package com.epam.mentoring.domain.sportevent;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.bet.Bet;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "sport_event")
public class SportEvent {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private LocalDateTime startDate;

    @Column(nullable = false)
    private LocalDateTime endDate;

    @Column(nullable = false)
    private SportEventType type;

    @OneToMany(mappedBy = "sportEvent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Bet> bets = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public SportEventType getType() {
        return type;
    }

    public void setType(SportEventType type) {
        this.type = type;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void addBet(Bet b) {
        if (Objects.isNull(b)) {
            throw new NullPointerException("Attempt to add nullable bet");
        }
        if (Objects.nonNull(b.getSportEvent())) {
            throw new IllegalStateException("Attempt to add a bet that already has a related sport event");
        }

        bets.add(b);
        b.setSportEvent(this);
    }

    public List<Outcome> getOutcomes() {
        return bets.stream().map(Bet::getOutcomes).flatMap(Collection::stream).collect(Collectors.toList());
    }
}
