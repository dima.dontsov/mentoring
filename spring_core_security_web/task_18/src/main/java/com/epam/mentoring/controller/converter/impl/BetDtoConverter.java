package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.controller.converter.ReversibleDtoConverter;
import com.epam.mentoring.controller.converter.ReversibleListDtoConverter;
import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.bet.Bet;
import com.epam.mentoring.domain.bet.BetType;
import com.epam.mentoring.controller.dto.BetDto;
import com.epam.mentoring.controller.dto.OutcomeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BetDtoConverter implements ReversibleDtoConverter<Bet, BetDto>, ReversibleListDtoConverter<Bet, BetDto> {

    private OutcomeDtoConverter outcomeDtoConverter;

    @Autowired
    public BetDtoConverter(OutcomeDtoConverter outcomeDtoConverter) {
        this.outcomeDtoConverter = outcomeDtoConverter;
    }

    @Override
    public Bet reverseConvert(BetDto d) {
        Bet bet = new Bet();

        bet.setId(d.getId());
        bet.setDescription(d.getBetDescription());
        bet.setType(BetType.valueOf(d.getBetType()));

        List<Outcome> outcomes = outcomeDtoConverter.reverseConvert(d.getOutcomes());
        outcomes.forEach(bet::addOutcome);

        return bet;
    }

    @Override
    public BetDto convert(Bet e) {
        BetDto dto = new BetDto();

        dto.setId(e.getId());
        dto.setBetType(e.getType().toString());
        dto.setBetDescription(e.getDescription());

        List<OutcomeDto> outcomeDtos = outcomeDtoConverter.convert(e.getOutcomes());
        dto.setOutcomeOdds(outcomeDtos);

        return dto;
    }

    @Override
    public List<Bet> reverseConvert(List<BetDto> ds) {
        return ds.stream().map(this::reverseConvert).collect(Collectors.toList());
    }

    @Override
    public List<BetDto> convert(List<Bet> es) {
        return es.stream().map(this::convert).collect(Collectors.toList());
    }
}
