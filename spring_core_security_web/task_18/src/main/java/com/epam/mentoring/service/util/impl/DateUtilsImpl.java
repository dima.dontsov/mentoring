package com.epam.mentoring.service.util.impl;

import com.epam.mentoring.service.util.DateUtils;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DateUtilsImpl implements DateUtils {

    @Override
    public Date asDate(LocalDate ld) {
        return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    @Override
    public LocalDate asLocalDate(Date d) {
        return Instant.ofEpochMilli(d.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    @Override
    public Date asDate(LocalDateTime l) {
        return Date.from(l.atZone(ZoneId.systemDefault()).toInstant());
    }

    @Override
    public LocalDateTime asLocalDateTime(Date d) {
        return LocalDateTime.ofInstant(d.toInstant(), ZoneId.systemDefault());
    }
}
