package com.epam.mentoring.service.converter;

import com.epam.mentoring.domain.user.Currency;

import java.math.BigDecimal;

public interface CurrencyConverter {
    BigDecimal convert(BigDecimal amount, Currency from, Currency to);
}
