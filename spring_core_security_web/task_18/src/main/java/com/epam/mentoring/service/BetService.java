package com.epam.mentoring.service;

import com.epam.mentoring.domain.bet.Bet;

import java.util.Optional;

public interface BetService {
    Optional<Bet> getById(long id);
}
