package com.epam.mentoring.api.controller.authentication;

import com.epam.mentoring.api.controller.authentication.dto.AuthenticationRequestDto;
import com.epam.mentoring.api.controller.authentication.dto.AuthenticationTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/signin")
    public AuthenticationTokenDto signIn(@RequestBody AuthenticationRequestDto data) {
        try {

            String username = data.getUsername();
            String password = data.getPassword();

            Authentication upaToken = new UsernamePasswordAuthenticationToken(username, password);

            Authentication auth = authenticationManager.authenticate(upaToken);
            Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
            List<String> roles = authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

            String jwtToken = jwtTokenProvider.createToken(username, roles);

            return new AuthenticationTokenDto(jwtToken);

        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid credentials supplied", e);
        }
    }
}