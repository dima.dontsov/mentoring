package com.epam.mentoring.controller.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.*;

public class SportEventDto {

    @Null(message = "You can not change or specify id of persistent entity")
    private Long id;

    @NotBlank
    private String eventTitle;

    @FutureOrPresent
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date startDate;

    @Future
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date endDate;

    @Pattern(regexp="^(FOOTBALL|TENNIS)")
    private String eventType;

    private List<BetDto> bets = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public List<BetDto> getBets() {
        return bets;
    }

    public void setBets(List<BetDto> bets) {
        this.bets = bets;
    }
}
