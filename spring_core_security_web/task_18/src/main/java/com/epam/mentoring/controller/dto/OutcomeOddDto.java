package com.epam.mentoring.controller.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Null;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;

public class OutcomeOddDto {

    @Null(message = "You can not change or specify id of persistent entity")
    private Long id;

    @PositiveOrZero(message = "odd value have to be a positive number")
    private double value;

    @FutureOrPresent
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date validFrom;

    @Future
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date validTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }
}
