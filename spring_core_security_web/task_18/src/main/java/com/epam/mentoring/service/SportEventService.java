package com.epam.mentoring.service;

import com.epam.mentoring.domain.sportevent.SportEvent;

import java.util.List;
import java.util.Optional;

public interface SportEventService {
    List<SportEvent> getEvents();

    Optional<SportEvent> getById(long id);

    Optional<SportEvent> getByBetId(long id);

    SportEvent save(SportEvent sportEvent);

}
