package com.epam.mentoring.api.controller;

import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.controller.dto.SportEventDto;
import com.epam.mentoring.service.SportEventService;
import com.epam.mentoring.controller.converter.impl.SportEventDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/api/events", produces = "application/json")
public class SportEventApiController {

    private SportEventService sportEventService;
    private SportEventDtoConverter sportEventDtoConverter;

    @Autowired
    public SportEventApiController(SportEventService sportEventService, SportEventDtoConverter sportEventDtoConverter) {
        this.sportEventService = sportEventService;
        this.sportEventDtoConverter = sportEventDtoConverter;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public SportEventDto postSportEvent(@RequestBody @Valid SportEventDto sportEventDto) {
        SportEvent eventToSave = sportEventDtoConverter.reverseConvert(sportEventDto);
        SportEvent result = sportEventService.save(eventToSave);
        return sportEventDtoConverter.convert(result);
    }

    @GetMapping
    public List<SportEventDto> allSportEvents() {
        List<SportEvent> events = sportEventService.getEvents();
        return sportEventDtoConverter.convert(events);
    }
}
