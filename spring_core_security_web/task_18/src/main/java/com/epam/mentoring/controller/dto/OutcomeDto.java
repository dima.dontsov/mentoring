package com.epam.mentoring.controller.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;

public class OutcomeDto
{
    @Null(message = "You can not change or specify id of persistent entity")
    private Long id;

    @NotBlank
    private String value;

    private List<OutcomeOddDto> outcomeOdds = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<OutcomeOddDto> getOutcomeOdds() {
        return outcomeOdds;
    }

    public void setOutcomeOdds(List<OutcomeOddDto> outcomeOddDtos) {
        this.outcomeOdds = outcomeOddDtos;
    }
}
