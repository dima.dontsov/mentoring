package com.epam.mentoring.domain.sportevent;

public enum SportEventType
{
    FOOTBALL, TENNIS
}
