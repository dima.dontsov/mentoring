package com.epam.mentoring.service;

import com.epam.mentoring.domain.Result;

public interface ResultService {
    void process(Result result);
}
