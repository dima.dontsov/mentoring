package com.epam.mentoring.service.converter.impl;

import com.epam.mentoring.domain.user.Currency;
import com.epam.mentoring.service.converter.CurrencyConverter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CurrencyConverterImpl implements CurrencyConverter {

    private static final BigDecimal UAH_PER_USD_RATE = new BigDecimal(28);

    //TODO bad to use switch, but im just tired
    @Override
    public BigDecimal convert(BigDecimal amount, Currency from, Currency to) {
        //TODO use rate api and convertCurrency amount

        BigDecimal converted;

        switch (to) {
        case UAH:
            converted = convertToUAH(amount, from);
            break;
        case USD:
            converted = convertToUSD(amount, from);
            break;
        default:
            throw new UnsupportedOperationException();
        }

        return converted;
    }

    private BigDecimal convertToUSD(BigDecimal amount, Currency from) {
        switch (from) {
        case UAH:
            return amount.divide(UAH_PER_USD_RATE, BigDecimal.ROUND_HALF_UP);
        default:
            throw new UnsupportedOperationException();
        }
    }

    private BigDecimal convertToUAH(BigDecimal amount, Currency from) {
        switch (from) {
        case USD:
            return amount.multiply(UAH_PER_USD_RATE);
        default:
            throw new UnsupportedOperationException();
        }
    }
}
