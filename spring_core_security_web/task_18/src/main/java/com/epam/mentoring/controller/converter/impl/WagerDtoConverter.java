package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.bet.Bet;
import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.domain.user.Currency;
import com.epam.mentoring.controller.dto.OutcomeOddDto;
import com.epam.mentoring.controller.dto.WagerDto;
import com.epam.mentoring.service.util.DateUtils;
import com.epam.mentoring.controller.converter.ReversibleDtoConverter;
import com.epam.mentoring.controller.converter.ReversibleListDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class WagerDtoConverter implements ReversibleDtoConverter<Wager, WagerDto>, ReversibleListDtoConverter<Wager, WagerDto> {

    private OutcomeOddDtoConverter oddDtoConverter;
    private DateUtils dateUtils;

    @Autowired
    public WagerDtoConverter(OutcomeOddDtoConverter oddDtoConverter, DateUtils dateUtils) {
        this.oddDtoConverter = oddDtoConverter;
        this.dateUtils = dateUtils;
    }

    @Override
    public Wager reverseConvert(@NotNull WagerDto d) {
        Wager wager = new Wager();

        wager.setAmount(BigDecimal.valueOf(d.getAmount()));
        wager.setCurrency(Currency.valueOf(d.getCurrency()));
        wager.setProcessed(d.isProcessed());
        wager.setWin(d.isWin());

        OutcomeOdd odd = oddDtoConverter.reverseConvert(d.getOutcomeOdd());
        wager.setOutcomeOdd(odd);

        return wager;
    }

    @Override
    public WagerDto convert(Wager e) {
        WagerDto dto = new WagerDto();

        OutcomeOdd odd = e.getOutcomeOdd();
        Outcome outcome = odd.getOutcome();
        Bet bet = outcome.getBet();
        SportEvent event = bet.getSportEvent();

        dto.setId(e.getId());
        dto.setEventTitle(event.getTitle());
        dto.setStartDate(dateUtils.asDate(event.getStartDate()));
        dto.setEndDate(dateUtils.asDate(event.getStartDate()));
        dto.setEventType(event.getType().toString());
        dto.setBetType(bet.getType().toString());

        OutcomeOddDto oddDto = new OutcomeOddDto();
        oddDto.setValue(odd.getValue());

        dto.setOutcomeOdd(oddDto);

        dto.setAmount(e.getAmount().doubleValue());
        dto.setCurrency(e.getCurrency().toString());
        dto.setWin(e.getWin());
        dto.setProcessed(e.getProcessed());
        dto.setBetDescription(bet.getDescription());
        dto.setOutcomeValue(outcome.getValue());

        return dto;
    }

    @Override
    public List<Wager> reverseConvert(List<WagerDto> ds) {
        return ds.stream().map(this::reverseConvert).collect(Collectors.toList());
    }

    @Override
    public List<WagerDto> convert(List<Wager> es) {
        return es.stream().map(this::convert).collect(Collectors.toList());
    }
}
