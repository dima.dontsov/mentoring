package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.controller.converter.DtoConverter;
import com.epam.mentoring.controller.converter.ReversibleDtoConverter;
import com.epam.mentoring.controller.dto.ResultDto;
import com.epam.mentoring.domain.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ResultDtoConverter implements DtoConverter<Result, ResultDto>, ReversibleDtoConverter<Result, ResultDto> {

    private OutcomeDtoConverter outcomeDtoConverter;

    @Autowired
    public ResultDtoConverter(OutcomeDtoConverter outcomeDtoConverter) {
        this.outcomeDtoConverter = outcomeDtoConverter;
    }

    @Override
    public ResultDto convert(Result e) {
        ResultDto dto = new ResultDto();
        dto.setId(e.getId());
        dto.setOutcomes(outcomeDtoConverter.convert(e.getOutcomes()));
        return dto;
    }

    @Override
    public Result reverseConvert(ResultDto d) {
        Result result = new Result();
        result.setId(d.getId());
        result.setOutcomes(outcomeDtoConverter.reverseConvert(d.getOutcomes()));
        return result;
    }
}
