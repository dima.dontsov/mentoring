package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.Result;
import com.epam.mentoring.domain.sportevent.SportEvent;
import com.epam.mentoring.repository.ResultRepository;
import com.epam.mentoring.service.ResultService;
import com.epam.mentoring.service.SportEventService;
import com.epam.mentoring.service.WagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {

    private ResultRepository resultRepository;
    private WagerService wagerService;
    private SportEventService sportEventService;

    @Autowired
    public ResultServiceImpl(ResultRepository resultRepository, WagerService wagerService, SportEventService sportEventService) {
        this.resultRepository = resultRepository;
        this.wagerService = wagerService;
        this.sportEventService = sportEventService;
    }

    @Override
    @Transactional
    public void process(Result result) {
        this.resultRepository.save(result);

        List<Outcome> resultOutcomes = result.getOutcomes();

        SportEvent sportEvent = resultOutcomes.get(0).getBet().getSportEvent();

        List<Outcome> lostOutcomes = sportEvent.getOutcomes();
        lostOutcomes.removeAll(resultOutcomes);

        wagerService.processOutcomeWagersResults(lostOutcomes, false);
        wagerService.processOutcomeWagersResults(resultOutcomes, true);
    }
}
