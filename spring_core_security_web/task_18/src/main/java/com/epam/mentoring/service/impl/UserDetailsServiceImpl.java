package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.auththentication.UserDetailsImpl;
import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findUserByName(s);
        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException("no username with name " + s);
        }
        return new UserDetailsImpl(user);
    }
}
