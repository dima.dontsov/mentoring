package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.controller.converter.ListDtoConverter;
import com.epam.mentoring.controller.converter.ReversibleDtoConverter;
import com.epam.mentoring.controller.converter.ReversibleListDtoConverter;
import com.epam.mentoring.controller.dto.ResultDto;
import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.controller.dto.OutcomeDto;
import com.epam.mentoring.controller.dto.OutcomeOddDto;
import com.epam.mentoring.domain.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OutcomeDtoConverter implements ReversibleDtoConverter<Outcome, OutcomeDto>, ListDtoConverter<Outcome, OutcomeDto>, ReversibleListDtoConverter<Outcome, OutcomeDto> {

    private OutcomeOddDtoConverter oddDtoConverter;

    @Autowired
    public OutcomeDtoConverter(OutcomeOddDtoConverter oddDtoConverter) {
        this.oddDtoConverter = oddDtoConverter;
    }

    @Override
    public Outcome reverseConvert(OutcomeDto d) {
        Outcome outcome = new Outcome();

        outcome.setId(d.getId());
        outcome.setValue(d.getValue());

        List<OutcomeOdd> odds = oddDtoConverter.reverseConvert(d.getOutcomeOdds());
        odds.forEach(outcome::addOdd);

        return outcome;
    }

    @Override
    public OutcomeDto convert(Outcome e) {
        OutcomeDto outcomeDto = new OutcomeDto();

        outcomeDto.setId(e.getId());
        outcomeDto.setValue(e.getValue());

        List<OutcomeOddDto> oddDtos = oddDtoConverter.convert(e.getOutcomeOdds());
        outcomeDto.setOutcomeOdds(oddDtos);

        return outcomeDto;
    }

    @Override
    public List<Outcome> reverseConvert(List<OutcomeDto> ds) {
        return ds.stream().map(this::reverseConvert).collect(Collectors.toList());
    }

    @Override
    public List<OutcomeDto> convert(List<Outcome> outcomes) {
        return outcomes.stream().map(this::convert).collect(Collectors.toList());
    }
}
