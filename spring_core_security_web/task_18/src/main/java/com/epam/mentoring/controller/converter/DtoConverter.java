package com.epam.mentoring.controller.converter;

public interface DtoConverter<E, D> {
    D convert(E e);
}
