package com.epam.mentoring.service;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.user.User;

import java.util.List;
import java.util.Optional;

public interface WagerService {
    List<Wager> getPlayerWagers(User u);

    Optional<Wager> getById(Long id);

    List<Wager> getWithOdd(OutcomeOdd odd);

    boolean delete(Wager w);

    void create(Wager w);

    void processOutcomeWagersResults(List<Outcome> outcomes, boolean won);
}
