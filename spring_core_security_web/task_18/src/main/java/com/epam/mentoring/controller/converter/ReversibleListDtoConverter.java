package com.epam.mentoring.controller.converter;

import java.util.List;

public interface ReversibleListDtoConverter<E, D> extends ListDtoConverter<E, D> {
    List<E> reverseConvert(List<D> ds);
}
