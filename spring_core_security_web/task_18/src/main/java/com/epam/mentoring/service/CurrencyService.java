package com.epam.mentoring.service;

import com.epam.mentoring.domain.user.Currency;

import java.math.BigDecimal;
import java.util.List;

public interface CurrencyService {
    List<String> getAsStrings(Currency [] currencies);

    String getAsString(Currency currency);

    BigDecimal convertCurrency(BigDecimal amount, Currency from, Currency to);
}
