package com.epam.mentoring.controller.dto;

import com.epam.mentoring.controller.dto.validation.annotation.AllOutcomesRelatesToSameSportEvent;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

//TODO how to get if they are relate to same sport event?
@AllOutcomesRelatesToSameSportEvent
public class ResultDto {

    @NotNull
    @PositiveOrZero
    private long id;

    @NotNull
    @Size(min = 1)
    private List<@NotNull OutcomeDto> outcomes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public List<OutcomeDto> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(List<OutcomeDto> outcomes) {
        this.outcomes = outcomes;
    }
}
