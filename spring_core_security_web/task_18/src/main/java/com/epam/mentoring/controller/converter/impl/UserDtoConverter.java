package com.epam.mentoring.controller.converter.impl;

import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.controller.dto.UserDto;
import com.epam.mentoring.service.util.DateUtils;
import com.epam.mentoring.controller.converter.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class UserDtoConverter implements DtoConverter<User, UserDto> {

    private DateUtils dateUtils;

    @Autowired
    public UserDtoConverter(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    @Override
    public UserDto convert(@NotNull User e) {
        UserDto dto = new UserDto();

        dto.setAccountNumber(e.getId());
        dto.setName(e.getName());
        dto.setPassword(e.getPassword());
        dto.setDateOfBirth(dateUtils.asDate(e.getDateOfBirth()));
        dto.setCurrency(e.getCurrency().toString());
        dto.setBalance(e.getBalance().doubleValue());

        return dto;
    }
}
