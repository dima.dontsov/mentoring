package com.epam.mentoring.repository;

import com.epam.mentoring.domain.OutcomeOdd;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OutcomeOddRepository extends CrudRepository<OutcomeOdd, Long> {

}
