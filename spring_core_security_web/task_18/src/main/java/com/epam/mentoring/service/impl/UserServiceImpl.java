package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.repository.UserRepository;
import com.epam.mentoring.service.UserService;
import com.epam.mentoring.service.util.CalculationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private CalculationUtils calculationUtils;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, CalculationUtils calculationUtils) {
        this.userRepository = userRepository;
        this.calculationUtils = calculationUtils;
    }

    @Override
    public User getByName(String name) {
        return userRepository.findUserByName(name);
    }

    @Override
    public User update(User u) {
        return userRepository.save(u);
    }

    @Override
    public Optional<User> getById(long accountNumber) {
        return userRepository.findById(accountNumber);
    }

    @Override
    public void processPlayerWagerResult(User player, Wager wager) {
        boolean win = wager.getWin();
        if(win){

            Double odd = wager.getOutcomeOdd().getValue();
            BigDecimal wagerAmount = wager.getAmount();
            BigDecimal winAmount = calculationUtils.multiply(wagerAmount, odd);

            BigDecimal initBalance = player.getBalance();
            BigDecimal afterWinAmount = calculationUtils.add(initBalance, winAmount);

            player.setBalance(afterWinAmount);
            userRepository.save(player);
        }
    }
}
