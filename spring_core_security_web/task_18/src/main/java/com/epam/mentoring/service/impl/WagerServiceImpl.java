package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.Outcome;
import com.epam.mentoring.domain.OutcomeOdd;
import com.epam.mentoring.domain.Wager;
import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.repository.WagerRepository;
import com.epam.mentoring.service.UserService;
import com.epam.mentoring.service.WagerService;
import com.epam.mentoring.service.util.CalculationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class WagerServiceImpl implements WagerService {

    private UserService userService;
    private WagerRepository wagerRepository;
    private CalculationUtils calcUtils;

    @Autowired
    public WagerServiceImpl(UserService userService, WagerRepository wagerRepository, CalculationUtils calcUtils) {
        this.userService = userService;
        this.wagerRepository = wagerRepository;
        this.calcUtils = calcUtils;
    }

    @Override
    public List<Wager> getPlayerWagers(User p) {
        List<Wager> wagers = wagerRepository.findByPlayer(p);
        if (Objects.isNull(wagers)) {
            return new ArrayList<>();
        }

        return wagers;
    }

    @Override
    public Optional<Wager> getById(Long id) {
        return wagerRepository.findById(id);
    }

    @Override
    public List<Wager> getWithOdd(OutcomeOdd odd) {
        return wagerRepository.findByOutcomeOdd(odd);
    }

    @Transactional
    @Override
    public boolean delete(Wager w) {
        if (isWagerEventHasStarted(w)) {
            return false;
        }

        wagerRepository.delete(w);
        User player = updateUserBalanceAfterWagerDeletion(w);
        userService.update(player);

        return true;
    }

    @Transactional
    @Override
    public void create(Wager w) {
        wagerRepository.save(w);
        User p = updateUserBalanceAfterWagerCreation(w);
        userService.update(p);
    }

    @Transactional
    @Override
    public void processOutcomeWagersResults(List<Outcome> outcomes, boolean won) {
        for (Outcome outcome : outcomes) {
            List<OutcomeOdd> odds = outcome.getOutcomeOdds();
            for (OutcomeOdd odd : odds) {
                List<Wager> oddWagers = getWithOdd(odd);
                for (Wager wager : oddWagers) {
                    wager.setProcessed(true);
                    wager.setWin(won);

                    User p = wager.getPlayer();
                    userService.processPlayerWagerResult(p, wager);
                }
                wagerRepository.saveAll(oddWagers);
            }
        }
    }

    //TODO does it belong here?
    private User updateUserBalanceAfterWagerDeletion(Wager w) {
        User player = w.getPlayer();
        BigDecimal before = player.getBalance();
        BigDecimal after = calcUtils.add(before, w.getAmount());
        player.setBalance(after);
        return player;
    }

    //TODO does it belong here?
    private User updateUserBalanceAfterWagerCreation(Wager w) {
        User player = w.getPlayer();
        BigDecimal before = player.getBalance();
        BigDecimal after = calcUtils.subtract(before, w.getAmount());
        player.setBalance(after);
        return player;
    }

    private boolean isWagerEventHasStarted(Wager w) {

        //TODO impressive chain isn't it?
        LocalDateTime start = w.getOutcomeOdd().getOutcome().getBet().getSportEvent().getStartDate();
        LocalDateTime now = LocalDateTime.now();

        return start.isBefore(now);
    }
}
