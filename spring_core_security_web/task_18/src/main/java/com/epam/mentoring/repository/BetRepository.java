package com.epam.mentoring.repository;

import com.epam.mentoring.domain.bet.Bet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BetRepository extends CrudRepository<Bet, Long> {
}
