package com.epam.mentoring.service.impl;

import com.epam.mentoring.domain.user.Currency;
import com.epam.mentoring.service.CurrencyService;
import com.epam.mentoring.service.converter.CurrencyConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private CurrencyConverter currencyConverter;

    @Autowired
    public CurrencyServiceImpl(CurrencyConverter currencyConverter) {
        this.currencyConverter = currencyConverter;
    }

    @Override
    public List<String> getAsStrings(Currency[] currencies) {
        return Stream.of(currencies).map(Enum::toString).collect(Collectors.toList());
    }

    @Override
    public String getAsString(Currency currency) {
        return currency.toString();
    }

    @Override
    public BigDecimal convertCurrency(BigDecimal amount, Currency from, Currency to) {
        return currencyConverter.convert(amount, from, to);
    }
}
