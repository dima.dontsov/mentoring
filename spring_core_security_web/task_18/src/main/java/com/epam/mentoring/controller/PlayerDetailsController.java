package com.epam.mentoring.controller;

import com.epam.mentoring.domain.user.Currency;
import com.epam.mentoring.domain.user.User;
import com.epam.mentoring.controller.dto.UserDto;
import com.epam.mentoring.controller.exception.UserNotFoundException;
import com.epam.mentoring.service.CurrencyService;
import com.epam.mentoring.service.UserService;
import com.epam.mentoring.service.util.DateUtils;
import com.epam.mentoring.controller.converter.impl.UserDtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Controller
public class PlayerDetailsController {

    private UserService userService;
    private CurrencyService currencyService;
    private UserDtoConverter userDtoConverter;
    private DateUtils dateUtils;

    @Autowired
    public PlayerDetailsController(UserService userService, CurrencyService currencyService, UserDtoConverter userDtoConverter, DateUtils dateUtils) {
        this.userService = userService;
        this.currencyService = currencyService;
        this.userDtoConverter = userDtoConverter;
        this.dateUtils = dateUtils;
    }

    @GetMapping(path = {"/", "/details"})
    public String toDetailsPage(Model model, Authentication auth) {

        String userName = ((UserDetails) auth.getPrincipal()).getUsername();
        User user = userService.getByName(userName);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException();
        }

        UserDto dto = userDtoConverter.convert(user);
        model.addAttribute("userDetails", dto);

        List<String> curs = currencyService.getAsStrings(Currency.values());
        model.addAttribute("currencies", curs);

        return "details";
    }

    @PostMapping(path = {"/", "/details"})
    public ModelAndView updateDetails(
        @Valid @ModelAttribute(name = "userDetails") UserDto dto, BindingResult br) {

        ModelAndView mav = new ModelAndView("details", "userDetails", dto);

        if (br.hasErrors()) {
            return mav;
        }

        User updUser = updateUserDetails(dto);
        if (Objects.isNull(updUser)) {
            br.reject("Failed to update user");
            return mav;
        }

        return new ModelAndView("redirect:/details");
    }

    private User updateUserDetails(UserDto dto) {

        Optional<User> user = userService.getById(dto.getAccountNumber());
        user.orElseThrow(UserNotFoundException::new);

        User updUser = user.get();
        updUser.setPassword(dto.getPassword());
        updUser.setDateOfBirth(dateUtils.asLocalDate(dto.getDateOfBirth()));
        updateCurrencyDetails(updUser, dto);

        return userService.update(updUser);
    }

    private void updateCurrencyDetails(User user, UserDto dto) {
        Currency from = user.getCurrency();
        Currency to = Currency.valueOf(dto.getCurrency());

        if (!from.equals(to)) {
            BigDecimal init = user.getBalance();
            BigDecimal converted = currencyService.convertCurrency(init, from, to);

            //TODO update user's bets amount and currency, I just can't stand it already...
            user.setCurrency(Currency.valueOf(dto.getCurrency()));
            user.setBalance(converted);
        }
    }
}
