package com.epam.mentoring.domain.bet;

public enum BetType
{
    WINNER("winner prediction -"),
    GOALS("number of scored goals will be"),
    SCORES("will score");

    private final String description;

    BetType(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return this.description;
    }
}
