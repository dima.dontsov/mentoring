package com.epam.mentoring.ui.playercreating.steps;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import domain.user.impl.Player;

public interface PlayerCreationStep {
    boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider);
}
