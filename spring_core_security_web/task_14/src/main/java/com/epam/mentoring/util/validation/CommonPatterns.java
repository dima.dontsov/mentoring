package com.epam.mentoring.util.validation;

import java.util.regex.Pattern;

public final class CommonPatterns {
    public static final Pattern ACCOUNT_NUMBER = Pattern.compile("([\\d]{4})-([\\d]{4})");
    public static final Pattern CURRENCY = Pattern.compile("[A-Za-z]{3}");
}
