package com.epam.mentoring.service.wager;

import com.epam.mentoring.controller.exception.InsufficientFundsException;
import domain.Wager;
import domain.user.impl.Player;

import java.util.List;

public interface WagerService {
    List<Wager> getPlayerWagers(Player player);

    void addWager(Wager wager) throws InsufficientFundsException;
}
