package com.epam.mentoring.util.localization.impl;

import com.epam.mentoring.util.localization.Localizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class LocalizerImpl implements Localizer {

    private Locale locale;
    private MessageSource ms;

    @Autowired
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Autowired
    public void setMs(MessageSource ms) {
        this.ms = ms;
    }

    @Override
    public String get(String key) {
        return ms.getMessage(key, null, locale);
    }

    @Override
    public String get(String key, Locale locale) {
        return ms.getMessage(key, null, locale);
    }
}
