package com.epam.mentoring.ui.gameflow.impl;

import com.epam.mentoring.service.GameService;
import com.epam.mentoring.ui.gameflow.GameController;
import com.epam.mentoring.ui.gameflow.steps.GameFlowStep;
import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.formatting.Formatter;
import com.epam.mentoring.util.localization.Localizer;
import domain.Wager;
import domain.user.impl.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class ConsoleGameController implements GameController {

    private GameService gameService;
    private MessageProvider messageProvider;
    private InfoCollector infoCollector;
    private List<GameFlowStep> bettingSteps;

    private Player player;

    private Formatter formatter;
    private Localizer localizer;

    @Autowired
    public ConsoleGameController(GameService gameService, MessageProvider messageProvider, InfoCollector infoCollector, List<GameFlowStep> bettingSteps, Player player, Formatter formatter) {
        this.gameService = gameService;
        this.messageProvider = messageProvider;
        this.infoCollector = infoCollector;
        this.bettingSteps = bettingSteps;
        this.player = player;
        this.formatter = formatter;
    }

    @Autowired
    public void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public void play() {
        AtomicBoolean done = new AtomicBoolean(false);

        do {
            processBettingSteps(player, done);
        } while (!isBettingDone(done));

        List<Wager> processedWagers = this.gameService.getProcessedWagers(player);
        showResult(player, processedWagers);
    }

    void processBettingSteps(Player p, AtomicBoolean done) {
        Iterator<GameFlowStep> i = bettingSteps.iterator();

        while (i.hasNext() && !isBettingDone(done)) {
            GameFlowStep s = i.next();
            processBettingStep(s, p, done);
        }
    }

    void processBettingStep(GameFlowStep s, Player p, AtomicBoolean done) {
        Boolean stepResult;

        do {
            stepResult = s.process(this.gameService, p, this.messageProvider, this.infoCollector);
        } while (!isPlayerQuit(stepResult) && !stepResult);

        done.set(isPlayerQuit(stepResult));
    }

    void showResult(Player player, List<Wager> playerWagers) {
        messageProvider.showMessage("\n" + localizer.get("betting.results") + "\n");

        playerWagers.forEach(w -> messageProvider.showMessage(getWagerResultingMessage(w)));

        messageProvider.showMessage("\n" + localizer.get("betting.final.balance.prefix") + formatter.formatUserBalance(player.getBalance()));
    }

    boolean isPlayerQuit(Boolean stepResult) {
        return Objects.isNull(stepResult);
    }

    String getWagerResultingMessage(Wager wager) {
        return (wager.getWin() ? localizer.get("betting.win.prefix") : localizer.get("betting.lose.prefix"))
            + wager.getOutcomeOdd().getFullRecursiveInfo();
    }

    boolean isBettingDone(AtomicBoolean isBettingDone) {
        return isBettingDone.get();
    }
}
