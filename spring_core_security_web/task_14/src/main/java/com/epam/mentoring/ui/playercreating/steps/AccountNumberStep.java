package com.epam.mentoring.ui.playercreating.steps;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.localization.Localizer;
import com.epam.mentoring.util.validation.CommonPatterns;
import com.epam.mentoring.util.validation.CommonValidator;
import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AccountNumberStep implements PlayerCreationStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountNumberStep.class);

    private Localizer localizer;

    @Autowired
    void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\n" + localizer.get("player.creation.enter.account.number"));
        String accNumStr = infoCollector.obtainStringValue();

        try {
            Preconditions.checkArgument(CommonValidator.isAccountNumberValid(accNumStr, CommonPatterns.ACCOUNT_NUMBER),
                localizer.get("player.creation.wrong.account.number"));

            playerToBuild.setAccountNumber(accNumStr);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            LOGGER.warn(ex.getMessage());
        }

        return stepSucceed;
    }
}
