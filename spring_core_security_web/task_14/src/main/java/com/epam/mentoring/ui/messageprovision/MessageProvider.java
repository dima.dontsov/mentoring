package com.epam.mentoring.ui.messageprovision;

public interface MessageProvider {
    void showMessage(String message);
    void showErrorMessage(String errorMessage);
}
