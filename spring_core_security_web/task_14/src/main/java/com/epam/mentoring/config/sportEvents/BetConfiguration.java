//package com.epam.mentoring.config.sportEvents;
//
//import domain.Bet;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class BetConfiguration {
//
//    @Autowired
//    private SportEventConfiguration sportEventConfiguration;
//
//    @Autowired
//    private OutcomeConfiguration outcomeConfiguration;
//
//    @Bean
//    public Bet footballPlayerScoresBetExample() {
//        Bet bet = new Bet("Messi", Bet.Type.SCORES, sportEventConfiguration.footballEvent());
//
//        bet.addOutcome(outcomeConfiguration.oneScoreFootballOutcome());
//        bet.addOutcome(outcomeConfiguration.zeroScoresFootballOutcome());
//        bet.addOutcome(outcomeConfiguration.moreThatTwoScoresFootballOutcome());
//
//        return bet;
//    }
//
//    @Bean
//    public Bet footballGoalsBet() {
//        Bet bet = new Bet("", Bet.Type.GOALS, sportEventConfiguration.footballEvent());
//
//        bet.addOutcome(outcomeConfiguration.zeroGoalsFootballOutcome());
//        bet.addOutcome(outcomeConfiguration.oneGoalFootballOutcome());
//        bet.addOutcome(outcomeConfiguration.moreThatTwoGoalsFootballOutcome());
//
//        return bet;
//    }
//
//    @Bean
//    public Bet footballWinnerBetExample() {
//        Bet bet = new Bet("", Bet.Type.WINNER, sportEventConfiguration.footballEvent());
//
//        bet.addOutcome(outcomeConfiguration.winnerFirstTeamFootballOutcome());
//        bet.addOutcome(outcomeConfiguration.winnerSecondTeamFootballOutcome());
//        bet.addOutcome(outcomeConfiguration.winnerDrawFootballOutcome());
//
//        return bet;
//    }
//
//    @Bean
//    public Bet tennisWinnerBetExample() {
//        Bet bet = new Bet("", Bet.Type.WINNER, sportEventConfiguration.tennisEvent());
//
//        bet.addOutcome(outcomeConfiguration.winnerFirstPlayerTennisOutcome());
//        bet.addOutcome(outcomeConfiguration.winnerSecondPlayerTennisOutcome());
//
//        return bet;
//    }
//}
