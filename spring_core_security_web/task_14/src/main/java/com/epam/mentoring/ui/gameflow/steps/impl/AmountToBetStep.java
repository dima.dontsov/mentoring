package com.epam.mentoring.ui.gameflow.steps.impl;

import com.epam.mentoring.controller.exception.InsufficientFundsException;
import com.epam.mentoring.service.GameService;
import com.epam.mentoring.ui.gameflow.steps.GameFlowStep;
import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.localization.Localizer;
import domain.user.impl.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.InputMismatchException;

public class AmountToBetStep implements GameFlowStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmountToBetStep.class);

    private Localizer localizer;

    @Autowired
    public void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public Boolean process(GameService gameService, Player player, MessageProvider messageProvider, InfoCollector infoCollector) {
        boolean result = false;

        messageProvider.showMessage(localizer.get("betting.how.much"));

        try {

            BigDecimal betAmount = infoCollector.obtainBigDecimalValue();
            gameService.placeBet(player, betAmount);

            result = true;

        } catch (InputMismatchException e) {
            LOGGER.warn(localizer.get("betting.invalid.bet.pattern"));
        } catch (InsufficientFundsException e) {
            LOGGER.warn(localizer.get("betting.not.enough.money"));
        }

        return result;
    }
}
