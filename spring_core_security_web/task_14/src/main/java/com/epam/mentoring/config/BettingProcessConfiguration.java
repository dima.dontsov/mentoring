package com.epam.mentoring.config;

import com.epam.mentoring.ui.gameflow.steps.GameFlowStep;
import com.epam.mentoring.ui.gameflow.steps.impl.AmountToBetStep;
import com.epam.mentoring.ui.gameflow.steps.impl.ChooseBetStep;
import com.epam.mentoring.ui.gameflow.steps.impl.ShowNewBalanceStep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
public class BettingProcessConfiguration {

    @Bean
    @Order(1)
    public GameFlowStep chooseBetStep() {
        return new ChooseBetStep();
    }

    @Bean
    @Order(2)
    public GameFlowStep amountToBetStep() {
        return new AmountToBetStep();
    }

    @Bean
    @Order(3)
    public GameFlowStep showNewBalanceStep() {
        return new ShowNewBalanceStep();
    }
}
