package com.epam.mentoring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;

@Configuration
public class FormatConfiguration {


    @Bean
    public DateTimeFormatter dateFormatter() {
        return DateTimeFormatter.ofPattern("yyyy.MM.dd");
    }


    @Bean
    public DecimalFormat decimalFormat() {
        return new DecimalFormat("#.###");
    }
}
