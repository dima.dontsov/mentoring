package com.epam.mentoring.config;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.ui.playercreating.PlayerBuilder;
import com.epam.mentoring.ui.playercreating.impl.ConsolePlayerBuilder;
import com.epam.mentoring.ui.playercreating.steps.*;
import com.epam.mentoring.util.annotation.ConditionalOnPlayerCreationNeed;
import domain.user.impl.Player;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
public class PlayerConfiguration {


    //region player
    @Bean
    @ConditionalOnPlayerCreationNeed
    public Player newPlayer(PlayerBuilder playerBuilder) {
        return playerBuilder.buildPlayer();
    }

    @Bean
    @ConditionalOnMissingBean
    public Player demoPlayer() {
        return Player.newDemoPlayer();
    }

    @Bean
    @ConditionalOnPlayerCreationNeed
    public PlayerBuilder playerBuilder(InfoCollector infoCollector, MessageProvider messageProvider) {
        return new ConsolePlayerBuilder(infoCollector, messageProvider);
    }
    //endregion

    //region player creation steps
    @Bean
    @Order(1)
    @ConditionalOnPlayerCreationNeed
    public PlayerCreationStep nameStep() {
        return new NameStep();
    }

    @Bean
    @Order(2)
    @ConditionalOnPlayerCreationNeed
    public PlayerCreationStep accountNumberStep() {
        return new AccountNumberStep();
    }

    @Bean
    @Order(3)
    @ConditionalOnPlayerCreationNeed
    public PlayerCreationStep passwordStep() {
        return new PasswordStep();
    }

    @Bean
    @Order(4)
    @ConditionalOnPlayerCreationNeed
    public PlayerCreationStep birthDateStep() {
        return new BirthDateStep();
    }

    @Bean
    @Order(5)
    @ConditionalOnPlayerCreationNeed
    public PlayerCreationStep currencyStep() {
        return new CurrencyStep();
    }

    @Bean
    @Order(6)
    @ConditionalOnPlayerCreationNeed
    public PlayerCreationStep initialMoneyAmountStep() {
        return new InitialMoneyAmountStep();
    }

    @Bean
    @Order(7)
    @ConditionalOnPlayerCreationNeed
    public PlayerCreationStep endStep() {
        return new EndStep();
    }
    //endregion
}
