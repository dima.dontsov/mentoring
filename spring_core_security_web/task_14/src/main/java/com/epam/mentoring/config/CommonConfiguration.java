package com.epam.mentoring.config;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.infocollecting.impl.ScannerBasedConsoleInfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.ui.messageprovision.impl.ConsoleMessageProvider;
import domain.outcome.OutcomeOdd;
import domain.user.impl.Player;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class CommonConfiguration {

    @Value("${locale}")
    private String locale;

    @Bean
    public Locale locale() {
        return new Locale(locale);
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }

    @Bean
    public InfoCollector infoCollector() {
        return new ScannerBasedConsoleInfoCollector(scanner());
    }

    @Bean
    public MessageProvider messageProvider() {
        return new ConsoleMessageProvider();
    }

    @Bean
    public Map<Player, OutcomeOdd> playerChosenOdd() {
        return new ConcurrentHashMap<>();
    }
}
