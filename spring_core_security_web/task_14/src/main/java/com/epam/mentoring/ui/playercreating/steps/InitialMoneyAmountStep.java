package com.epam.mentoring.ui.playercreating.steps;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.localization.Localizer;
import com.epam.mentoring.util.validation.CommonValidator;
import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.InputMismatchException;

public class InitialMoneyAmountStep implements PlayerCreationStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(InitialMoneyAmountStep.class);

    private Localizer localizer;

    @Autowired
    void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage(localizer.get("player.creation.enter.initial.amount"));

        try {
            BigDecimal initialAmount = infoCollector.obtainBigDecimalValue();

            Preconditions.checkArgument(CommonValidator.isInitialAmountValid(initialAmount),
                localizer.get("player.creation.invalid.amount"));

            playerToBuild.setBalance(initialAmount);
            stepSucceed = true;

        } catch (InputMismatchException ex) {
            LOGGER.warn(localizer.get("player.creation.invalid.money.pattern"));
        } catch (IllegalArgumentException ex) {
            LOGGER.warn(ex.getMessage());
        }

        return stepSucceed;
    }
}
