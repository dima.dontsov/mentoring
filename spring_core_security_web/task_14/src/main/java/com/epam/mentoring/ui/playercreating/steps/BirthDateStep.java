package com.epam.mentoring.ui.playercreating.steps;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.formatting.Formatter;
import com.epam.mentoring.util.localization.Localizer;
import com.epam.mentoring.util.validation.CommonValidator;
import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

public class BirthDateStep implements PlayerCreationStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(BirthDateStep.class);

    private Formatter formatter;

    private Localizer localizer;

    @Autowired
    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }

    @Autowired
    void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\n" + localizer.get("player.creation.enter.birth.date"));
        String birthDateStr = infoCollector.obtainStringValue();
        try {
            LocalDate birthDate = formatter.formatDate(birthDateStr);

            Preconditions.checkArgument(CommonValidator.isBirthDateValid(birthDate), localizer.get("player.creation.invalid.birth.date"));

            playerToBuild.setDateOfBirth(birthDate);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            LOGGER.warn(ex.getMessage());
        }

        return stepSucceed;
    }
}
