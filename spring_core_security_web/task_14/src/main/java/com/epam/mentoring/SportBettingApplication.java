package com.epam.mentoring;

import com.epam.mentoring.ui.gameflow.GameController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportBettingApplication implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(SportBettingApplication.class);

    private GameController gameController;

    @Autowired
    public SportBettingApplication(GameController gameController) {
        this.gameController = gameController;
    }

    public static void main(String[] args) {
        SpringApplication.run(SportBettingApplication.class);
    }

    @Override
    public void run(String... args) {
        LOGGER.error("Check error log");
        LOGGER.warn("Check warn log");
        LOGGER.info("Check info log");

        gameController.play();
    }
}
