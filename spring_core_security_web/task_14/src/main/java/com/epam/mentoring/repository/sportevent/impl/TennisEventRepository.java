package com.epam.mentoring.repository.sportevent.impl;

import com.epam.mentoring.repository.sportevent.SportEventRepository;
import domain.sportevent.TennisSportEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TennisEventRepository implements SportEventRepository<TennisSportEvent> {

    private List<TennisSportEvent> events;

    @Override
    public List<TennisSportEvent> getEvents() {
        return events;
    }

    @Autowired
    void setEvents(List<TennisSportEvent> events) {
        this.events = events;
    }
}
