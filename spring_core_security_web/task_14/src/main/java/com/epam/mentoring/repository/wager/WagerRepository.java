package com.epam.mentoring.repository.wager;

import domain.Wager;
import domain.user.impl.Player;

import java.util.List;

public interface WagerRepository {
    boolean add(Wager wager);
    List<Wager> getPlayerWagers(Player player);
}
