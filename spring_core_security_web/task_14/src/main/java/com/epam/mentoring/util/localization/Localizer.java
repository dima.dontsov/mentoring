package com.epam.mentoring.util.localization;

import java.util.Locale;

public interface Localizer {
    String get(String key);

    String get(String key, Locale locale);
}
