package com.epam.mentoring.ui.gameflow.steps.impl;

import com.epam.mentoring.service.GameService;
import com.epam.mentoring.ui.gameflow.steps.GameFlowStep;
import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.formatting.Formatter;
import com.epam.mentoring.util.localization.Localizer;
import domain.user.impl.Player;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Locale;

public class ShowNewBalanceStep implements GameFlowStep {

    private Formatter formatter;
    private Localizer localizer;

    @Autowired
    void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }

    @Autowired
    void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public Boolean process(GameService gameService, Player player, MessageProvider messageProvider, InfoCollector infoCollector) {
        BigDecimal playerBalance = gameService.getPlayerAvailableMoney(player);
        String formattedBalance = formatter.formatUserBalance(playerBalance);

        messageProvider.showMessage(localizer.get("betting.new.balance.prefix") + formattedBalance);

        return true;
    }
}
