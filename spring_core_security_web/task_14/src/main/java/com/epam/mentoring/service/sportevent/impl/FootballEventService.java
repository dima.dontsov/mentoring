package com.epam.mentoring.service.sportevent.impl;

import com.epam.mentoring.repository.sportevent.impl.FootballEventRepository;
import com.epam.mentoring.service.sportevent.SportEventService;
import domain.sportevent.FootballSportEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FootballEventService implements SportEventService<FootballSportEvent> {

    private FootballEventRepository footballEventRepository;

    @Autowired
    public FootballEventService(FootballEventRepository footballEventRepository) {
        this.footballEventRepository = footballEventRepository;
    }

    @Override
    public List<FootballSportEvent> getEvents() {
        return footballEventRepository.getEvents();
    }
}
