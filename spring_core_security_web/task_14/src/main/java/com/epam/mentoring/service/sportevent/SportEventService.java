package com.epam.mentoring.service.sportevent;

import domain.outcome.Outcome;
import domain.sportevent.SportEvent;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface SportEventService<T extends SportEvent> {

    List<T> getEvents();

    default List<Outcome> getOutcomes() {
        return getEvents().stream()
            .map(SportEvent::getOutcomes)
            .flatMap(Collection::stream).collect(Collectors.toList());
    }
}
