package com.epam.mentoring.ui.playercreating.impl;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.ui.playercreating.PlayerBuilder;
import com.epam.mentoring.ui.playercreating.steps.PlayerCreationStep;
import domain.user.impl.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

public class ConsolePlayerBuilder implements PlayerBuilder {
    private InfoCollector infoCollector;
    private MessageProvider messageProvider;
    private List<PlayerCreationStep> creationSteps;

    public ConsolePlayerBuilder(InfoCollector infoCollector, MessageProvider messageProvider) {
        this.infoCollector = infoCollector;
        this.messageProvider = messageProvider;
    }

    @Autowired
    public void setCreationSteps(List<PlayerCreationStep> creationSteps) {
        this.creationSteps = creationSteps;
    }

    @Override
    public Player buildPlayer() {
        Player p = new Player();
        creationSteps.forEach(step -> processPlayerCreationStep(step, p));
        return p;
    }

    void processPlayerCreationStep(PlayerCreationStep step, Player playerToBuild) {
        boolean stepSucceed;

        do {
            stepSucceed = step.process(playerToBuild, this.infoCollector, this.messageProvider);
        } while (!stepSucceed);
    }
}
