package com.epam.mentoring.service;

import com.epam.mentoring.controller.exception.InsufficientFundsException;
import com.epam.mentoring.service.sportevent.impl.FootballEventService;
import com.epam.mentoring.service.sportevent.impl.TennisEventService;
import com.epam.mentoring.service.wager.WagerService;
import domain.Result;
import domain.Wager;
import domain.outcome.Outcome;
import domain.outcome.OutcomeOdd;
import domain.sportevent.FootballSportEvent;
import domain.sportevent.SportEvent;
import domain.sportevent.TennisSportEvent;
import domain.user.impl.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class GameService {

    private WagerService wagerService;
    private FootballEventService footballEventService;
    private TennisEventService tennisEventService;
    private Map<Player, OutcomeOdd> playerChosenOddMap;

    @Autowired
    public GameService(WagerService wagerService, FootballEventService footballEventService, TennisEventService tennisEventService, Map<Player, OutcomeOdd> playerChosenOddMap) {
        this.wagerService = wagerService;
        this.footballEventService = footballEventService;
        this.tennisEventService = tennisEventService;
        this.playerChosenOddMap = playerChosenOddMap;
    }

    public void setPlayerChosenOdd(Player player, OutcomeOdd odd) {
        this.playerChosenOddMap.put(player, odd);
    }

    public BigDecimal getPlayerAvailableMoney(Player player) {
        return player.getBalance();
    }

    public List<Wager> getProcessedWagers(Player player) {
        generateResultsForEvents(getOutcomes());

        List<Result> results = getResults();

        List<Wager> playerWagers = wagerService.getPlayerWagers(player);

        List<OutcomeOdd> resultsOutcomeOdds = getResultsOutcomeOdds(results);
        playerWagers.forEach(w -> {
            if (resultsOutcomeOdds.contains(w.getOutcomeOdd())) {
                w.setWin(true);
                processWonBet(player, w);
            } else {
                w.setWin(false);
            }
            w.setProcessed(true);
        });

        return wagerService.getPlayerWagers(player);
    }

    public void placeBet(Player player, BigDecimal betAmount) throws InsufficientFundsException {
        OutcomeOdd odd = playerChosenOddMap.get(player);
        Wager wager = getPreparedWager(player, betAmount, odd);
        wagerService.addWager(wager);

        BigDecimal finalBalance = getAfterBetAmount(player.getBalance(), betAmount);
        player.setBalance(finalBalance);
    }

    Wager getPreparedWager(Player player, BigDecimal betAmount, OutcomeOdd outcomeOdd) {
        return Wager.newBuilder()
            .setPlayer(player)
            .setOutcomeOdd(outcomeOdd)
            .setAmount(betAmount)
            .setCurrency(player.getCurrency())
            .setTimestamp(LocalDateTime.now()).build();
    }

    public List<OutcomeOdd> getOutcomeOdds() {
        return getOutcomes().stream()
            .map(Outcome::getOdds)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    List<OutcomeOdd> getResultsOutcomeOdds(List<Result> results) {
        return results.stream()
            .map(Result::getOutcomes)
            .flatMap(Collection::stream)
            .map(Outcome::getOdds)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    List<Result> getResults() {
        return getEvents()
            .stream()
            .map(SportEvent::getResult)
            .collect(Collectors.toList());
    }

    void processWonBet(Player player, Wager wager) {
        BigDecimal winAmount = getWinAmount(wager);
        BigDecimal afterWinAmount = getAfterWinAmount(player.getBalance(), winAmount);
        player.setBalance(afterWinAmount);
    }

    BigDecimal getAfterWinAmount(BigDecimal initBalance, BigDecimal winAmount) {
        return initBalance.add(winAmount);
    }

    BigDecimal getAfterBetAmount(BigDecimal initBalance, BigDecimal betAmount) {
        return initBalance.subtract(betAmount);
    }

    BigDecimal getWinAmount(Wager wager) {
        return wager.getAmount().multiply(new BigDecimal(wager.getOutcomeOdd().getOdd()));
    }

    void generateResultsForEvents(List<Outcome> outcomes) {
        outcomes.forEach(o -> {
            boolean outcomePass = Math.random() < 0.5;
            if (outcomePass) {
                o.getBet().getSportEvent().getResult().getOutcomes().add(o);
            }
        });
    }

    private List<? extends SportEvent> getEvents() {
        List<SportEvent> result = new ArrayList<>();

        List<FootballSportEvent> fe = footballEventService.getEvents();
        List<TennisSportEvent> te = tennisEventService.getEvents();

        result.addAll(fe);
        result.addAll(te);

        return result;
    }

    private List<Outcome> getOutcomes() {
        List<Outcome> fo = footballEventService.getOutcomes();
        List<Outcome> to = tennisEventService.getOutcomes();

        return Stream.of(fo, to)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }
}
