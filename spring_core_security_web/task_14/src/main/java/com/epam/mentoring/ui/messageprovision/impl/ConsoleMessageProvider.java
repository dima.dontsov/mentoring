package com.epam.mentoring.ui.messageprovision.impl;

import com.epam.mentoring.ui.messageprovision.MessageProvider;
import org.springframework.stereotype.Component;

@Component
public class ConsoleMessageProvider implements MessageProvider {

    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        System.out.println(errorMessage);
    }
}
