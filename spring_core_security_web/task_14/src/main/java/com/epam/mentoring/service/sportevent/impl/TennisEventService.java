package com.epam.mentoring.service.sportevent.impl;

import com.epam.mentoring.repository.sportevent.impl.TennisEventRepository;
import com.epam.mentoring.service.sportevent.SportEventService;
import domain.sportevent.TennisSportEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TennisEventService implements SportEventService<TennisSportEvent> {

    private TennisEventRepository tennisEventRepository;

    @Autowired
    public TennisEventService(TennisEventRepository tennisEventRepository) {
        this.tennisEventRepository = tennisEventRepository;
    }

    @Override
    public List<TennisSportEvent> getEvents() {
        return this.tennisEventRepository.getEvents();
    }
}
