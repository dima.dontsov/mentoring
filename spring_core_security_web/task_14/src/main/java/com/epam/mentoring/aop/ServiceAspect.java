package com.epam.mentoring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Objects;

@Aspect
@Configuration
public class ServiceAspect {

    public static final Logger LOGGER = LoggerFactory.getLogger(ServiceAspect.class);

    @Pointcut("execution(public * com.epam.mentoring.service..*.*(..))")
    public void publicServiceMethods() {

    }

    @Around("publicServiceMethods()")
    public Object measureMethodExecutionTime(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        Object output = pjp.proceed();
        long elapsedTime = System.currentTimeMillis() - start;

        LOGGER.info(pjp.getSignature().getName() + " execution time: " + elapsedTime + " ms");

        return output;
    }

    @Before("publicServiceMethods()")
    public void logArguments(JoinPoint jp) {
        StringBuilder s = new StringBuilder();

        String methodName = jp.getSignature().getName();
        if (jp.getArgs().length < 1) {
            s.append("No arguments passed for ").append(methodName);
        } else {
            s.append("Arguments for ").append(methodName).append(":\n");
            Arrays.stream(jp.getArgs()).forEach(a -> s.append(a.getClass()).append(": ").append(a).append("\n"));
        }
        s.append("\n");

        LOGGER.info(s.toString());
    }

    @AfterReturning(value = "publicServiceMethods()", returning = "returnedValue")
    public void logReturnValue(JoinPoint jp, Object returnedValue) {
        if (Objects.nonNull(returnedValue)) {
            LOGGER.info(jp.getSignature().getName() + " returned " + returnedValue + "\n");
        }
    }
}
