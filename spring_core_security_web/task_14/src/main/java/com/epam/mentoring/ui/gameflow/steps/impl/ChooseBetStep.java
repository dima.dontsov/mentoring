package com.epam.mentoring.ui.gameflow.steps.impl;

import com.epam.mentoring.service.GameService;
import com.epam.mentoring.ui.gameflow.steps.GameFlowStep;
import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.localization.Localizer;
import domain.outcome.OutcomeOdd;
import domain.user.impl.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.stream.IntStream;

public class ChooseBetStep implements GameFlowStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmountToBetStep.class);

    @Value("${quit.symbol}")
    private String quitSymbol;

    private Localizer localizer;

    @Autowired
    public void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public Boolean process(GameService gameService, Player player, MessageProvider messageProvider, InfoCollector infoCollector) {
        boolean result = false;

        messageProvider.showMessage("\n" + localizer.get("betting.choose") + "\n");
        showPossibleBets(gameService, messageProvider);
        messageProvider.showMessage("\n" + localizer.get("betting.choice"));

        String choice = infoCollector.obtainStringValue();
        if (choice.equals(quitSymbol)) {
            return null;
        }

        try {

            OutcomeOdd chosenOdd = gameService.getOutcomeOdds().get(Integer.parseInt(choice) - 1);
            gameService.setPlayerChosenOdd(player, chosenOdd);

            messageProvider.showMessage("\n" + localizer.get("betting.chosen.prefix") + chosenOdd.getFullRecursiveInfo());

            result = true;

        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            LOGGER.warn(localizer.get("betting.wrong.bet.number"));
        }

        return result;
    }

    void showPossibleBets(GameService gameService, MessageProvider messageProvider) {
        List<OutcomeOdd> odds = gameService.getOutcomeOdds();

        IntStream.range(0, odds.size()).forEach(i ->
            messageProvider.showMessage(i + 1 + ". " + odds.get(i).getFullRecursiveInfo())
        );
    }
}
