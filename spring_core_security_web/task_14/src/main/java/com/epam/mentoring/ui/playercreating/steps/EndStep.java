package com.epam.mentoring.ui.playercreating.steps;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.localization.Localizer;
import domain.user.impl.Player;
import org.springframework.beans.factory.annotation.Autowired;

public class EndStep implements PlayerCreationStep {
    private Localizer localizer;

    @Autowired
    void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        messageProvider.showMessage("\n" + localizer.get("player.creation.done") + "\n");
        return true;
    }
}
