package com.epam.mentoring.repository.wager.impl;

import com.epam.mentoring.repository.wager.WagerRepository;
import domain.Wager;
import domain.user.impl.Player;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class WagerRepositoryImpl implements WagerRepository {

    private final List<Wager> wagers = new ArrayList<>();

    @Override
    public boolean add(Wager wager) {
        return this.wagers.add(wager);
    }

    @Override
    public List<Wager> getPlayerWagers(Player player) {
        return wagers.stream().filter(w -> w.getPlayer().equals(player)).collect(Collectors.toList());
    }
}
