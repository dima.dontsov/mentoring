package com.epam.mentoring.ui.infocollecting;

import java.math.BigDecimal;

public interface InfoCollector {

    String obtainStringValue();

    BigDecimal obtainBigDecimalValue();
}
