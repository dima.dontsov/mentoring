package com.epam.mentoring.repository.sportevent;

import domain.sportevent.SportEvent;

import java.util.List;

public interface SportEventRepository<T extends SportEvent> {
    List<T> getEvents();
}
