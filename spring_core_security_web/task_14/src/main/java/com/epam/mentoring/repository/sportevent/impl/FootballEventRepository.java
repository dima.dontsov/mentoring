package com.epam.mentoring.repository.sportevent.impl;

import com.epam.mentoring.repository.sportevent.SportEventRepository;
import domain.sportevent.FootballSportEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FootballEventRepository implements SportEventRepository<FootballSportEvent> {

    private List<FootballSportEvent> events;

    @Override
    public List<FootballSportEvent> getEvents() {
        return events;
    }

    @Autowired
    void setEvents(List<FootballSportEvent> events) {
        this.events = events;
    }
}
