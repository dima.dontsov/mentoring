package com.epam.mentoring.util.formatting;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface Formatter {
    String formatUserBalance(BigDecimal balance);
    LocalDate formatDate(String stringDate);
}
