package com.epam.mentoring.ui.infocollecting.impl;

import com.epam.mentoring.ui.infocollecting.ConsoleInfoCollector;

import java.math.BigDecimal;
import java.util.Scanner;

public class ScannerBasedConsoleInfoCollector implements ConsoleInfoCollector {

    private Scanner scanner;

    public ScannerBasedConsoleInfoCollector(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public String obtainStringValue() {
        return scanner.nextLine();
    }

    @Override
    public BigDecimal obtainBigDecimalValue() {
        try {
            return scanner.nextBigDecimal();
        } finally {
            scanner.nextLine();
        }
    }
}
