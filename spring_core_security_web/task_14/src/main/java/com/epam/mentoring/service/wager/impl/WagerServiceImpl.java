package com.epam.mentoring.service.wager.impl;

import com.epam.mentoring.controller.exception.InsufficientFundsException;
import com.epam.mentoring.repository.wager.WagerRepository;
import com.epam.mentoring.service.wager.WagerService;
import domain.Wager;
import domain.user.impl.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.epam.mentoring.util.validation.CommonValidator.isBetMoneyValid;

@Component
public class WagerServiceImpl implements WagerService {
    private WagerRepository wagerRepository;

    @Autowired
    public WagerServiceImpl(WagerRepository wagerRepository) {
        this.wagerRepository = wagerRepository;
    }

    @Override
    public List<Wager> getPlayerWagers(Player player) {
        return wagerRepository.getPlayerWagers(player);
    }

    @Override
    public void addWager(Wager wager) throws InsufficientFundsException {
        if (!isBetMoneyValid(wager.getAmount(), wager.getPlayer().getBalance())) {
            throw new InsufficientFundsException();
        }
        wagerRepository.add(wager);
    }
}
