package com.epam.mentoring.ui.gameflow.steps;

import com.epam.mentoring.service.GameService;
import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import domain.user.impl.Player;

public interface GameFlowStep {
    Boolean process(GameService gameService, Player player, MessageProvider messageProvider, InfoCollector infoCollector);
}
