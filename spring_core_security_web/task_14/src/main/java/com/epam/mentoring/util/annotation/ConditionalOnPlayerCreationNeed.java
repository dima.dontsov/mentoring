package com.epam.mentoring.util.annotation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

@ConditionalOnProperty(name = "player.demo", havingValue = "f")
public @interface ConditionalOnPlayerCreationNeed {

}
