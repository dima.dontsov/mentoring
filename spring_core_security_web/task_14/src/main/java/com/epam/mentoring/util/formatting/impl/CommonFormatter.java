package com.epam.mentoring.util.formatting.impl;

import com.epam.mentoring.util.formatting.Formatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class CommonFormatter implements Formatter {

    private DecimalFormat decimalFormat;
    private DateTimeFormatter dateFormatter;

    @Autowired
    public CommonFormatter(DateTimeFormatter dateFormatter, DecimalFormat decimalFormat){
        this.dateFormatter = dateFormatter;
        this.decimalFormat = decimalFormat;
    }

    @Override
    public String formatUserBalance(BigDecimal balance) {
        return decimalFormat.format(balance);
    }

    @Override
    public LocalDate formatDate(String stringDate) {
        try {
            return LocalDate.parse(stringDate, dateFormatter);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid date pattern provided");
        }
    }
}
