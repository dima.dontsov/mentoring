package com.epam.mentoring.ui.playercreating.steps;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.localization.Localizer;
import com.epam.mentoring.util.validation.CommonValidator;
import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class NameStep implements PlayerCreationStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(NameStep.class);

    private Localizer localizer;

    @Autowired
    void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\n" + localizer.get("player.creation.enter.name"));

        String name = infoCollector.obtainStringValue();
        try {
            Preconditions.checkArgument(CommonValidator.isUserNameValid(name),
                localizer.get("player.creation.invalid.name"));

            playerToBuild.setUsername(name);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            LOGGER.warn(ex.getMessage());
        }

        return stepSucceed;
    }
}
