//package com.epam.mentoring.config.sportEvents;
//
//import domain.outcome.Outcome;
//import domain.outcome.OutcomeOdd;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.time.LocalDateTime;
//
//@Configuration
//public class OutcomeConfiguration {
//
//    @Autowired
//    private BetConfiguration betConfiguration;
//
//    @Bean
//    public Outcome zeroScoresFootballOutcome() {
//        Outcome out = new Outcome("0", betConfiguration.footballPlayerScoresBetExample());
//
//        OutcomeOdd odd = new OutcomeOdd(1.75, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome oneScoreFootballOutcome() {
//        Outcome out = new Outcome("1", betConfiguration.footballPlayerScoresBetExample());
//
//        OutcomeOdd odd = new OutcomeOdd(2.50, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome moreThatTwoScoresFootballOutcome() {
//        Outcome out = new Outcome(">=2", betConfiguration.footballPlayerScoresBetExample());
//
//        OutcomeOdd odd = new OutcomeOdd(5.78, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//
//
//    @Bean
//    public Outcome zeroGoalsFootballOutcome() {
//        Outcome out = new Outcome("0", betConfiguration.footballGoalsBet());
//
//        OutcomeOdd odd = new OutcomeOdd(1.75, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome oneGoalFootballOutcome() {
//        Outcome out = new Outcome("1", betConfiguration.footballGoalsBet());
//
//        OutcomeOdd odd = new OutcomeOdd(1.25, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome moreThatTwoGoalsFootballOutcome() {
//        Outcome out = new Outcome(">=2", betConfiguration.footballGoalsBet());
//
//        OutcomeOdd odd = new OutcomeOdd(1.05, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome winnerFirstTeamFootballOutcome() {
//        Outcome out = new Outcome("Southampton", betConfiguration.footballWinnerBetExample());
//
//        OutcomeOdd outcome1odd1 = new OutcomeOdd(4d, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0),
//            LocalDateTime.of(2016, 9, 30, 18, 59));
//
//        OutcomeOdd outcome1odd2 = new OutcomeOdd(
//            5d, out, LocalDateTime.of(2016, 9, 30, 19, 0));
//
//        out.addOdd(outcome1odd1);
//        out.addOdd(outcome1odd2);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome winnerSecondTeamFootballOutcome() {
//        Outcome out = new Outcome("Bournemoth", betConfiguration.footballWinnerBetExample());
//
//        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.7, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0),
//            LocalDateTime.of(2016, 9, 30, 18, 59));
//
//        OutcomeOdd outcome2odd2 = new OutcomeOdd(1.5, out,
//            LocalDateTime.of(2016, 9, 30, 19, 0));
//
//        out.addOdd(outcome2odd1);
//        out.addOdd(outcome2odd2);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome winnerDrawFootballOutcome() {
//        Outcome out = new Outcome("Draw", betConfiguration.footballWinnerBetExample());
//
//        OutcomeOdd outcome3odd1 = new OutcomeOdd(3d, out,
//            LocalDateTime.of(2016, 9, 27, 19, 0),
//            LocalDateTime.of(2016, 9, 30, 18, 59));
//
//        OutcomeOdd outcome3odd2 = new OutcomeOdd(3.5, out,
//            LocalDateTime.of(2016, 9, 30, 19, 0));
//
//        out.addOdd(outcome3odd1);
//        out.addOdd(outcome3odd2);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome winnerFirstPlayerTennisOutcome() {
//        Outcome out = new Outcome("Rafael Nadal", betConfiguration.tennisWinnerBetExample());
//
//        OutcomeOdd odd = new OutcomeOdd(1.01, out,
//            LocalDateTime.of(2016, 1, 1, 0, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//
//    @Bean
//    public Outcome winnerSecondPlayerTennisOutcome() {
//        Outcome out = new Outcome("Alexander Zverev", betConfiguration.tennisWinnerBetExample());
//
//        OutcomeOdd odd = new OutcomeOdd(1.7, out,
//            LocalDateTime.of(2016, 1, 1, 0, 0));
//
//        out.addOdd(odd);
//
//        return out;
//    }
//}
