package com.epam.mentoring.ui.playercreating;

import domain.user.impl.Player;

public interface PlayerBuilder {
    Player buildPlayer();
}
