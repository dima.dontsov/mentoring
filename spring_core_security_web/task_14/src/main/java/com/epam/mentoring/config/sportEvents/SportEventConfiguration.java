package com.epam.mentoring.config.sportEvents;

import domain.Bet;
import domain.outcome.Outcome;
import domain.outcome.OutcomeOdd;
import domain.sportevent.FootballSportEvent;
import domain.sportevent.TennisSportEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.Arrays;

@Configuration
public class SportEventConfiguration {

    @Bean
    public FootballSportEvent footballEvent() {
        FootballSportEvent event = new FootballSportEvent("Southampton v Bournemoth",
            LocalDateTime.of(2016, 10, 27, 19, 0),
            LocalDateTime.of(2016, 10, 27, 21, 0)
        );

        event.addBet(getFootballWinnerBetExample(event));
        event.addBet(getFootballGoalsBetExample(event));
        event.addBet(getFootballScoresBetExample(event));

        return event;
    }

    @Bean
    public TennisSportEvent tennisEvent() {
        TennisSportEvent event = new TennisSportEvent(
            "Rafael Nadal vs. Alexander Zverev, Indian Wells 4th Round",
            LocalDateTime.of(2016, 8, 10, 19, 0),
            LocalDateTime.of(2016, 8, 10, 22, 0)
        );

        event.addBet(getTennisWinnerBetExample(event));

        return event;
    }

    private Bet getFootballScoresBetExample(FootballSportEvent event) {
        Bet bet = new Bet("Messi", Bet.Type.SCORES, event);

        Outcome outcome1 = new Outcome("0", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(1.75, outcome1,
            LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome2 = new Outcome("1", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(2.50, outcome2,
            LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome3 = new Outcome(">=2", bet);

        OutcomeOdd outcome3odd1 = new OutcomeOdd(5.78, outcome2,
            LocalDateTime.of(2016, 9, 27, 19, 0));

        outcome1.addOdd(outcome1odd1);
        outcome2.addOdd(outcome2odd1);
        outcome3.addOdd(outcome3odd1);

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);
        bet.addOutcome(outcome3);

        return bet;
    }

    private Bet getFootballGoalsBetExample(FootballSportEvent event) {
        Bet bet = new Bet("", Bet.Type.GOALS, event);

        Outcome outcome1 = new Outcome("0", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(1.75, outcome1,
            LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome2 = new Outcome("1", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.25, outcome2,
            LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome3 = new Outcome(">=2", bet);

        OutcomeOdd outcome3odd1 = new OutcomeOdd(1.05, outcome2,
            LocalDateTime.of(2016, 9, 27, 19, 0));

        outcome1.addOdd(outcome1odd1);
        outcome2.addOdd(outcome2odd1);
        outcome3.addOdd(outcome3odd1);

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);
        bet.addOutcome(outcome3);

        return bet;
    }

    private Bet getFootballWinnerBetExample(FootballSportEvent event) {
        Bet bet = new Bet("", Bet.Type.WINNER, event);

        Outcome outcome1 = new Outcome("Southampton", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(4d, outcome1,
            LocalDateTime.of(2016, 9, 27, 19, 0),
            LocalDateTime.of(2016, 9, 30, 18, 59));

        OutcomeOdd outcome1odd2 = new OutcomeOdd(
            5d, outcome1, LocalDateTime.of(2016, 9, 30, 19, 0));

        Outcome outcome2 = new Outcome("Bournemoth", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.7, outcome2,
            LocalDateTime.of(2016, 9, 27, 19, 0),
            LocalDateTime.of(2016, 9, 30, 18, 59));

        OutcomeOdd outcome2odd2 = new OutcomeOdd(1.5, outcome2,
            LocalDateTime.of(2016, 9, 30, 19, 0));

        Outcome outcome3 = new Outcome("Draw", bet);

        OutcomeOdd outcome3odd1 = new OutcomeOdd(3d, outcome2,
            LocalDateTime.of(2016, 9, 27, 19, 0),
            LocalDateTime.of(2016, 9, 30, 18, 59));

        OutcomeOdd outcome3odd2 = new OutcomeOdd(3.5, outcome2,
            LocalDateTime.of(2016, 9, 30, 19, 0));

        outcome1.addOdds(Arrays.asList(outcome1odd1, outcome1odd2));
        outcome2.addOdds(Arrays.asList(outcome2odd1, outcome2odd2));
        outcome3.addOdds(Arrays.asList(outcome3odd1, outcome3odd2));

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);
        bet.addOutcome(outcome3);

        return bet;
    }

    private Bet getTennisWinnerBetExample(TennisSportEvent event) {
        Bet bet = new Bet("", Bet.Type.WINNER, event);

        Outcome outcome1 = new Outcome("Rafael Nadal", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(1.01, outcome1,
            LocalDateTime.of(2016, 1, 1, 0, 0));

        Outcome outcome2 = new Outcome("Alexander Zverev", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.7, outcome2,
            LocalDateTime.of(2016, 1, 1, 0, 0));

        outcome1.addOdd(outcome1odd1);
        outcome2.addOdd(outcome2odd1);

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);

        return bet;
    }
}
