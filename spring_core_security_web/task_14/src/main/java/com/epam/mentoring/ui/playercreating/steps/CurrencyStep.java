package com.epam.mentoring.ui.playercreating.steps;

import com.epam.mentoring.ui.infocollecting.InfoCollector;
import com.epam.mentoring.ui.messageprovision.MessageProvider;
import com.epam.mentoring.util.localization.Localizer;
import com.epam.mentoring.util.validation.CommonPatterns;
import com.epam.mentoring.util.validation.CommonValidator;
import com.google.common.base.Preconditions;
import domain.Currency;
import domain.user.impl.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class CurrencyStep implements PlayerCreationStep {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyStep.class);

    @Value("${currency.supported}")
    private String supportedCurrencies;

    private Localizer localizer;

    @Autowired
    void setLocalizer(Localizer localizer) {
        this.localizer = localizer;
    }

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\n" + localizer.get("player.creation.enter.currency") + "(" + supportedCurrencies + ")");
        String currencyStr = infoCollector.obtainStringValue();

        try {
            Preconditions.checkArgument(CommonValidator.isCurrencyValid(currencyStr, CommonPatterns.CURRENCY),
                localizer.get("player.creation.invalid.currency.pattern"));

            Currency currency = Currency.valueOf(currencyStr);
            Preconditions.checkNotNull(currency, localizer.get("player.creation.not.supported.pattern"));

            playerToBuild.setCurrency(currency);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            LOGGER.warn(ex.getMessage());
        }

        return stepSucceed;
    }
}
