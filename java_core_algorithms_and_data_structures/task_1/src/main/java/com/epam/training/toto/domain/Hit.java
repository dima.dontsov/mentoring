package com.epam.training.toto.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Class that represents amount of prize mapped to a hitsGamesNumber number
 */
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Hit {

    /**
     * How many hits have been made
     */
    @Getter
    @EqualsAndHashCode.Include
    public int hitsCount;

    /**
     * Number of games with 'hitsCount' hits
     */
    @Getter
    public int hitsGamesNumber;

    /**
     * Prize for 'hitsCount' hits
     */
    @Getter
    public MoneyPrize prize;

    @Override
    public String toString() {
        return "{ " + hitsCount + ": " +
                hitsGamesNumber + ", " +
                prize.getDetails() + " }";
    }
}
