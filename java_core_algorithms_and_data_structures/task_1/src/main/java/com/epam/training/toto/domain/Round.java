package com.epam.training.toto.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Class that represents one row in the .csv file
 */
@NoArgsConstructor
public class Round {

    @Getter
    @Setter
    private LocalDate date;

    @Getter
    @Setter
    private Integer year;

    @Getter
    @Setter
    private Integer week;

    @Getter
    @Setter
    private Integer roundNo;

    @Getter
    @Setter
    private Set<Hit> hits = new LinkedHashSet<>();

    @Getter
    @Setter
    private List<Outcome> outcomes = new ArrayList<>();

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Date: ").append(this.date);
        sb.append("; Year: ").append(getYear());
        sb.append("; Week: ").append(getWeek());
        sb.append("; Round: ").append(getRoundNo());

        sb.append("; Prizes for hits: [ ");
        this.getHits().forEach(hp -> sb.append(hp).append(", "));
        sb.append("]");

        sb.append("Outcomes: [ ");
        getOutcomes().forEach(o -> sb.append(o).append("; "));
        sb.append("]");

        return sb.toString();
    }
}
