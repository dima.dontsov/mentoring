package com.epam.training.toto.validator;

public class CommonValidator {
    public static boolean validateWeekNo(int weekNoValue) {
        return weekNoValue > 0 && weekNoValue <= 52;
    }
}
