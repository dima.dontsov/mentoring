package com.epam.training.toto.statistics.impl;

import com.epam.training.toto.statistics.ParseStatisticsCollector;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TotoParseStatisticsCollector implements ParseStatisticsCollector {

    private long successfulLinesParsed = 0;

    private long failedLinesParsed = 0;

    @Override
    public void incrementSuccessfulEntriesParsed() {
        successfulLinesParsed++;
    }

    @Override
    public void incrementFailedEntriesParsed() {
        failedLinesParsed++;
    }

    @Override
    public long getSuccessfullyParsedEntriesNumber() {
        return successfulLinesParsed;
    }

    @Override
    public long getFailedParsedEntriesNumber() {
        return failedLinesParsed;
    }

    @Override
    public String getResult() {
        return "Toto file parsed lines info: " +
                "successful - " + successfulLinesParsed + ", " +
                "with failures - " + failedLinesParsed;
    }
}
