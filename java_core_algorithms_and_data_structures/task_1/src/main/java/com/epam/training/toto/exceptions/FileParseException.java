package com.epam.training.toto.exceptions;

/**
 * Exception that notifies that a file in common failed to be parsed
 * */
public class FileParseException extends Exception {
    public FileParseException(String errorMessage) {
        super(errorMessage);
    }
}
