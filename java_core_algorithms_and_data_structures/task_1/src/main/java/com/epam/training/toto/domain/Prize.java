package com.epam.training.toto.domain;

/**
 * Interface for all possible prizes
 * */
public interface Prize {
    String getDetails();
}
