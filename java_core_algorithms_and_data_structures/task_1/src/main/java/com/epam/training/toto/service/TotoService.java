package com.epam.training.toto.service;

import com.epam.training.toto.domain.*;
import com.epam.training.toto.exceptions.FileParseException;
import com.epam.training.toto.parsing.TotoParser;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static com.epam.training.toto.App.DEFAULT_APP_CURRENCY;

public class TotoService {

    private TotoParser totoParser;
    private List<Round> parsedRounds;

    public TotoService(TotoParser parser) throws FileParseException {
        this.totoParser = parser;
        this.parsedRounds = parser.parse();
    }

    public Prize getLargestPrize() {
        Optional<MoneyPrize> maxMoneyPrize = parsedRounds.parallelStream().
                map(Round::getHits).
                flatMap(Collection::stream).
                map(Hit::getPrize).
                max(Comparator.comparing(MoneyPrize::getAmount));

        return maxMoneyPrize.orElse(MoneyPrize.getDefaultValue(DEFAULT_APP_CURRENCY));
    }

    public String getRoundsOutcomeDistributions(DecimalFormat displayFormat) {
        StringBuilder sb = new StringBuilder();

        parsedRounds.forEach(r -> {
            sb.append(r.getDate()).append(", round ").append(r.getRoundNo()).append(" {\n");

            Arrays.stream(Outcome.values()).forEach(o -> {
                int totalCount = r.getOutcomes().size();
                long count = r.getOutcomes().stream().filter(out -> out.equals(o)).count();
                double distribution = ((double) count / totalCount) * 100;

                sb.append("\t").append(o.getValue()).append(" - ");
                sb.append(displayFormat.format(distribution)).append("%\n");
            });

            sb.append("}\n\n");
        });

        return sb.toString();
    }

    public void refreshData() throws FileParseException {
        parsedRounds = totoParser.parse();
    }

    /**
     * @return wager result message
     */
    public String getWagerResult(LocalDate date, int roundNo, Outcome[] wagerOutcomes) {
        List<Round> foundRounds = this.parsedRounds.parallelStream()
                .filter(r -> r.getDate().equals(date))
                .filter(r -> r.getRoundNo().equals(roundNo))
                .collect(Collectors.toList());

        if (foundRounds.size() < 1) {
            throw new IllegalArgumentException("No parsedRounds have been found for provided date and round number");
        }

        if (foundRounds.size() > 1) {
            throw new IllegalArgumentException("Try another date and round no, there is some error with this round");
        }

        Round round = foundRounds.get(0);

        if (round.getOutcomes().size() != wagerOutcomes.length) {
            throw new IllegalArgumentException(
                    "For this round you have to provide " + round.getOutcomes().size() + " outcomes");
        }

        int coincidences = 0;
        for (int i = 0; i < round.getOutcomes().size(); i++) {
            if (round.getOutcomes().get(i).equals(wagerOutcomes[i])) {
                coincidences++;
            }
        }

        MoneyPrize prize = getPrizeForCoincidences(round, coincidences);

        return "Hits: " + coincidences + ", prize: " + prize.getDetails();
    }

    private MoneyPrize getPrizeForCoincidences(Round round, int coincidences) {
        Optional<Hit> hit = round.getHits().stream()
                .filter(h -> h.hitsCount == coincidences)
                .findFirst();

        return hit.isPresent() ? hit.get().prize : MoneyPrize.getDefaultValue(DEFAULT_APP_CURRENCY);
    }
}
