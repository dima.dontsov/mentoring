package com.epam.training.toto.domain;

import java.util.Arrays;
import java.util.Optional;

/**
 * All possible outcomes for a round
 * */
public enum Outcome {
    FIRST_TEAM_WIN("1"), SECOND_TEAM_WIN("2"), DRAW("X");

    private String value;

    Outcome(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Outcome getByValue(String value) {
        Optional<Outcome> optOutcome = Arrays.stream(Outcome.values())
                .filter(o -> o.value.equalsIgnoreCase(value))
                .findFirst();

        return optOutcome.orElse(null);
    }

    @Override
    public String toString() {
        return this.value;
    }
}
