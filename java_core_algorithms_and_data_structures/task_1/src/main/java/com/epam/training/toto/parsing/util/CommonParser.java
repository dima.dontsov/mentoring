package com.epam.training.toto.parsing.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Generic parser that deals with common parsing entities
 */
public class CommonParser {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd.");

    public static LocalDate parseDate(String stringDate) throws IllegalArgumentException {
        try {
            return LocalDate.parse(stringDate, DATE_FORMATTER);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse date " + stringDate);
        }
    }
}
