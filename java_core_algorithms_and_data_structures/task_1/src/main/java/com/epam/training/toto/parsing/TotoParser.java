package com.epam.training.toto.parsing;

import com.epam.training.toto.domain.MoneyPrize;
import com.epam.training.toto.domain.Outcome;
import com.epam.training.toto.domain.Round;
import com.epam.training.toto.exceptions.FileParseException;
import com.epam.training.toto.exceptions.LineComponentParseException;
import com.epam.training.toto.statistics.ParseStatisticsCollector;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
public abstract class TotoParser{

    @Getter
    protected final ParseStatisticsCollector parseStatisticsCollector;

    public abstract List<Round> parse() throws FileParseException;

    protected abstract Integer getParsedYearValue(String yearString) throws LineComponentParseException;

    protected abstract Integer getParsedWeekNumber(String weekString) throws LineComponentParseException;

    protected abstract LocalDate getParsedDate(String dateString) throws LineComponentParseException;

    protected abstract Integer getParsedRoundNo(String roundNoString) throws LineComponentParseException;

    protected abstract Integer getParsedHitsNumber(String hitsNumberString) throws LineComponentParseException;

    protected abstract String getParsedPrizeCurrency(String prizeString) throws LineComponentParseException;

    protected abstract Long getParsedPrizeAmount(String prizeString) throws LineComponentParseException;

    protected MoneyPrize getParsedPrize(String prizeString) throws LineComponentParseException {
        return new MoneyPrize(
                getParsedPrizeAmount(prizeString),
                getParsedPrizeCurrency(prizeString)
        );
    }

    protected abstract Outcome getParsedOutcome(String outcomeString) throws LineComponentParseException;
}
