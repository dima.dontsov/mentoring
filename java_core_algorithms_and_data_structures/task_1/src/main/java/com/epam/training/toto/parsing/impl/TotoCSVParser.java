package com.epam.training.toto.parsing.impl;

import com.epam.training.toto.domain.Hit;
import com.epam.training.toto.domain.MoneyPrize;
import com.epam.training.toto.domain.Outcome;
import com.epam.training.toto.domain.Round;
import com.epam.training.toto.exceptions.FileParseException;
import com.epam.training.toto.exceptions.LineComponentParseException;
import com.epam.training.toto.parsing.TotoParser;
import com.epam.training.toto.parsing.util.CommonParser;
import com.epam.training.toto.statistics.ParseStatisticsCollector;
import com.epam.training.toto.util.RegexPatterns;
import com.epam.training.toto.validator.CommonValidator;
import com.epam.training.toto.validator.TotoValidator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.epam.training.toto.App.*;

/**
 * Toto logic parser that deals with .csv files
 */
public class TotoCSVParser extends TotoParser {

    private String csvFilePath;
    private String lineSplitter;

    public TotoCSVParser(String csvFilePath, String lineSplitter, ParseStatisticsCollector parseStatisticsCollector) {
        super(parseStatisticsCollector);

        this.csvFilePath = csvFilePath;
        this.lineSplitter = lineSplitter;
    }

    @Override
    public List<Round> parse() throws FileParseException {
        List<Round> parsedRounds = new ArrayList<>();
        String filePath = getClass().getClassLoader().getResource(csvFilePath).getFile();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

            String line;
            while ((line = br.readLine()) != null) {
                String[] roundLineComponents = line.split(lineSplitter);
                try {
                    parsedRounds.add(mapLineToEntity(roundLineComponents));
                    parseStatisticsCollector.incrementSuccessfulEntriesParsed();
                } catch (LineComponentParseException e) {
                    parseStatisticsCollector.incrementFailedEntriesParsed();
                }
            }

            return parsedRounds;

        } catch (IOException e) {
            throw new FileParseException("Failed to parse file: " + e.getMessage());
        }
    }

    private Round mapLineToEntity(String[] lineValues) throws LineComponentParseException {
        Round round = new Round();

        round.setYear(getParsedYearValue(lineValues[0]));
        round.setWeek(getParsedWeekNumber(lineValues[1]));
        round.setRoundNo(getParsedRoundNo(lineValues[2]));
        round.setDate(getParsedDate(lineValues[3]));

        for (int i = HIT_PRIZE_SECTION_BEGIN_INDEX, hits = OUTCOMES_ENTRIES_COUNT;
             i < HIT_PRIZE_SECTION_END_INDEX;
             i += 2, hits--) {

            int hitsNumber = getParsedHitsNumber(lineValues[i]);
            MoneyPrize prizeForHits = getParsedPrize(lineValues[i + 1]);

            round.getHits().add(new Hit(hits, hitsNumber, prizeForHits));
        }

        for (int i = OUTCOMES_SECTION_BEGIN_INDEX; i < OUTCOMES_SECTION_END_INDEX; i++) {
            round.getOutcomes().add(getParsedOutcome(lineValues[i]));
        }

        return round;
    }

    @Override
    protected Integer getParsedYearValue(String yearString) throws LineComponentParseException {
        try {
            int year = Integer.parseInt(yearString);
            if (!TotoValidator.validateAllowedYear(year)) {
                throw new IllegalArgumentException();
            }
            return year;
        } catch (NumberFormatException e) {
            throw new LineComponentParseException("Failed to parse year from '" + yearString + "'");
        } catch (IllegalArgumentException e) {
            throw new LineComponentParseException("Invalid year value in '" + yearString + "'");
        }
    }

    @Override
    protected Integer getParsedWeekNumber(String weekString) throws LineComponentParseException {
        try {
            int week = Integer.parseInt(weekString);
            if (!CommonValidator.validateWeekNo(week)) {
                throw new IllegalArgumentException();
            }
            return week;
        } catch (NumberFormatException e) {
            throw new LineComponentParseException("Failed to parse week from '" + weekString + "'");
        } catch (IllegalArgumentException e) {
            throw new LineComponentParseException("Invalid week value in '" + weekString + "'");
        }
    }

    @Override
    protected LocalDate getParsedDate(String dateString) throws LineComponentParseException {
        try {
            return CommonParser.parseDate(dateString);
        } catch (Exception e) {
            throw new LineComponentParseException(e.getMessage());
        }
    }

    @Override
    protected Integer getParsedRoundNo(String roundNoString) throws LineComponentParseException {
        try {
            int roundNo = Integer.parseInt(roundNoString);
            if (!TotoValidator.validateRoundNo(roundNo)) {
                throw new IllegalArgumentException();
            }
            return roundNo;
        } catch (NumberFormatException e) {
            throw new LineComponentParseException("Failed to parse round no from '" + roundNoString + "'");
        } catch (IllegalArgumentException e) {
            throw new LineComponentParseException("Invalid round no value in '" + roundNoString + "'");
        }
    }

    @Override
    protected Integer getParsedHitsNumber(String hitsNumberString) throws LineComponentParseException {
        try {
            int hitsNumber = Integer.parseInt(hitsNumberString);
            if (!TotoValidator.validateHitsNumber(hitsNumber)) {
                throw new IllegalArgumentException();
            }
            return hitsNumber;
        } catch (NumberFormatException e) {
            throw new LineComponentParseException("Failed to parse hits number from '" + hitsNumberString + "'");
        } catch (IllegalArgumentException e) {
            throw new LineComponentParseException("Invalid hits number value in '" + hitsNumberString + "'");
        }
    }

    @Override
    protected String getParsedPrizeCurrency(String prizeString) throws LineComponentParseException {
        Pattern currencyPattern = Pattern.compile(RegexPatterns.CURRENCY);
        Matcher currencyMatcher = currencyPattern.matcher(prizeString);

        if (!currencyMatcher.find() || currencyMatcher.groupCount() > 1) {
            throw new LineComponentParseException("Failed to parse currency from '" + prizeString + "'");
        }

        return currencyMatcher.group(0);
    }

    @Override
    protected Long getParsedPrizeAmount(String prizeString) throws LineComponentParseException {
        Pattern amountPattern = Pattern.compile(RegexPatterns.AMOUNT);
        Matcher amountMatcher = amountPattern.matcher(prizeString);

        if (!amountMatcher.find()) {
            throw new LineComponentParseException("Failed to parse amount from '" + prizeString + "'");
        }

        String amountString = amountMatcher.group(0).replace(" ", "");

        try {
            long amount = Long.parseLong(amountString);
            if (!TotoValidator.validatePrizeAmount(amount)) {
                throw new IllegalArgumentException();
            }
            return amount;
        } catch (NumberFormatException e) {
            throw new LineComponentParseException("Failed to parse amount from '" + prizeString + "'");
        } catch (IllegalArgumentException e) {
            throw new LineComponentParseException("Invalid prize amount value in '" + prizeString + "'");
        }
    }

    @Override
    protected Outcome getParsedOutcome(String outcomeString) throws LineComponentParseException {
        Pattern outcomePattern = Pattern.compile(RegexPatterns.OUTCOME);
        Matcher outcomeMatcher = outcomePattern.matcher(outcomeString);

        if (!outcomeMatcher.find() || outcomeMatcher.groupCount() > 1) {
            throw new LineComponentParseException("Failed to parse currency from '" + outcomeString + "'");
        }

        Outcome outcome = Outcome.getByValue(outcomeMatcher.group(0));

        return Optional.ofNullable(outcome).orElseThrow(() ->
                new LineComponentParseException("No outcomes found for '" + outcomeString + "'")
        );
    }
}
