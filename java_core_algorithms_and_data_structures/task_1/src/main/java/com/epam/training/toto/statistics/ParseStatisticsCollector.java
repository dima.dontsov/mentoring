package com.epam.training.toto.statistics;

public interface ParseStatisticsCollector extends StatisticsCollector {
    void incrementSuccessfulEntriesParsed();

    void incrementFailedEntriesParsed();

    long getSuccessfullyParsedEntriesNumber();

    long getFailedParsedEntriesNumber();
}
