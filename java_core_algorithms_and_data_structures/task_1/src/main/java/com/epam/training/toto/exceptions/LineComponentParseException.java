package com.epam.training.toto.exceptions;

/**
 * Exception for notifying that a file line failed to be parsed
 * */
public class LineComponentParseException extends Exception {

    public LineComponentParseException(String errorMessage) {
        super(errorMessage);
    }
}
