package com.epam.training.toto.validator;

/**
 * Validates Toto relating components
 */
public class TotoValidator {
    public static boolean validateRoundNo(int roundNo) {
        return roundNo >= 1 && roundNo <= 2;
    }

    public static boolean validateHitsNumber(int hitsNumber) {
        return hitsNumber > -1;
    }

    public static boolean validatePrizeAmount(long prizeAmount) {
        return prizeAmount > -1;
    }

    public static boolean validateAllowedYear(int yearValue) {
        return yearValue > 0;
    }
}
