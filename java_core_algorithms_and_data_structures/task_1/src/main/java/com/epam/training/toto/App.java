package com.epam.training.toto;

import com.epam.training.toto.domain.Outcome;
import com.epam.training.toto.exceptions.FileParseException;
import com.epam.training.toto.parsing.TotoParser;
import com.epam.training.toto.parsing.impl.TotoCSVParser;
import com.epam.training.toto.parsing.util.CommonParser;
import com.epam.training.toto.service.TotoService;
import com.epam.training.toto.statistics.ParseStatisticsCollector;
import com.epam.training.toto.statistics.impl.TotoParseStatisticsCollector;
import com.epam.training.toto.util.RegexPatterns;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Application's point of entry
 */
public class App {

    /**
     * Additional information constants
     */
    public static final int HIT_PRIZE_ENTRIES_COUNT = 5;
    public static final int HIT_PRIZE_SECTION_BEGIN_INDEX = 4;
    public static final int HIT_PRIZE_SECTION_END_INDEX = HIT_PRIZE_SECTION_BEGIN_INDEX + (HIT_PRIZE_ENTRIES_COUNT * 2);

    public static final int OUTCOMES_SECTION_BEGIN_INDEX = 14;
    public static final int OUTCOMES_ENTRIES_COUNT = 14;
    public static final int OUTCOMES_SECTION_END_INDEX = OUTCOMES_SECTION_BEGIN_INDEX + OUTCOMES_ENTRIES_COUNT;

    public static final String DEFAULT_APP_CURRENCY = "UAH";

    private static final String FILE_PATH = "toto.csv";
    private static final String FILE_COMPONENTS_SPLITTER = ";";

    /**
     * Options for user of the application to choose
     */
    private static final String HIGHEST_REWARD_OPTION = "1";
    private static final String OUTCOMES_DISTRIBUTIONS_OPTION = "2";
    private static final String WAGING_OPTION = "3";

    public static void main(String[] args) {
        ParseStatisticsCollector parseStatisticsCollector = new TotoParseStatisticsCollector();

        TotoParser parser = new TotoCSVParser(FILE_PATH, FILE_COMPONENTS_SPLITTER, parseStatisticsCollector);

        try {

            TotoService totoService = new TotoService(parser);
            System.out.println(parser.getParseStatisticsCollector().getResult());
            System.out.println("Application will use only " +
                    parser.getParseStatisticsCollector().getSuccessfullyParsedEntriesNumber() + " valid lines data");

            Scanner scanner = new Scanner(System.in);
            boolean go = true;
            while (go) {
                try {
                    printAppPossibilities();
                    String choice = scanner.nextLine();
                    switch (choice) {
                        case HIGHEST_REWARD_OPTION:
                            System.out.println(totoService.getLargestPrize());
                            break;
                        case OUTCOMES_DISTRIBUTIONS_OPTION:
                            DecimalFormat df = new DecimalFormat("#.##");
                            df.setRoundingMode(RoundingMode.FLOOR);

                            System.out.println("\n" + totoService.getRoundsOutcomeDistributions(df));
                            break;
                        case WAGING_OPTION:
                            processWagerOption(totoService);
                            break;
                        default:
                            System.out.println("Wrong choice. Try again!");
                    }

                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                } finally {
                    System.out.println("\n Would you like to try again? (enter 'y' if so):");
                    String choice = scanner.nextLine();
                    go = choice.equalsIgnoreCase("y");
                }
            }

        } catch (FileParseException e) {
            System.out.println("Failed to parse source file: " + e.getMessage());
        }
    }

    private static void printAppPossibilities() {
        System.out.println("\nPress: \n" + HIGHEST_REWARD_OPTION + " - to see the largest prize recorded;");
        System.out.println(OUTCOMES_DISTRIBUTIONS_OPTION +
                " - to see outcomes distributions for each successfully parsed round;");
        System.out.println(WAGING_OPTION + " - to try to wager and see your result and prize.");

        System.out.println("\nYour choice?");
    }

    private static void processWagerOption(TotoService totoService) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter date (pattern: year.month.day):");
        LocalDate date = CommonParser.parseDate(scanner.nextLine());

        System.out.println("Enter round number (1 or 2):");
        int roundNo = Integer.parseInt(scanner.nextLine());

        if (roundNo < 1 || roundNo > 2) {
            throw new IllegalArgumentException("Round number has to be 1 or 2");
        }

        System.out.println("Enter " + OUTCOMES_ENTRIES_COUNT + " wagers separated by comma (1, 2 or X)");
        String wagerString = scanner.nextLine();

        Pattern p = Pattern.compile(RegexPatterns.WAGING);
        Matcher m = p.matcher(wagerString);

        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid waging values entered");
        }

        String[] wagers = wagerString.split(",");
        Outcome[] outcomes = new Outcome[OUTCOMES_ENTRIES_COUNT];
        for (int i = 0; i < OUTCOMES_ENTRIES_COUNT; i++) {
            outcomes[i] = Outcome.getByValue(wagers[i]);
        }

        System.out.println(totoService.getWagerResult(date, roundNo, outcomes));
    }
}
