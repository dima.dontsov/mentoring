package com.epam.training.toto.util;

/**
 * Regex patterns used by the application
 * */
public interface RegexPatterns {

    String CURRENCY = "([A-Z]){3}";
    String AMOUNT = "([0-9]{1,3}\\s)+";
    String OUTCOME = "([12Xx]{1})";

    //TODO not good to use 14 like that, use dynamic value insert instead
    String WAGING = "([12xX]{1},*){14}";
}
