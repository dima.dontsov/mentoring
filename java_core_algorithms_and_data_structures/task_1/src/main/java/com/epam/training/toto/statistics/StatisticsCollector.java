package com.epam.training.toto.statistics;

/**
 * Interface which implementors are responsible for statistics gathering
 * */
public interface StatisticsCollector {

    /**
     * @return statistics collector result
     * */
    String getResult();
}
