package com.epam.training.toto.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
public class MoneyPrize implements Prize {

    @Getter
    private Long amount;

    @Getter
    private String currency;

    public MoneyPrize(Long amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public static MoneyPrize getDefaultValue(String currency) {
        return new MoneyPrize(0L, currency.toUpperCase());
    }

    @Override
    public String getDetails() {
        return amount + " " + currency.toUpperCase();
    }

    @Override
    public String toString() {
        return getDetails();
    }
}