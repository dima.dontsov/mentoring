package sportsbetting.ui.gameflow.steps.impl;

import org.junit.Before;
import org.junit.Test;
import sportsbetting.exception.InsufficientFundsException;

import java.math.BigDecimal;
import java.util.InputMismatchException;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.*;

public class AmountToBetStepTest extends GameFlowStepTest {

    private AmountToBetStep step;

    @Before
    public void setUp() {
        step = spy(new AmountToBetStep());
    }

    @Test
    public void process_validAmountProvided() {
        BigDecimal validAmount = new BigDecimal(11);
        doReturn(validAmount).when(infoCollectorMock).obtainBigDecimalValue();

        Boolean result = step.process(gameServiceMock, messageProviderMock, infoCollectorMock);

        assertTrue(result);
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidMoneyPatternProvided() {
        doThrow(InputMismatchException.class).when(infoCollectorMock).obtainBigDecimalValue();

        Boolean result = step.process(gameServiceMock, messageProviderMock, infoCollectorMock);

        assertFalse(result);
        verify(messageProviderMock).showErrorMessage(anyString());
    }

    @Test
    public void process_notEnoughMoney() {
        doThrow(InsufficientFundsException.class).when(infoCollectorMock).obtainBigDecimalValue();

        Boolean result = step.process(gameServiceMock, messageProviderMock, infoCollectorMock);

        assertFalse(result);
        verify(messageProviderMock).showErrorMessage(anyString());
    }
}