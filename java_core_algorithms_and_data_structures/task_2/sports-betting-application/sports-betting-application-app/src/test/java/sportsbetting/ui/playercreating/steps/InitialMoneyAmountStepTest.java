package sportsbetting.ui.playercreating.steps;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.InputMismatchException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;


public class InitialMoneyAmountStepTest extends PlayerCreationStepTest {

    private static final BigDecimal VALID_INITIAL_AMOUNT = new BigDecimal(1);
    private static final BigDecimal INVALID_INITIAL_AMOUNT = new BigDecimal(-1);

    private InitialMoneyAmountStep step;

    @Before
    public void setUp() {
        step = new InitialMoneyAmountStep();
    }

    @Test
    public void process_validInitialAmount() {
        when(infoCollectorMock.obtainBigDecimalValue()).thenReturn(VALID_INITIAL_AMOUNT);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertTrue(result);
        verify(playerToBuildMock).setBalance(VALID_INITIAL_AMOUNT);
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidInitialAmount() {
        when(infoCollectorMock.obtainBigDecimalValue()).thenReturn(INVALID_INITIAL_AMOUNT);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setBalance(any(BigDecimal.class));
        verify(messageProviderMock).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidMoneyPattern() {
        when(infoCollectorMock.obtainBigDecimalValue()).thenThrow(InputMismatchException.class);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setBalance(any(BigDecimal.class));
        verify(messageProviderMock).showErrorMessage(anyString());
    }
}