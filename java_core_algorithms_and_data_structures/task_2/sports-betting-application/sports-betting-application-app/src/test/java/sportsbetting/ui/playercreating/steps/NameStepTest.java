package sportsbetting.ui.playercreating.steps;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class NameStepTest extends PlayerCreationStepTest {

    private static final String VALID_NAME = "test_name";
    private static final String INVALID_NAME = "";

    private NameStep step;

    @Before
    public void setUp() {
        step = new NameStep();
    }

    @Test
    public void process_validName() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(VALID_NAME);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertTrue(result);
        verify(playerToBuildMock).setUsername(VALID_NAME);
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidName() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(INVALID_NAME);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setUsername(anyString());
        verify(messageProviderMock).showErrorMessage(anyString());
    }
}