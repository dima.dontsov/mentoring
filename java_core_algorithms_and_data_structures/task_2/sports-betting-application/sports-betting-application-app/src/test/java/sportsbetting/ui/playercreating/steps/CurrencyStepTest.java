package sportsbetting.ui.playercreating.steps;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import domain.Currency;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class CurrencyStepTest extends PlayerCreationStepTest {

    private static final String VALID_CURRENCY_STRING = "UAH";
    private static final String INVALID_CURRENCY_STRING = "UAHO";
    private static final String NON_EXISTING_CURRENCY_STRING = "AOP";

    private CurrencyStep step;

    @Before
    public void setUp() {
        step = new CurrencyStep();
    }

    @Test
    public void process_validExistingCurrency() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(VALID_CURRENCY_STRING);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertTrue(result);
        verify(playerToBuildMock).setCurrency(any(Currency.class));
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    @Ignore
    public void process_validNonExistingCurrency() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(NON_EXISTING_CURRENCY_STRING);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setCurrency(any(Currency.class));
        verify(messageProviderMock).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidCurrency() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(INVALID_CURRENCY_STRING);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setCurrency(any(Currency.class));
        verify(messageProviderMock).showErrorMessage(anyString());
    }
}