package sportsbetting.ui.playercreating.steps;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class PasswordStepTest extends PlayerCreationStepTest {

    private static final String VALID_PASSWORD = "test_pass";
    private static final String INVALID_PASSWORD = "";

    private PasswordStep step;

    @Before
    public void setUp() {
        this.step = new PasswordStep();
    }

    @Test
    public void process_validPassword() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(VALID_PASSWORD);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertTrue(result);
        verify(playerToBuildMock).setPassword(VALID_PASSWORD);
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidPassword() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(INVALID_PASSWORD);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setUsername(anyString());
        verify(messageProviderMock).showErrorMessage(anyString());
    }
}