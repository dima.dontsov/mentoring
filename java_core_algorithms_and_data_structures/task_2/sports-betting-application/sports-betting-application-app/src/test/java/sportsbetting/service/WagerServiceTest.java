package sportsbetting.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import domain.Wager;
import domain.user.impl.Player;
import sportsbetting.exception.InsufficientFundsException;
import sportsbetting.repository.WagerRepository;
import sportsbetting.ui.playercreating.steps.AccountNumberStep;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WagerServiceTest extends AccountNumberStep {

    private WagerService service;

    @Mock
    private WagerRepository repositoryMock;

    @Mock
    private Wager wagerMock;

    @Mock
    private Player playerMock;

    @Before
    public void setUp() {
        service = new WagerService(repositoryMock);
    }

    @Test
    public void addWager_enoughMoney() {
        BigDecimal balance = BigDecimal.valueOf(11);
        BigDecimal wagerAmount = BigDecimal.valueOf(balance.doubleValue() - 1);

        doReturn(wagerAmount).when(wagerMock).getAmount();
        doReturn(playerMock).when(wagerMock).getPlayer();
        doReturn(balance).when(playerMock).getBalance();

        service.addWager(wagerMock);

        verify(repositoryMock).add(wagerMock);
    }

    @Test(expected = InsufficientFundsException.class)
    public void addWager_notEnoughMoney() {
        BigDecimal balance = BigDecimal.valueOf(11);
        BigDecimal wagerAmount = BigDecimal.valueOf(balance.doubleValue() + 1);

        doReturn(wagerAmount).when(wagerMock).getAmount();
        doReturn(playerMock).when(wagerMock).getPlayer();
        doReturn(balance).when(playerMock).getBalance();

        service.addWager(wagerMock);

        verify(repositoryMock, never()).add(any(Wager.class));
    }
}