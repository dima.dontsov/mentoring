package sportsbetting.ui.gameflow.steps.impl;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class ShowNewBalanceStepTest extends GameFlowStepTest {

    private ShowNewBalanceStep step;

    @Before
    public void setUp() {
        step = new ShowNewBalanceStep();
    }

    @Test
    public void process() {
        BigDecimal balance = new BigDecimal(11);

        doReturn(balance).when(gameServiceMock).getPlayerAvailableMoney();

        Boolean result = step.process(gameServiceMock, messageProviderMock, infoCollectorMock);

        assertTrue(result);
        verify(messageProviderMock).showMessage(anyString());
    }
}