package sportsbetting.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import domain.Result;
import domain.Wager;
import domain.outcome.OutcomeOdd;
import domain.sportevent.SportEvent;
import domain.user.impl.Player;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    //    @RunWith(JUnitParamsRunner.class)
//    public class CalculatorTest {
//
//        @Test
//        @Parameters({
//            "1+1/2     |  1",
//            "1-4+4/2   | -1",
//            "5/2+1     |  3",
//            "2*3-3/2+1 |  6"
//        })
//        public void shouldReturnResult(String line, int result) {
//            assertThat(runCalculation(line)).isEqualTo(result);
//        }
//    }
    //    ArgumentCaptor<Player> argumentCaptor;


    @InjectMocks
    @Spy
    private GameService gameService;

    @Mock
    private Player player;

    @Mock
    private WagerService wagerService;

    @Mock
    private SportEventService sportEventService;

    @Test
    public void getProcessedWagers() {
        //TODO
    }

    @Test
    public void getWinAmount() {
        BigDecimal amount = new BigDecimal(10);
        double odd = 2;

        OutcomeOdd outcomeOddMock = mock(OutcomeOdd.class);
        Wager wagerMock = mock(Wager.class);
        when(wagerMock.getAmount()).thenReturn(amount);
        when(wagerMock.getOutcomeOdd()).thenReturn(outcomeOddMock);
        when(outcomeOddMock.getOdd()).thenReturn(odd);

        BigDecimal expected = new BigDecimal(20);
        BigDecimal result = gameService.getWinAmount(wagerMock);

        assertEquals(expected, result);
    }

    @Test
    public void getAfterWinAmount() {
        BigDecimal initAmount = new BigDecimal(2);
        BigDecimal winAmount = new BigDecimal(2);

        BigDecimal expected = new BigDecimal(4);
        BigDecimal result = gameService.getAfterWinAmount(initAmount, winAmount);

        assertEquals(expected, result);
    }

    @Test
    public void getAfterBetAmount() {
        BigDecimal initAmount = new BigDecimal(2);
        BigDecimal betAmount = new BigDecimal(1);

        BigDecimal expected = new BigDecimal(1);
        BigDecimal result = gameService.getAfterBetAmount(initAmount, betAmount);

        assertEquals(expected, result);
    }

    @Test
    public void placeBet() {
        Player playerMock = mock(Player.class);
        Wager wagerMock = mock(Wager.class);
        OutcomeOdd oddMock = mock(OutcomeOdd.class);

        BigDecimal betAmount = new BigDecimal(10);
        BigDecimal afterBetAmount = new BigDecimal(5);

        doReturn(wagerMock).when(gameService).getPreparedWager(any(Player.class), any(BigDecimal.class), any(OutcomeOdd.class));
        doReturn(afterBetAmount).when(gameService).getAfterBetAmount(any(BigDecimal.class), any(BigDecimal.class));

        gameService.placeBet(betAmount, oddMock);

        verify(wagerService).addWager(wagerMock);
        verify(player).setBalance(afterBetAmount);
    }

    @Test
    public void getResults() {
        Result resultMock1 = mock(Result.class);
        Result resultMock2 = mock(Result.class);

        SportEvent sportEventMock1 = mock(SportEvent.class);
        SportEvent sportEventMock2 = mock(SportEvent.class);

        List<SportEvent> events = new ArrayList<>();
        events.add(sportEventMock1);
        events.add(sportEventMock2);

        doReturn(resultMock1).when(sportEventMock1).getResult();
        doReturn(resultMock2).when(sportEventMock2).getResult();
        doReturn(events).when(sportEventService).getEvents();

        List<Result> expected = Arrays.asList(resultMock1, resultMock2);
        List<Result> result = gameService.getResults();

        assertEquals(expected, result);
    }

    @Test
    public void processWonBet() {
        BigDecimal winAmount = new BigDecimal(2);
        BigDecimal afterWinAmount = new BigDecimal(10);

        Wager wagerMock = mock(Wager.class);
        doReturn(winAmount).when(gameService).getWinAmount(wagerMock);
        doReturn(afterWinAmount).when(gameService).getAfterWinAmount(any(BigDecimal.class), any(BigDecimal.class));

        gameService.processWonBet(wagerMock);

        verify(player).setBalance(afterWinAmount);
    }

    @Test
    public void getOutcomeOdds() {
        //nothing to test
    }

    @Test
    public void getResultsOutcomeOdds() {
        //nothing to test
    }

    @Test
    public void generateResultsForEvents() {
        //nothing to test
    }
}