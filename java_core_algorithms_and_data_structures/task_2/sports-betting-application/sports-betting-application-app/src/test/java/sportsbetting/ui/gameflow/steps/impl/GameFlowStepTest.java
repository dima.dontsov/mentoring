package sportsbetting.ui.gameflow.steps.impl;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import sportsbetting.service.GameService;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;

@RunWith(MockitoJUnitRunner.class)
public abstract class GameFlowStepTest {

    @Mock
    public GameService gameServiceMock;

    @Mock
    public MessageProvider messageProviderMock;

    @Mock
    public InfoCollector infoCollectorMock;
}
