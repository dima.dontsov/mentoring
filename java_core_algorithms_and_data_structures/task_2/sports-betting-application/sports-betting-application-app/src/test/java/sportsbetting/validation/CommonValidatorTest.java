package sportsbetting.validation;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CommonValidatorTest {

    @Test
    public void isBetMoneyValid_betEqualsBalance() {
        BigDecimal bet = new BigDecimal(1);
        BigDecimal balance = new BigDecimal(1);

        assertTrue(CommonValidator.isBetMoneyValid(bet, balance));
    }

    @Test
    public void isBetMoneyValid_betLessThanBalance() {
        BigDecimal bet = new BigDecimal(1);
        BigDecimal balance = new BigDecimal(2);

        assertTrue(CommonValidator.isBetMoneyValid(bet, balance));
    }

    @Test
    public void isBetMoneyValid_betBiggerThanBalance() {
        BigDecimal bet = new BigDecimal(2);
        BigDecimal balance = new BigDecimal(1);

        assertFalse(CommonValidator.isBetMoneyValid(bet, balance));
    }

    @Test
    public void isBetMoneyValid_betLessThanMinimumAllowed() {
        BigDecimal bet = new BigDecimal(-1);
        BigDecimal balance = new BigDecimal(1);

        assertFalse(CommonValidator.isBetMoneyValid(bet, balance));
    }

    @Test
    public void isAccountNumberValid_validAccountNumber() {
        assertTrue(CommonValidator.isAccountNumberValid("1111-1111", CommonPatterns.ACCOUNT_NUMBER));
    }

    @Test
    public void isAccountNumberValid_invalidAccountNumber() {
        assertFalse(CommonValidator.isAccountNumberValid("111-,1111", CommonPatterns.ACCOUNT_NUMBER));
    }

    @Test
    public void isUserNameValid_nameIsPresent() {
        assertTrue(CommonValidator.isUserNameValid("Dan"));
    }

    @Test
    public void isUserNameValid_nameIsNotPresent() {
        assertFalse(CommonValidator.isUserNameValid(""));
    }

    @Test
    public void isPasswordValid_passwordIsPresent() {
        assertTrue(CommonValidator.isPasswordValid("any pass"));
    }

    @Test
    public void isPasswordValid_passwordIsNotPresent() {
        assertFalse(CommonValidator.isPasswordValid(""));
    }

    @Test
    public void isBirthDateValid_birthDateIsBeforeCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        LocalDate validBirthDate = LocalDate.of(
                currentDate.getYear() - 1, currentDate.getMonth(), currentDate.getDayOfMonth());

        assertTrue(CommonValidator.isBirthDateValid(validBirthDate));
    }

    @Test
    public void isBirthDateValid_birthDateIsAfterCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        LocalDate invalidBirthDate = LocalDate.of(
                currentDate.getYear() + 1, currentDate.getMonth(), currentDate.getDayOfMonth());

        assertFalse(CommonValidator.isBirthDateValid(invalidBirthDate));
    }

    @Test
    public void isCurrencyValid_validNumberOfLetters() {
        assertTrue(CommonValidator.isCurrencyValid("uio", CommonPatterns.CURRENCY));
    }

    @Test
    public void isCurrencyValid_invalidNumberOfLetters() {
        assertFalse(CommonValidator.isCurrencyValid("uior", CommonPatterns.CURRENCY));
    }

    @Test
    public void isCurrencyValid_containsInvalidSymbols() {
        assertFalse(CommonValidator.isCurrencyValid("u8i", CommonPatterns.CURRENCY));
    }

    @Test
    public void isInitialAmountValid_amountGreaterThanZero() {
        assertTrue(CommonValidator.isInitialAmountValid(new BigDecimal(1)));
    }

    @Test
    public void isInitialAmountValid_amountEqualsZero() {
        assertTrue(CommonValidator.isInitialAmountValid(new BigDecimal(1)));
    }

    @Test
    public void isInitialAmountValid_amountLessThanZero() {
        assertFalse(CommonValidator.isInitialAmountValid(new BigDecimal(-1)));
    }
}