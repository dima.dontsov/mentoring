package sportsbetting.ui.playercreating.steps;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class AccountNumberStepTest extends PlayerCreationStepTest {

    private static final String VALID_ACC_NUMBER = "1111-1111";
    private static final String INVALID_ACC_NUMBER = "abc";

    private AccountNumberStep step;

    @Before
    public void setUp() {
        step = new AccountNumberStep();
    }

    @Test
    public void process_validAccountNumber() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(VALID_ACC_NUMBER);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertTrue(result);
        verify(playerToBuildMock).setAccountNumber(VALID_ACC_NUMBER);
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidAccountNumber() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(INVALID_ACC_NUMBER);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setAccountNumber(VALID_ACC_NUMBER);
        verify(messageProviderMock).showErrorMessage(anyString());
    }
}