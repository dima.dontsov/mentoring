package sportsbetting.repository;

import org.junit.Before;
import org.junit.Test;
import domain.Wager;
import domain.user.impl.Player;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WagerRepositoryTest {

    private WagerRepository wagerRepository;
    private Player firstTestPlayer;
    private Player secondTestPlayer;

    @Before
    public void init() {
        wagerRepository = new WagerRepository();

        firstTestPlayer = new Player();
        firstTestPlayer.setAccountNumber("1111-1111");

        secondTestPlayer = new Player();
        secondTestPlayer.setAccountNumber("2222-2222");
    }

    @Test
    public void getPlayerWagers() {
        Wager firstPlayerWager1 = Wager.newBuilder().setPlayer(firstTestPlayer).build();
        Wager firstPlayerWager2 = Wager.newBuilder().setPlayer(firstTestPlayer).build();
        Wager secondPlayerWager = Wager.newBuilder().setPlayer(secondTestPlayer).build();

        wagerRepository.add(firstPlayerWager1);
        wagerRepository.add(firstPlayerWager2);
        wagerRepository.add(secondPlayerWager);

        List<Wager> playerWagers = wagerRepository.getPlayerWagers(firstTestPlayer);

        assertEquals(2, playerWagers.size());

        assertTrue(playerWagers.contains(firstPlayerWager1));
        assertTrue(playerWagers.contains(firstPlayerWager2));
    }


    @Test
    public void getPlayerWagers_containsNoPlayerWagers() {
        Wager secondPlayerWager = Wager.newBuilder().setPlayer(secondTestPlayer).build();

        wagerRepository.add(secondPlayerWager);

        int resultingSize = wagerRepository.getPlayerWagers(firstTestPlayer).size();

        assertEquals(0, resultingSize);
    }
}