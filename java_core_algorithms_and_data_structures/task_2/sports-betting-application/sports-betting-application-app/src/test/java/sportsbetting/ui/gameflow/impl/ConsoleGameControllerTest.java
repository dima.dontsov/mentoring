package sportsbetting.ui.gameflow.impl;

import domain.Wager;
import domain.outcome.OutcomeOdd;
import sportsbetting.service.GameService;
import sportsbetting.ui.gameflow.steps.GameFlowStep;
import sportsbetting.ui.infocollecting.ConsoleInfoCollector;
import sportsbetting.ui.messageprovision.impl.ConsoleMessageProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleGameControllerTest {

    @Mock
    private GameService gameServiceMock;

    @Mock
    private ConsoleMessageProvider messageProviderMock;

    @Mock
    private ConsoleInfoCollector infoCollectorMock;

    private ConsoleGameController controller;

    @Before
    public void setUp() {
        controller = spy(new ConsoleGameController(gameServiceMock, messageProviderMock, infoCollectorMock));
    }

    @Test
    public void play() {
        doReturn(false).doReturn(true).when(controller).isBettingDone(any(AtomicBoolean.class));
        doNothing().when(controller).processBettingSteps(any(AtomicBoolean.class));
        doNothing().when(controller).showResult(any(List.class));

        controller.play();

        verify(controller).showResult(any(List.class));
        verify(controller, times(2)).processBettingSteps(any(AtomicBoolean.class));
    }

    @Test
    public void processBettingStep() {
        GameFlowStep stepMock = mock(GameFlowStep.class);
        AtomicBoolean isBettingDoneMock = mock(AtomicBoolean.class);

        when(stepMock.process(gameServiceMock, messageProviderMock, infoCollectorMock)).thenReturn(false, true);

        controller.processBettingStep(stepMock, isBettingDoneMock);

        verify(stepMock, times(2)).process(gameServiceMock, messageProviderMock, infoCollectorMock);
    }

    @Test
    public void processBettingSteps_playerQuitsInMiddle() {
        AtomicBoolean bettingDoneFlagMock = mock(AtomicBoolean.class);

        doReturn(false).doReturn(true).when(controller).isBettingDone(any(AtomicBoolean.class));
        doNothing().when(controller).processBettingStep(any(GameFlowStep.class), any(AtomicBoolean.class));

        controller.processBettingSteps(bettingDoneFlagMock);

        verify(controller).processBettingStep(any(GameFlowStep.class), any(AtomicBoolean.class));
    }

    @Test
    public void processBettingSteps_playerDoesNotQuit() {
        AtomicBoolean bettingDoneFlagMock = mock(AtomicBoolean.class);

        doReturn(false).when(controller).isBettingDone(any(AtomicBoolean.class));
        doNothing().when(controller).processBettingStep(any(GameFlowStep.class), any(AtomicBoolean.class));

        controller.processBettingSteps(bettingDoneFlagMock);

        int betStepsCount = ConsoleGameController.BETTING_STEPS.size();
        verify(controller, times(betStepsCount)).processBettingStep(any(GameFlowStep.class), any(AtomicBoolean.class));
    }

    @Test
    public void isPlayerQuit_quit() {
        assertTrue(controller.isPlayerQuit(null));
    }

    @Test
    public void isPlayerQuit_noQuit() {
        assertFalse(controller.isPlayerQuit(true));
    }

    @Test
    public void showResult(){
        Wager wagerMock1 = mock(Wager.class);
        Wager wagerMock2 = mock(Wager.class);

        List<Wager> wagerListMock = Arrays.asList(wagerMock1, wagerMock2);

        doReturn("").when(controller).getWagerResultingMessage(any(Wager.class));
        doReturn(new BigDecimal(10)).when(controller).getPlayerBalance();

        controller.showResult(wagerListMock);

        int expectedInvocationTime = 2 + wagerListMock.size();
        verify(messageProviderMock, times(expectedInvocationTime)).showMessage(anyString());
    }

    @Test
    public void getWagerResultingMessage_wagerWin(){
        String outcomeOddInfo = "info";

        Wager wagerMock = mock(Wager.class);
        OutcomeOdd outcomeOddMock = mock(OutcomeOdd.class);

        when(wagerMock.getWin()).thenReturn(true);
        when(wagerMock.getOutcomeOdd()).thenReturn(outcomeOddMock);
        when(outcomeOddMock.getFullRecursiveInfo()).thenReturn(outcomeOddInfo);

        String expected = ConsoleGameController.WAGER_WIN_PREFIX + outcomeOddInfo;
        String result = controller.getWagerResultingMessage(wagerMock);

        assertEquals(expected, result);
    }

    @Test
    public void getWagerResultingMessage_wagerLose(){
        String outcomeOddInfo = "info";

        Wager wagerMock = mock(Wager.class);
        OutcomeOdd outcomeOddMock = mock(OutcomeOdd.class);

        when(wagerMock.getWin()).thenReturn(false);
        when(wagerMock.getOutcomeOdd()).thenReturn(outcomeOddMock);
        when(outcomeOddMock.getFullRecursiveInfo()).thenReturn(outcomeOddInfo);

        String expected = ConsoleGameController.WAGER_LOSE_PREFIX + outcomeOddInfo;
        String result = controller.getWagerResultingMessage(wagerMock);

        assertEquals(expected, result);
    }
}