package sportsbetting.parsing;

import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class CommonParserTest {

    private static final DateTimeFormatter TEST_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd");

    @Test
    public void parseDate_parseCorrectDate() {
        String correctDateString = "1997.12.12";

        LocalDate expected = LocalDate.of(1997, 12, 12);
        LocalDate result = CommonParser.parseDate(correctDateString, TEST_DATE_TIME_FORMATTER);

        assertEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseDate_parseIncorrectDate() {
        String incorrectDateString = "1997/12/12";
        CommonParser.parseDate(incorrectDateString, TEST_DATE_TIME_FORMATTER);
    }
}