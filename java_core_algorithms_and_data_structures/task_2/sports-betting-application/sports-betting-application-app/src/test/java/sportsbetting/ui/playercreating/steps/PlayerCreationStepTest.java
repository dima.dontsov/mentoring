package sportsbetting.ui.playercreating.steps;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;

@RunWith(MockitoJUnitRunner.class)
abstract class PlayerCreationStepTest {

    @Mock
    Player playerToBuildMock;

    @Mock
    InfoCollector infoCollectorMock;

    @Mock
    MessageProvider messageProviderMock;
}
