package sportsbetting.ui.playercreating.steps;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class BirthDateStepTest extends PlayerCreationStepTest {

    private static final String VALID_BIRTH_DATE_STRING = "1997.01.08";
    private static final String INVALID_FORMAT_DATE_STRING = "1997/1/8";
    private static final String INVALID_DATE_STRING = "2999.01.08";

    private BirthDateStep step;

    @Before
    public void setUp() {
        step = new BirthDateStep();
    }

    @Test
    public void process_validBirthDate() {
        when(infoCollectorMock.obtainStringValue()).thenReturn(VALID_BIRTH_DATE_STRING);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertTrue(result);
        verify(playerToBuildMock).setDateOfBirth(any(LocalDate.class));
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    public void process_invalidFormatBirthDate() {
        invalidBirthDateTestBody(INVALID_FORMAT_DATE_STRING);
    }

    @Test
    public void invalidBirthDateTestBody() {
        invalidBirthDateTestBody(INVALID_DATE_STRING);
    }

    private void invalidBirthDateTestBody(String invalidDateString) {
        when(infoCollectorMock.obtainStringValue()).thenReturn(invalidDateString);

        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertFalse(result);
        verify(playerToBuildMock, never()).setDateOfBirth(any(LocalDate.class));
        verify(messageProviderMock).showErrorMessage(anyString());
    }
}
