package sportsbetting.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import domain.outcome.Outcome;
import domain.sportevent.SportEvent;
import sportsbetting.repository.sportevent.FootballEventRepository;
import sportsbetting.repository.sportevent.TennisEventRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SportEventServiceTest {

    private SportEventService sportEventService;

    @Mock
    private FootballEventRepository footballRepository;

    @Mock
    private TennisEventRepository tennisRepository;

    @Before
    public void setUp() {
        sportEventService = spy(new SportEventService(footballRepository, tennisRepository));
    }

    @Test
    public void getEvents() {
        List<SportEvent> footballEvents = Arrays.asList(mock(SportEvent.class), mock(SportEvent.class));
        List<SportEvent> tennisEvents = Collections.singletonList(mock(SportEvent.class));

        doReturn(footballEvents).when(footballRepository).getEvents();
        doReturn(tennisEvents).when(tennisRepository).getEvents();

        List<SportEvent> events = sportEventService.getEvents();

        int expectedSize = 3;
        assertEquals(expectedSize, events.size());
    }

    @Test
    public void getOutcomes() {
        List outcomeListMock1 = Arrays.asList(mock(Outcome.class), mock(Outcome.class));
        List outcomeListMock2 = Collections.singletonList(mock(Outcome.class));

        SportEvent eventMock1 = mock(SportEvent.class);
        SportEvent eventMock2 = mock(SportEvent.class);

        List<SportEvent> allEvents = Arrays.asList(eventMock1, eventMock2);

        doReturn(outcomeListMock1).when(eventMock1).getOutcomes();
        doReturn(outcomeListMock2).when(eventMock2).getOutcomes();
        doReturn(allEvents).when(sportEventService).getEvents();

        List<Outcome> allOutcomes = sportEventService.getAllOutcomes();

        int expectedSize = 3;
        assertEquals(expectedSize, allOutcomes.size());
    }
}