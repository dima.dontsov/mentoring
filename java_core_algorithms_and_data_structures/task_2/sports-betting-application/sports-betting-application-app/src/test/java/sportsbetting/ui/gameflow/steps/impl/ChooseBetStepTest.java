package sportsbetting.ui.gameflow.steps.impl;

import org.junit.Before;
import org.junit.Test;
import sportsbetting.App;
import domain.outcome.OutcomeOdd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ChooseBetStepTest extends GameFlowStepTest {

    private ChooseBetStep step;

    @Before
    public void setUp() {
        step = spy(new ChooseBetStep());
    }

    @Test
    public void process_successfulBetChoosing() {
        String oddIndexInput = "1";
        List<OutcomeOdd> outcomeOddsTest = Collections.singletonList(mock(OutcomeOdd.class));

        doNothing().when(step).showPossibleBets(gameServiceMock, messageProviderMock);
        doReturn(oddIndexInput).when(infoCollectorMock).obtainStringValue();
        doReturn(outcomeOddsTest).when(gameServiceMock).getOutcomeOdds();

        Boolean result = step.process(gameServiceMock, messageProviderMock, infoCollectorMock);

        assertTrue(result);
        verify(messageProviderMock, never()).showErrorMessage(anyString());
        verify(gameServiceMock).setLastChosenOdd(any(OutcomeOdd.class));
    }

    @Test
    public void process_userQuits() {
        doNothing().when(step).showPossibleBets(gameServiceMock, messageProviderMock);
        doReturn(App.Config.QUIT_SYMBOL).when(infoCollectorMock).obtainStringValue();

        Boolean result = step.process(gameServiceMock, messageProviderMock, infoCollectorMock);

        assertNull(result);
        verify(messageProviderMock, never()).showErrorMessage(anyString());
    }

    @Test
    public void process_wrongBetNumberEntered() {
        String invalidOddIndexInput = "a";
        List<OutcomeOdd> outcomeOddsTest = Collections.singletonList(mock(OutcomeOdd.class));

        doNothing().when(step).showPossibleBets(gameServiceMock, messageProviderMock);
        doReturn(invalidOddIndexInput).when(infoCollectorMock).obtainStringValue();
        doReturn(outcomeOddsTest).when(gameServiceMock).getOutcomeOdds();

        Boolean result = step.process(gameServiceMock, messageProviderMock, infoCollectorMock);

        assertFalse(result);
        verify(messageProviderMock).showErrorMessage(anyString());
        verify(gameServiceMock, never()).setLastChosenOdd(any(OutcomeOdd.class));
    }


    @Test
    public void showPossibleBets() {
        int oddsNum = 3;
        List<OutcomeOdd> testOdds = new ArrayList<>();

        IntStream.rangeClosed(1, oddsNum).forEach(i -> testOdds.add(mock(OutcomeOdd.class)));

        doReturn(testOdds).when(gameServiceMock).getOutcomeOdds();
        step.showPossibleBets(gameServiceMock, messageProviderMock);

        verify(messageProviderMock, times(oddsNum)).showMessage(anyString());
    }
}