package sportsbetting.util;

import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class FormatterTest {

    @Ignore
    @Test
    public void getFormatValue_correctFormat() {
        String expected = "10001.111";
        BigDecimal balance = new BigDecimal(10001.1111);
        String actual = Formatter.getFormatValue(balance);

        assertEquals(expected, actual);
    }
}