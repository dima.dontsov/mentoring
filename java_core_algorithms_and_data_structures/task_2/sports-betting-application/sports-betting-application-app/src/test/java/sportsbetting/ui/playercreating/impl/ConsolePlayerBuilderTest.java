package sportsbetting.ui.playercreating.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.ConsoleInfoCollector;
import sportsbetting.ui.messageprovision.impl.ConsoleMessageProvider;
import sportsbetting.ui.playercreating.steps.PlayerCreationStep;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ConsolePlayerBuilderTest {

    private ConsolePlayerBuilder playerBuilder;

    @Mock
    private PlayerCreationStep stepMock;

    @Mock
    private ConsoleInfoCollector infoCollectorMock;

    @Mock
    private ConsoleMessageProvider messageProviderMock;

    @Before
    public void setUp() {
        playerBuilder = new ConsolePlayerBuilder(infoCollectorMock, messageProviderMock);
    }

    @Test
    public void processPlayerCreationStep() {
        Player playerToBuild = new Player();

        when(stepMock.process(playerToBuild, infoCollectorMock, messageProviderMock))
                .thenReturn(false, false, true);

        playerBuilder.processPlayerCreationStep(stepMock, playerToBuild);

        verify(stepMock, times(3)).process(playerToBuild, infoCollectorMock, messageProviderMock);
    }
}