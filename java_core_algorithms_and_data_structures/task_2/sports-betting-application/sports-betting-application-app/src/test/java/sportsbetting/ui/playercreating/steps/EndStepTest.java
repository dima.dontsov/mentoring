package sportsbetting.ui.playercreating.steps;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

public class EndStepTest extends PlayerCreationStepTest {

    private EndStep step;

    @Before
    public void setUp() {
        step = new EndStep();
    }

    @Test
    public void process() {
        boolean result = step.process(playerToBuildMock, infoCollectorMock, messageProviderMock);

        assertTrue(result);
        verify(messageProviderMock).showMessage(anyString());
    }
}