package sportsbetting.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import domain.outcome.Outcome;
import domain.sportevent.SportEvent;
import sportsbetting.repository.sportevent.FootballEventRepository;
import sportsbetting.repository.sportevent.SportEventRepository;
import sportsbetting.repository.sportevent.TennisEventRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class SportEventService {

    @NonNull
    private final FootballEventRepository footballRepository;

    @NonNull
    private final TennisEventRepository tennisRepository;

    public List<SportEvent> getEvents() {
        return Stream.of(footballRepository, tennisRepository)
                .map(SportEventRepository::getEvents)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public List<Outcome> getAllOutcomes() {
        return getEvents().stream()
                .map(SportEvent::getOutcomes)
                .flatMap(Collection::stream).collect(Collectors.toList());
    }
}
