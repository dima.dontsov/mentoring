package sportsbetting.ui.playercreating.steps;

import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;
import sportsbetting.validation.CommonPatterns;
import sportsbetting.validation.CommonValidator;

public class AccountNumberStep implements PlayerCreationStep {

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\nEnter your account number (pattern - 'XXXX-XXXX')");
        String accNumStr = infoCollector.obtainStringValue();

        try {
            Preconditions.checkArgument(CommonValidator.isAccountNumberValid(accNumStr, CommonPatterns.ACCOUNT_NUMBER),
                    "Wrong account number entered");

            playerToBuild.setAccountNumber(accNumStr);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            messageProvider.showErrorMessage(ex.getMessage());
        }

        return stepSucceed;
    }
}
