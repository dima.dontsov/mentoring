package sportsbetting.parsing;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CommonParser {
    public static LocalDate parseDate(String stringDate, DateTimeFormatter dateTimeFormatter) {
        try {
            return LocalDate.parse(stringDate, dateTimeFormatter);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid date pattern provided");
        }
    }
}
