package sportsbetting.ui.gameflow.steps.impl;

import sportsbetting.App;
import domain.outcome.OutcomeOdd;
import sportsbetting.service.GameService;
import sportsbetting.ui.gameflow.steps.GameFlowStep;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;

import java.util.List;
import java.util.stream.IntStream;

public class ChooseBetStep implements GameFlowStep {

    @Override
    public Boolean process(GameService gameService, MessageProvider messageProvider, InfoCollector infoCollector) {
        boolean result = false;

        messageProvider.showMessage("\nPlease choose an outcome to bet on! (choose a number or press q for quit):\n");

        showPossibleBets(gameService, messageProvider);

        messageProvider.showMessage("\nYour choice trololo?");

        String choice = infoCollector.obtainStringValue();

        if (choice.equals(App.Config.QUIT_SYMBOL)) {
            return null;
        }

        try {

            OutcomeOdd chosenOdd = gameService.getOutcomeOdds().get(Integer.parseInt(choice) - 1);
            gameService.setLastChosenOdd(chosenOdd);

            messageProvider.showMessage("\nChosen bet: " + chosenOdd.getFullRecursiveInfo());

            result = true;

        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            messageProvider.showErrorMessage("Wrong bet number entered");
        }

        return result;
    }

    void showPossibleBets(GameService gameService, MessageProvider messageProvider) {
        List<OutcomeOdd> odds = gameService.getOutcomeOdds();

        IntStream.range(0, odds.size()).forEach(i ->
                messageProvider.showMessage(i + 1 + ". " + odds.get(i).getFullRecursiveInfo())
        );
    }
}
