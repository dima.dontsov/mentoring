package sportsbetting.ui.playercreating.steps;

import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;

public interface PlayerCreationStep {
    boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider);
}
