package sportsbetting.validation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.regex.Pattern;

public class CommonValidator {

    public static boolean isBetMoneyValid(BigDecimal betAmount, BigDecimal playerBalance) {
        return betAmount.compareTo(new BigDecimal(0)) > 0 && betAmount.compareTo(playerBalance) <= 0;
    }

    public static boolean isAccountNumberValid(String accountNumber, Pattern accountNumberPattern) {
        return accountNumberPattern.matcher(accountNumber).matches();
    }

    public static boolean isUserNameValid(String userName) {
        return !userName.isEmpty();
    }

    public static boolean isPasswordValid(String password) {
        return !password.isEmpty();
    }

    public static boolean isBirthDateValid(LocalDate birthDate) {
        return birthDate.isBefore(LocalDate.now());
    }

    public static boolean isCurrencyValid(String currency, Pattern currencyPattern) {
        return currencyPattern.matcher(currency).matches();
    }

    public static boolean isInitialAmountValid(BigDecimal initialAmount) {
        return initialAmount.compareTo(BigDecimal.valueOf(0)) > 0;
    }
}
