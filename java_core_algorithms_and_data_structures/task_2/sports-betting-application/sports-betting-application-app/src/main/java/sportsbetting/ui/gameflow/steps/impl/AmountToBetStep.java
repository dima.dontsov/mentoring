package sportsbetting.ui.gameflow.steps.impl;

import sportsbetting.exception.InsufficientFundsException;
import sportsbetting.service.GameService;
import sportsbetting.ui.gameflow.steps.GameFlowStep;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;

import java.math.BigDecimal;
import java.util.InputMismatchException;

public class AmountToBetStep implements GameFlowStep {

    @Override
    public Boolean process(GameService gameService, MessageProvider messageProvider, InfoCollector infoCollector) {
        boolean result = false;

        messageProvider.showMessage("How much do you want to bet on it?");

        try {

            BigDecimal betAmount = infoCollector.obtainBigDecimalValue();

            gameService.placeBet(betAmount, gameService.getLastChosenOdd());

            result = true;

        } catch (InputMismatchException e) {
            messageProvider.showErrorMessage("Invalid bet pattern provided");

        } catch (InsufficientFundsException e) {
            messageProvider.showErrorMessage("You have no enough money for this bet");
        }

        return result;
    }
}
