package sportsbetting.ui.infocollecting.impl;

import sportsbetting.ui.infocollecting.ConsoleInfoCollector;

import java.math.BigDecimal;
import java.util.Scanner;

public class ScannerBasedInfoCollector implements ConsoleInfoCollector {

    private Scanner scanner = new Scanner(System.in);

    @Override
    public String obtainStringValue() {
        return scanner.nextLine();
    }

    @Override
    public BigDecimal obtainBigDecimalValue() {
        try {
            return scanner.nextBigDecimal();
        } finally {
            scanner.nextLine();
        }
    }
}
