package sportsbetting.ui.playercreating.steps;

import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;
import sportsbetting.validation.CommonValidator;

public class NameStep implements PlayerCreationStep {

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\nEnter your name (must not be empty): ");

        String name = infoCollector.obtainStringValue();
        try {
            Preconditions.checkArgument(CommonValidator.isUserNameValid(name), "Invalid user name");

            playerToBuild.setUsername(name);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            messageProvider.showErrorMessage(ex.getMessage());
        }

        return stepSucceed;
    }
}
