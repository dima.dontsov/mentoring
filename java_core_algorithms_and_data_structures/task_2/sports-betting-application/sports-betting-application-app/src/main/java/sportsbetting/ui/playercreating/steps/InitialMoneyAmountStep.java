package sportsbetting.ui.playercreating.steps;

import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;
import sportsbetting.validation.CommonValidator;

import java.math.BigDecimal;
import java.util.InputMismatchException;

public class InitialMoneyAmountStep implements PlayerCreationStep {

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\nHow much money do you have (more than 0)? ");

        try {
            BigDecimal initialAmount = infoCollector.obtainBigDecimalValue();

            Preconditions.checkArgument(CommonValidator.isInitialAmountValid(initialAmount),
                    "Invalid amount provided");

            playerToBuild.setBalance(initialAmount);
            stepSucceed = true;

        } catch (InputMismatchException ex) {
            messageProvider.showErrorMessage("Invalid money pattern");
        } catch (IllegalArgumentException ex) {
            messageProvider.showErrorMessage(ex.getMessage());
        }

        return stepSucceed;
    }
}
