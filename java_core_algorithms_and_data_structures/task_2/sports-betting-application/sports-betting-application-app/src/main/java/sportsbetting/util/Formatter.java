package sportsbetting.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;

public class Formatter {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd");

    public static String getFormatValue(BigDecimal value) {
        return DECIMAL_FORMAT.format(value.doubleValue());
    }

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");
}