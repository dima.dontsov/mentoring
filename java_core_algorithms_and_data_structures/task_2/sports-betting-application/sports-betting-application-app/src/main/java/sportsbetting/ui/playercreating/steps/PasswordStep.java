package sportsbetting.ui.playercreating.steps;

import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;
import sportsbetting.validation.CommonValidator;

public class PasswordStep implements PlayerCreationStep {

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\nEnter your password (must not be empty): ");
        String password = infoCollector.obtainStringValue();
        try {
            Preconditions.checkArgument(CommonValidator.isPasswordValid(password), "Invalid password");

            playerToBuild.setPassword(password);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            messageProvider.showErrorMessage(ex.getMessage());
        }

        return stepSucceed;
    }
}
