package sportsbetting.ui.playercreating.steps;

import com.google.common.base.Preconditions;
import domain.Currency;
import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;
import sportsbetting.util.CurrencyUtil;
import sportsbetting.validation.CommonPatterns;
import sportsbetting.validation.CommonValidator;

import java.util.Objects;

public class CurrencyStep implements PlayerCreationStep {

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\nEnter your currency (UAH, USD, EUR):");
        String currencyStr = infoCollector.obtainStringValue();

        try {
            Preconditions.checkArgument(CommonValidator.isCurrencyValid(currencyStr, CommonPatterns.CURRENCY),
                    "Invalid currency pattern");

            Currency currency = CurrencyUtil.getIfExists(currencyStr);

            Preconditions.checkNotNull(currency, "Entered currency is not supported");

            playerToBuild.setCurrency(Currency.UAH);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            messageProvider.showErrorMessage(ex.getMessage());
        }

        return stepSucceed;
    }
}
