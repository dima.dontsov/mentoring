package sportsbetting.ui.gameflow.impl;

import domain.Wager;
import sportsbetting.service.GameService;
import sportsbetting.ui.gameflow.GameController;
import sportsbetting.ui.gameflow.steps.GameFlowStep;
import sportsbetting.ui.gameflow.steps.impl.AmountToBetStep;
import sportsbetting.ui.gameflow.steps.impl.ChooseBetStep;
import sportsbetting.ui.gameflow.steps.impl.ShowNewBalanceStep;
import sportsbetting.ui.infocollecting.ConsoleInfoCollector;
import sportsbetting.ui.messageprovision.impl.ConsoleMessageProvider;
import sportsbetting.util.Formatter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

/**
 * State
 */
public class ConsoleGameController implements GameController {

    static final String WAGER_WIN_PREFIX = "WON: ";
    static final String WAGER_LOSE_PREFIX = "LOST: ";

    static final List<Supplier<GameFlowStep>> BETTING_STEPS = Arrays.asList(
            ChooseBetStep::new,
            AmountToBetStep::new,
            ShowNewBalanceStep::new
    );

    private GameService gameService;
    private ConsoleMessageProvider messageProvider;
    private ConsoleInfoCollector infoCollector;

    public ConsoleGameController(
            GameService gameService,
            ConsoleMessageProvider messageProvider,
            ConsoleInfoCollector infoCollector) {

        this.gameService = gameService;
        this.messageProvider = messageProvider;
        this.infoCollector = infoCollector;
    }

    public void play() {
        AtomicBoolean bettingDoneFlag = new AtomicBoolean(false);

        do {
            processBettingSteps(bettingDoneFlag);
        } while (!isBettingDone(bettingDoneFlag));

        showResult(this.gameService.getProcessedWagers());
    }

    void processBettingSteps(AtomicBoolean bettingDoneFlag) {
        Iterator<Supplier<GameFlowStep>> bettingStepsIterator = BETTING_STEPS.iterator();

        while (bettingStepsIterator.hasNext() && !isBettingDone(bettingDoneFlag)) {
            GameFlowStep nextStep = bettingStepsIterator.next().get();
            processBettingStep(nextStep, bettingDoneFlag);
        }
    }

    void processBettingStep(GameFlowStep step, AtomicBoolean isBettingDone) {
        Boolean stepResult;

        do {
            stepResult = step.process(this.gameService, this.messageProvider, this.infoCollector);
        } while (!isPlayerQuit(stepResult) && !stepResult);

        isBettingDone.set(isPlayerQuit(stepResult));
    }

    void showResult(List<Wager> playerProcessedWagers) {
        messageProvider.showMessage("\nBetting results:\n");

        playerProcessedWagers.forEach(wager -> messageProvider.showMessage(getWagerResultingMessage(wager)));

        messageProvider.showMessage("\nYour final balance: " + Formatter.getFormatValue(getPlayerBalance()));
    }

    boolean isPlayerQuit(Boolean stepResult) {
        return Objects.isNull(stepResult);
    }

    String getWagerResultingMessage(Wager wager) {
        return (wager.getWin() ? WAGER_WIN_PREFIX : WAGER_LOSE_PREFIX) + wager.getOutcomeOdd().getFullRecursiveInfo();
    }

    BigDecimal getPlayerBalance() {
        return this.gameService.getPlayer().getBalance();
    }

    boolean isBettingDone(AtomicBoolean isBettingDone) {
        return isBettingDone.get();
    }
}
