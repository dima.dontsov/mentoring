package sportsbetting;

import domain.user.impl.Player;
import sportsbetting.repository.WagerRepository;
import sportsbetting.repository.sportevent.FootballEventRepository;
import sportsbetting.repository.sportevent.TennisEventRepository;
import sportsbetting.service.GameService;
import sportsbetting.service.SportEventService;
import sportsbetting.service.WagerService;
import sportsbetting.ui.gameflow.GameController;
import sportsbetting.ui.gameflow.impl.ConsoleGameController;
import sportsbetting.ui.infocollecting.impl.ScannerBasedInfoCollector;
import sportsbetting.ui.messageprovision.impl.ConsoleMessageProvider;
import sportsbetting.ui.playercreating.PlayerBuilder;
import sportsbetting.ui.playercreating.impl.ConsolePlayerBuilder;

public class App {

    public static void main(String[] args) {

        PlayerBuilder playerBuilder = new ConsolePlayerBuilder(
                new ScannerBasedInfoCollector(), ConsoleMessageProvider.getInstance());

        Player player = Config.CREATE_NEW_PLAYER_MODE ? playerBuilder.buildPlayer() : Player.newDemoPlayer();

        GameService gameService = new GameService(
                player,
                new WagerService(new WagerRepository()),
                new SportEventService(new FootballEventRepository(), new TennisEventRepository())
        );

        GameController consoleGame = new ConsoleGameController(
                gameService, ConsoleMessageProvider.getInstance(), new ScannerBasedInfoCollector());

        consoleGame.play();
    }

    public static class Config {
        public static final String QUIT_SYMBOL = "q";
        private static final boolean CREATE_NEW_PLAYER_MODE = true;
    }
}
