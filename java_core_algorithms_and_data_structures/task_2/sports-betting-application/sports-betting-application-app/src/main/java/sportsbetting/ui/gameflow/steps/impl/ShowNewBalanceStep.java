package sportsbetting.ui.gameflow.steps.impl;

import sportsbetting.service.GameService;
import sportsbetting.ui.gameflow.steps.GameFlowStep;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;
import sportsbetting.util.Formatter;

public class ShowNewBalanceStep implements GameFlowStep {

    @Override
    public Boolean process(GameService gameService, MessageProvider messageProvider, InfoCollector infoCollector) {
        messageProvider.showMessage(
                "Your new balance: " + Formatter.getFormatValue(gameService.getPlayerAvailableMoney()));

        return true;
    }
}
