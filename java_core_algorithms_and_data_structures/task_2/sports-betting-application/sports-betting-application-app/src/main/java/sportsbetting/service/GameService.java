package sportsbetting.service;

import lombok.Getter;
import lombok.Setter;
import domain.Result;
import domain.Wager;
import domain.outcome.Outcome;
import domain.outcome.OutcomeOdd;
import domain.sportevent.SportEvent;
import domain.user.impl.Player;
import sportsbetting.exception.InsufficientFundsException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class GameService {

    @Getter
    private final Player player;

    private final List<SportEvent> events;

    private final WagerService wagerService;

    private final SportEventService sportEventService;

    @Getter
    @Setter
    private OutcomeOdd lastChosenOdd;

    public GameService(Player player, WagerService wagerService, SportEventService sportEventService) {
        this.player = player;
        this.wagerService = wagerService;
        this.sportEventService = sportEventService;

        this.events = new ArrayList<>(sportEventService.getEvents());
    }

    public BigDecimal getPlayerAvailableMoney() {
        return player.getBalance();
    }

    public List<Wager> getProcessedWagers() {
        generateResultsForEvents(this.sportEventService.getAllOutcomes());

        List<Result> results = getResults();

        List<Wager> playerWagers = wagerService.getPlayerWagers(player);

        List<OutcomeOdd> resultsOutcomeOdds = getResultsOutcomeOdds(results);
        playerWagers.forEach(w -> {
            if (resultsOutcomeOdds.contains(w.getOutcomeOdd())) {
                w.setWin(true);
                processWonBet(w);
            } else {
                w.setWin(false);
            }
            w.setProcessed(true);
        });

        return wagerService.getPlayerWagers(player);
    }

    public void placeBet(BigDecimal betAmount, OutcomeOdd outcomeOdd) throws InsufficientFundsException {
        Wager wager = getPreparedWager(player, betAmount, outcomeOdd);
        wagerService.addWager(wager);

        player.setBalance(getAfterBetAmount(player.getBalance(), betAmount));
    }

    Wager getPreparedWager(Player player, BigDecimal betAmount, OutcomeOdd outcomeOdd) {
        return Wager.newBuilder()
                .setPlayer(player)
                .setOutcomeOdd(outcomeOdd)
                .setAmount(betAmount)
                .setCurrency(player.getCurrency())
                .setTimestamp(LocalDateTime.now()).build();
    }

    public List<OutcomeOdd> getOutcomeOdds() {
        return this.events.stream()
                .map(SportEvent::getOutcomeOdds)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    List<OutcomeOdd> getResultsOutcomeOdds(List<Result> results) {
        return results.stream()
                .map(Result::getOutcomes)
                .flatMap(Collection::stream)
                .map(Outcome::getOdds)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    List<Result> getResults() {
        return this.sportEventService.getEvents()
                .stream()
                .map(SportEvent::getResult)
                .collect(Collectors.toList());
    }

    void processWonBet(Wager wager) {
        BigDecimal winAmount = getWinAmount(wager);
        BigDecimal afterWinAmount = getAfterWinAmount(this.player.getBalance(), winAmount);
        this.player.setBalance(afterWinAmount);
    }

    BigDecimal getAfterWinAmount(BigDecimal initBalance, BigDecimal winAmount) {
        return initBalance.add(winAmount);
    }

    BigDecimal getAfterBetAmount(BigDecimal initBalance, BigDecimal betAmount) {
        return initBalance.subtract(betAmount);
    }

    BigDecimal getWinAmount(Wager wager) {
        return wager.getAmount().multiply(new BigDecimal(wager.getOutcomeOdd().getOdd()));
    }

    void generateResultsForEvents(List<Outcome> outcomes) {
        outcomes.forEach(o -> {
            boolean outcomePass = Math.random() < 0.5;
            if (outcomePass) {
                o.getBet().getSportEvent().getResult().getOutcomes().add(o);
            }
        });
    }
}
