package sportsbetting.validation;

import java.util.regex.Pattern;

public interface CommonPatterns {
    Pattern ACCOUNT_NUMBER = Pattern.compile("([\\d]{4})-([\\d]{4})");
    Pattern CURRENCY = Pattern.compile("[A-Za-z]{3}");
}
