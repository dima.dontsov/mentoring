package sportsbetting.repository.sportevent;

import domain.Bet;
import domain.outcome.Outcome;
import domain.outcome.OutcomeOdd;
import domain.sportevent.TennisSportEvent;

import java.time.LocalDateTime;

public class TennisEventRepository extends SportEventRepository<TennisSportEvent> {

    @Override
    protected void prefillRepository() {

        TennisSportEvent event = new TennisSportEvent(
                "Rafael Nadal vs. Alexander Zverev, Indian Wells 4th Round",
                LocalDateTime.of(2016, 8, 10, 19, 0),
                LocalDateTime.of(2016, 8, 10, 22, 0)
        );

        event.addBet(getWinnerBetExample(event));

        this.events.add(event);
    }

    private Bet getWinnerBetExample(TennisSportEvent event) {
        Bet bet = new Bet("", Bet.Type.WINNER, event);

        Outcome outcome1 = new Outcome("Rafael Nadal", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(1.01, outcome1,
                LocalDateTime.of(2016, 1, 1, 0, 0));

        Outcome outcome2 = new Outcome("Alexander Zverev", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.7, outcome2,
                LocalDateTime.of(2016, 1, 1, 0, 0));

        outcome1.addOdd(outcome1odd1);
        outcome2.addOdd(outcome2odd1);

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);

        return bet;
    }
}
