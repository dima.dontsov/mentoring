package sportsbetting.repository.sportevent;

import domain.Bet;
import domain.outcome.Outcome;
import domain.outcome.OutcomeOdd;
import domain.sportevent.FootballSportEvent;

import java.time.LocalDateTime;
import java.util.Arrays;


public class FootballEventRepository extends SportEventRepository<FootballSportEvent> {

    @Override
    protected void prefillRepository() {

        FootballSportEvent event = new FootballSportEvent("Southampton v Bournemoth",
                LocalDateTime.of(2016, 10, 27, 19, 0),
                LocalDateTime.of(2016, 10, 27, 21, 0)
        );

        event.addBet(getWinnerBetExample(event));
        event.addBet(getGoalsBetExample(event));
        event.addBet(getScoresBetExample(event));

        this.events.add(event);
    }

    private Bet getScoresBetExample(FootballSportEvent event) {
        Bet bet = new Bet("Messi", Bet.Type.SCORES, event);

        Outcome outcome1 = new Outcome("0", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(1.75, outcome1,
                LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome2 = new Outcome("1", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(2.50, outcome2,
                LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome3 = new Outcome(">=2", bet);

        OutcomeOdd outcome3odd1 = new OutcomeOdd(5.78, outcome2,
                LocalDateTime.of(2016, 9, 27, 19, 0));

        outcome1.addOdd(outcome1odd1);
        outcome2.addOdd(outcome2odd1);
        outcome3.addOdd(outcome3odd1);

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);
        bet.addOutcome(outcome3);

        return bet;
    }

    private Bet getGoalsBetExample(FootballSportEvent event) {
        Bet bet = new Bet("", Bet.Type.GOALS, event);

        Outcome outcome1 = new Outcome("0", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(1.75, outcome1,
                LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome2 = new Outcome("1", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.25, outcome2,
                LocalDateTime.of(2016, 9, 27, 19, 0));

        Outcome outcome3 = new Outcome(">=2", bet);

        OutcomeOdd outcome3odd1 = new OutcomeOdd(1.05, outcome2,
                LocalDateTime.of(2016, 9, 27, 19, 0));

        outcome1.addOdd(outcome1odd1);
        outcome2.addOdd(outcome2odd1);
        outcome3.addOdd(outcome3odd1);

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);
        bet.addOutcome(outcome3);

        return bet;
    }

    private Bet getWinnerBetExample(FootballSportEvent event) {
        Bet bet = new Bet("", Bet.Type.WINNER, event);

        Outcome outcome1 = new Outcome("Southampton", bet);

        OutcomeOdd outcome1odd1 = new OutcomeOdd(4d, outcome1,
                LocalDateTime.of(2016, 9, 27, 19, 0),
                LocalDateTime.of(2016, 9, 30, 18, 59));

        OutcomeOdd outcome1odd2 = new OutcomeOdd(
                5d, outcome1, LocalDateTime.of(2016, 9, 30, 19, 0));

        Outcome outcome2 = new Outcome("Bournemoth", bet);

        OutcomeOdd outcome2odd1 = new OutcomeOdd(1.7, outcome2,
                LocalDateTime.of(2016, 9, 27, 19, 0),
                LocalDateTime.of(2016, 9, 30, 18, 59));

        OutcomeOdd outcome2odd2 = new OutcomeOdd(1.5, outcome2,
                LocalDateTime.of(2016, 9, 30, 19, 0));

        Outcome outcome3 = new Outcome("Draw", bet);

        OutcomeOdd outcome3odd1 = new OutcomeOdd(3d, outcome2,
                LocalDateTime.of(2016, 9, 27, 19, 0),
                LocalDateTime.of(2016, 9, 30, 18, 59));

        OutcomeOdd outcome3odd2 = new OutcomeOdd(3.5, outcome2,
                LocalDateTime.of(2016, 9, 30, 19, 0));

        outcome1.addOdds(Arrays.asList(outcome1odd1, outcome1odd2));
        outcome2.addOdds(Arrays.asList(outcome2odd1, outcome2odd2));
        outcome3.addOdds(Arrays.asList(outcome3odd1, outcome3odd2));

        bet.addOutcome(outcome1);
        bet.addOutcome(outcome2);
        bet.addOutcome(outcome3);

        return bet;
    }
}
