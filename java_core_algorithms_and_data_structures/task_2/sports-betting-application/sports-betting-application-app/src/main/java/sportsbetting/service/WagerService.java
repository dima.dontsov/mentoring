package sportsbetting.service;

import lombok.RequiredArgsConstructor;
import domain.Wager;
import domain.user.impl.Player;
import sportsbetting.exception.InsufficientFundsException;
import sportsbetting.repository.WagerRepository;

import java.util.List;

import static sportsbetting.validation.CommonValidator.isBetMoneyValid;

@RequiredArgsConstructor
public class WagerService {

    private final WagerRepository wagerRepository;

    public List<Wager> getPlayerWagers(Player player) {
        return wagerRepository.getPlayerWagers(player);
    }

    public void addWager(Wager wager) throws InsufficientFundsException {
        if (!isBetMoneyValid(wager.getAmount(), wager.getPlayer().getBalance())) {
            throw new InsufficientFundsException();
        }
        wagerRepository.add(wager);
    }
}
