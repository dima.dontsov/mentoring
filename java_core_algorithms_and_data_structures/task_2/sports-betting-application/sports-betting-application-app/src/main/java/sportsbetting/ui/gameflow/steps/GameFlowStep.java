package sportsbetting.ui.gameflow.steps;

import sportsbetting.service.GameService;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;

public interface GameFlowStep {
    Boolean process(GameService gameService, MessageProvider messageProvider, InfoCollector infoCollector);
}
