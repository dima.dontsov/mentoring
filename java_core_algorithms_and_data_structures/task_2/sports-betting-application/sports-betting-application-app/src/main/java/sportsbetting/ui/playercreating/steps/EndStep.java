package sportsbetting.ui.playercreating.steps;

import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;

public class EndStep implements PlayerCreationStep {

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        messageProvider.showMessage("\nDone creating new user!\n");
        return true;
    }
}
