package sportsbetting.ui.playercreating.impl;


import domain.user.impl.Player;
import sportsbetting.ui.infocollecting.ConsoleInfoCollector;
import sportsbetting.ui.messageprovision.impl.ConsoleMessageProvider;
import sportsbetting.ui.playercreating.PlayerBuilder;
import sportsbetting.ui.playercreating.steps.*;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * State
 */
public class ConsolePlayerBuilder implements PlayerBuilder {

    private final static List<Supplier<PlayerCreationStep>> CREATION_STEPS = Arrays.asList(
            NameStep::new,
            AccountNumberStep::new,
            PasswordStep::new,
            BirthDateStep::new,
            CurrencyStep::new,
            InitialMoneyAmountStep::new,
            EndStep::new
    );

    private ConsoleInfoCollector infoCollector;
    private ConsoleMessageProvider messageProvider;

    public ConsolePlayerBuilder(ConsoleInfoCollector infoCollector, ConsoleMessageProvider messageProvider) {
        this.infoCollector = infoCollector;
        this.messageProvider = messageProvider;
    }

    @Override
    public Player buildPlayer() {

        Player playerToBuild = new Player();

        CREATION_STEPS.forEach(stepSupplier -> processPlayerCreationStep(stepSupplier.get(), playerToBuild));

        return playerToBuild;
    }

    void processPlayerCreationStep(PlayerCreationStep step, Player playerToBuild) {
        boolean stepSucceed;

        do {
            stepSucceed = step.process(playerToBuild, this.infoCollector, this.messageProvider);
        } while (!stepSucceed);
    }
}
