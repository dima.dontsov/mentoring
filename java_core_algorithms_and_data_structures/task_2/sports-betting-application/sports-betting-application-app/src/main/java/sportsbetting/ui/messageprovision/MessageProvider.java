package sportsbetting.ui.messageprovision;

public interface MessageProvider {
    void showMessage(String message);
    void showErrorMessage(String errorMessage);
}
