package sportsbetting.ui.playercreating;

import domain.user.impl.Player;

public interface PlayerBuilder {
    Player buildPlayer();
}
