package sportsbetting.ui.messageprovision.impl;

import sportsbetting.ui.messageprovision.MessageProvider;

/**
 * Singleton
 */
public class ConsoleMessageProvider implements MessageProvider {

    private final static ConsoleMessageProvider INSTANCE = new ConsoleMessageProvider();

    private ConsoleMessageProvider() {

    }

    public static ConsoleMessageProvider getInstance() {
        return INSTANCE;
    }

    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        System.out.println(errorMessage);
    }
}
