package sportsbetting.ui.infocollecting;

import java.math.BigDecimal;

public interface InfoCollector {

    String obtainStringValue();

    BigDecimal obtainBigDecimalValue();
}
