package sportsbetting.ui.playercreating.steps;

import com.google.common.base.Preconditions;
import domain.user.impl.Player;
import sportsbetting.parsing.CommonParser;
import sportsbetting.ui.infocollecting.InfoCollector;
import sportsbetting.ui.messageprovision.MessageProvider;
import sportsbetting.util.Formatter;
import sportsbetting.validation.CommonValidator;

import java.time.LocalDate;

public class BirthDateStep implements PlayerCreationStep {

    @Override
    public boolean process(Player playerToBuild, InfoCollector infoCollector, MessageProvider messageProvider) {
        boolean stepSucceed = false;

        messageProvider.showMessage("\nEnter your date of birth (e.g. 1997.12.29):");
        String birthDateStr = infoCollector.obtainStringValue();
        try {
            LocalDate birthDate = CommonParser.parseDate(birthDateStr, Formatter.DATE_FORMATTER);

            Preconditions.checkArgument(CommonValidator.isBirthDateValid(birthDate), "Invalid birth date");

            playerToBuild.setDateOfBirth(birthDate);
            stepSucceed = true;

        } catch (IllegalArgumentException ex) {
            messageProvider.showErrorMessage(ex.getMessage());
        }

        return stepSucceed;
    }
}
