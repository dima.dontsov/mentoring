package sportsbetting.util;

import lombok.extern.log4j.Log4j2;
import domain.Currency;

@Log4j2
public class CurrencyUtil {

    public static Currency getIfExists(String str) {
        Currency result = null;

        try {
            result = Currency.valueOf(str.toUpperCase());
        } catch (IllegalArgumentException e) {
            log.debug(str + "may needs to be supported");
        }

        return result;
    }
}
