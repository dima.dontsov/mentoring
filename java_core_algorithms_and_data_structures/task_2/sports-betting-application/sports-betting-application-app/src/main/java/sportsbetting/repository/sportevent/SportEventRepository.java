package sportsbetting.repository.sportevent;

import lombok.Getter;
import lombok.NonNull;
import domain.sportevent.SportEvent;

import java.util.ArrayList;
import java.util.List;

public abstract class SportEventRepository<T extends SportEvent> {

    @Getter
    @NonNull
    protected final List<T> events = new ArrayList<>();

    public SportEventRepository() {
        this.prefillRepository();
    }

    protected abstract void prefillRepository();
}
