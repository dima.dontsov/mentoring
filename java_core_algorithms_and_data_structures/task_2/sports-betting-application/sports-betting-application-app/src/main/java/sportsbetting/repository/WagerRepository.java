package sportsbetting.repository;

import domain.Wager;
import domain.user.impl.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WagerRepository {

    private final List<Wager> wagers = new ArrayList<>();

    public void add(Wager wager) {
        this.wagers.add(wager);
    }

    public List<Wager> getPlayerWagers(Player player) {
        return wagers.stream().filter(w -> w.getPlayer().equals(player)).collect(Collectors.toList());
    }
}
