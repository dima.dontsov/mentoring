package domain.user.impl;

import lombok.Getter;
import lombok.Setter;
import domain.Currency;
import domain.user.User;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * The player who places odds
 */
public class Player extends User {

    @Getter
    @Setter
    private BigDecimal balance;

    @Getter
    @Setter
    private Currency currency;

    public static Player newDemoPlayer() {
        Player demoPlayer = new Player();

        demoPlayer.setAccountNumber("waytodanny@gmail.com");
        demoPlayer.setUsername("Daniel");
        demoPlayer.setDateOfBirth(LocalDate.ofYearDay(1997, 8));
        demoPlayer.setPassword("ping");

        demoPlayer.setBalance(BigDecimal.valueOf(23000));
        demoPlayer.setCurrency(Currency.UAH);

        demoPlayer.setDisabled(false);

        return demoPlayer;
    }

    @Override
    public String toString() {
        return "accountNumber='" + accountNumber + '\'' +
            ", username='" + username;
    }
}
