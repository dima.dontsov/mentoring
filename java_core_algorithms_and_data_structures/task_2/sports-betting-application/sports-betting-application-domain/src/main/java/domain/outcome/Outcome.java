package domain.outcome;

import lombok.Getter;
import lombok.NonNull;
import domain.Bet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * A possible outcome of a bet;
 * for example on a winner bet it is the winner teams name in the value,
 * on a goals bet it is the number of the goals in the value.
 */
public class Outcome {

    @Getter
    private final List<OutcomeOdd> odds;

    @Getter
    private String value;

    @Getter
    @NonNull
    private Bet bet;

    public Outcome(Outcome other) {
        this.odds = other.odds;
        this.value = other.value;
        this.bet = other.bet;
    }

    public Outcome(String value, Bet bet) {
        this.value = value;
        this.bet = bet;
        this.odds = new ArrayList<>();
    }

    public void addOdd(OutcomeOdd odd) {
        Optional.of(odd).ifPresent(odds::add);
    }

    public void addOdds(List<OutcomeOdd> odds) {
        Optional.of(odds).ifPresent(this.odds::addAll);
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Outcome outcome = (Outcome) o;
        return Objects.equals(value, outcome.value) &&
                Objects.equals(bet, outcome.bet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, bet);
    }
}
