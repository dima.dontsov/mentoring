package domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import domain.outcome.Outcome;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains multiple outcomes of a match, e.g.: 5 goals shot, Dima Ostritskyi scored 2 goals
 */
@NoArgsConstructor
public class Result {

    @Getter
    private final List<Outcome> outcomes = new ArrayList<>();
}
