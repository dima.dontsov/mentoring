package domain.outcome;

import domain.Bet;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * The odd of an outcome; stands for how many times multiplied your money that you bet on a match result if you win
 */
@RequiredArgsConstructor
public class OutcomeOdd {

    @Getter
    private final Double odd;

    @NonNull
    private final Outcome outcome;

    @NonNull
    private final LocalDateTime validFrom;

    @NonNull
    private final LocalDateTime validTo;

    /**
     * Without end date clarifying, end date becomes equal to it's event end date
     */
    public OutcomeOdd(Double odd, Outcome outcome, LocalDateTime validFrom) {
        this.odd = odd;
        this.outcome = new Outcome(outcome);
        this.validFrom = validFrom;
        this.validTo = outcome.getBet().getSportEvent().getEndDate();
    }

    @Override
    public String toString() {
        return odd + ", valid from " + validFrom/*.format(Formatter.DATE_TIME_FORMATTER)*/
                + " to " + validTo/*.format(Formatter.DATE_TIME_FORMATTER)*/;
    }

    public String getFullRecursiveInfo() {
        Bet parentBet = outcome.getBet();

        return "Bet on the '" + parentBet.getSportEvent().getTitle() + "' sport event: " +
                parentBet.getDescription() + " " + parentBet.getType().getDescription() +
                " " + outcome.getValue() +
                ". Odd is " + this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutcomeOdd that = (OutcomeOdd) o;
        return Objects.equals(outcome, that.outcome) &&
                Objects.equals(validFrom, that.validFrom) &&
                Objects.equals(validTo, that.validTo) &&
                Objects.equals(odd, that.odd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(odd, outcome, validFrom, validTo);
    }
}
