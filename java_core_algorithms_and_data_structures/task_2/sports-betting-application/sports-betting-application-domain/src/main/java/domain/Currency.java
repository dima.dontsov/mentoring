package domain;

/**
 * All currencies supportable by the application
 */
public enum Currency {
    UAH, USD, EUR;
}