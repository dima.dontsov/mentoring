package domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import domain.outcome.Outcome;
import domain.sportevent.SportEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * A bet has possible outcomes, outcomes can be predicted with wagers by the players
 */
public class Bet {

    @Getter
    private final List<Outcome> outcomes = new ArrayList<>();

    @Getter
    private final Type type;

    @Getter
    @NonNull
    private final SportEvent sportEvent;

    @Getter
    @Setter
    private String description;

    public Bet(Type type, SportEvent sportEvent) {
        this.type = type;
        this.sportEvent = sportEvent;
    }

    public Bet(String description, Type type, SportEvent sportEvent) {
        this(type, sportEvent);
        this.description = description;
    }

    public void addOutcome(Outcome outcome) {
        Optional.of(outcome).ifPresent(outcomes::add);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        outcomes.forEach(outcome -> {
            outcome.getOdds().forEach(odd -> {
                sb.append("\nBet on the '").append(sportEvent.getTitle()).append("' sport event: ");
                sb.append(description).append(" ").append(type.getDescription());
                sb.append(" ").append(outcome.getValue());
                sb.append(". Odd is ").append(odd);
            });
        });


        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bet bet = (Bet) o;
        return Objects.equals(sportEvent, bet.sportEvent) &&
                Objects.equals(description, bet.description) &&
                type == bet.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, sportEvent, description);
    }

    /**
     * All bet types supported by the application
     */
    @RequiredArgsConstructor
    public enum Type {
        WINNER("winner prediction -"),
        GOALS("number of scored goals will be"),
        SCORES("will score");

        @Getter
        private final String description;
    }
}
