package domain.sportevent;

import domain.Bet;
import domain.Result;
import domain.outcome.Outcome;
import domain.outcome.OutcomeOdd;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class SportEvent {

    protected final List<Bet> bets = new ArrayList<>();

    @Getter
    @Setter
    protected String title;

    @Getter
    @Setter
    protected LocalDateTime startDate;

    @Getter
    @Setter
    protected LocalDateTime endDate;

    @Getter
    @Setter
    protected Result result;

    public SportEvent(String title, LocalDateTime startDate, LocalDateTime endDate) {
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.result = new Result();
    }

    public void addBet(Bet bet) {
        if (isBetValid(bet)) {
            bets.add(bet);
        }
    }

    private boolean isBetValid(Bet bet) {
        return Objects.nonNull(bet) && !bets.contains(bet);
    }

    public void addBets(List<Bet> bets) {
        List<Bet> validBets = getValidBets(bets);
        bets.addAll(validBets);
    }

    private List<Bet> getValidBets(List<Bet> bets) {
        return bets.parallelStream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<Bet> getBets() {
        return bets;
    }

    public List<Outcome> getOutcomes() {
        return this.getBets().stream()
            .map(Bet::getOutcomes)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    public List<OutcomeOdd> getOutcomeOdds() {
        return this.getOutcomes().stream()
            .map(Outcome::getOdds)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SportEvent that = (SportEvent) o;
        return Objects.equals(title, that.title) &&
            Objects.equals(startDate, that.startDate) &&
            Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, startDate, endDate);
    }
}
