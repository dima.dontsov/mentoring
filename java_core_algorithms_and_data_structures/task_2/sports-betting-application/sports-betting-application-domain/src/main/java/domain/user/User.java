package domain.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Objects;

/**
 * A user of the application
 */
@AllArgsConstructor
@NoArgsConstructor
public abstract class User {

    @Getter
    @Setter
    protected String accountNumber;

    @Getter
    @Setter
    protected String username;

    @Getter
    @Setter
    protected String password;

    @Getter
    @Setter
    protected LocalDate dateOfBirth;

    @Getter
    @Setter
    protected boolean disabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(accountNumber, user.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountNumber);
    }
}
