package domain;

import lombok.Getter;
import lombok.Setter;
import domain.outcome.OutcomeOdd;
import domain.user.impl.Player;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * A wager placed by a Player on an outcome; It stores the odd, the amount and the currency of the bet,
 * the date and time when the bet is created and the state of having been processed or not.
 */
public class Wager {

    @Getter
    private Player player;
    @Getter
    private OutcomeOdd outcomeOdd;
    @Getter
    private BigDecimal amount;
    @Getter
    private Currency currency;
    @Getter
    private LocalDateTime timestamp;

    @Getter
    @Setter
    private boolean processed;

    @Getter
    @Setter
    private Boolean win;

    private Wager() {

    }

    public static Builder newBuilder() {
        return new Wager().new Builder();
    }

    /**
     * Builder
     */
    public class Builder {

        private Builder() {

        }

        public Builder setPlayer(Player player) {
            Wager.this.player = player;
            return this;
        }

        public Builder setOutcomeOdd(OutcomeOdd outcomeOdd) {
            Wager.this.outcomeOdd = outcomeOdd;
            return this;
        }

        public Builder setAmount(BigDecimal amount) {
            Wager.this.amount = amount;
            return this;
        }

        public Builder setCurrency(Currency currency) {
            Wager.this.currency = currency;
            return this;
        }

        public Builder setTimestamp(LocalDateTime timestamp) {
            Wager.this.timestamp = timestamp;
            return this;
        }

        public Wager build() {
            return Wager.this;
        }
    }
}
