package com.epam.multithcracking.decrypting.md5;

import com.epam.multithcracking.util.HashCalculator;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO static is evil, but methods really looks like static things
 */
public class MD5Util {

    private static HashCalculator HASH_CALCULATOR = new HashCalculator();

    private MD5Util() {

    }

    static String getResultingWord(int combIndex, char[] possChars, int wordLength) {
        return new String(getLetters(combIndex, possChars, wordLength));
    }

    /**
     * @return letters for a combination index and possible characters
     */
    static char[] getLetters(long combIndex, char[] possChars, int wordLength) {
        int[] letterIndexes = MD5Util.getLetterIndexes(combIndex, possChars, wordLength);
        char[] word = new char[wordLength];

        for (int i = 0; i < wordLength; i++) {
            word[i] = possChars[letterIndexes[i]];
        }

        return word;
    }

    /**
     * @return letter indexes for a combination index and possible characters
     */
    static int[] getLetterIndexes(long combIndex, char[] possChars, int wordLength) {
        int radix = possChars.length;
        int[] indexes = new int[wordLength];

        for (int i = wordLength - 1; i >= 0; i--) {
            if (combIndex > 0) {
                int rest = (int) (combIndex % radix);
                combIndex /= radix;
                indexes[i] = rest;
            } else {
                indexes[i] = 0;
            }
        }

        return indexes;
    }

    static String encrypt(String str) {
       return HASH_CALCULATOR.hash(str);
    }

    static boolean compareHashes(String s2, String s1) {
        return s2.equals(s1);
    }
}
