package com.epam.multithcracking;

import com.epam.multithcracking.decrypting.Decryptor;
import com.epam.multithcracking.decrypting.md5.MD5Decryptor;
import com.epam.multithcracking.util.HashCalculator;
import com.epam.multithcracking.util.Printer;
import com.epam.multithcracking.util.Timer;

import java.util.concurrent.Executors;

public class App {

    public static final String DECRYPTION_FAILURE_MSG = "Failed to decrypt";

    public static final String NO_RESULT = "";

    public static final int OPTIMAL_THREAD_COUNT = 128;

    private static final int MAX_WORD_LENGTH = 6;

    private static final char[] POSSIBLE_CHARS = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public static void main(String[] args) {
        String encrypted = new HashCalculator().hash("zzzzzz");

        Decryptor decryptor = new MD5Decryptor(
                Executors.newFixedThreadPool(OPTIMAL_THREAD_COUNT), POSSIBLE_CHARS, MAX_WORD_LENGTH);

        double msTime = Timer.getExecTime(() -> Printer.printDecryptionResult(decryptor.decrypt(encrypted)));

        Printer.printExecutionTime(msTime);
    }
}
