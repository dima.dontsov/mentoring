package com.epam.multithcracking.decrypting.md5;

import com.epam.multithcracking.App;
import com.epam.multithcracking.decrypting.Decryptor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.epam.multithcracking.App.DECRYPTION_FAILURE_MSG;
import static com.epam.multithcracking.App.NO_RESULT;

public class MD5Decryptor implements Decryptor {

    private ExecutorService pool;
    private char[] possChars;
    private int maxWordLength;

    private volatile AtomicBoolean isDecrypted = new AtomicBoolean(false);

    public MD5Decryptor(ExecutorService pool, char[] possChars, int maxWordLength) {
        this.pool = pool;
        this.possChars = possChars;
        this.maxWordLength = maxWordLength;
    }

    @Override
    public String decrypt(String encryptWord) {

        try {

            for (int wordLength = 1; wordLength <= maxWordLength; wordLength++) {
                String result = getResultForCurrentWordLength(wordLength, encryptWord);

                if (isResultPresent(result)) {
                    return result;
                }
            }

            return DECRYPTION_FAILURE_MSG;

        } finally {
            cleanupPool();
        }
    }

    private String getResultForCurrentWordLength(int wordLength, String enc) {
        String result = NO_RESULT;

        long combinations = (long) Math.pow(possChars.length, wordLength);
        int threadsCount = getOptimalThreadsCount();

        List<MD5WordSearcher> searchers = getEncryptWordSearchers(wordLength, enc, threadsCount, combinations);

        try {
            result = pool.invokeAny(searchers);
            isDecrypted.set(true);
        } catch (ExecutionException | InterruptedException e) {
            System.out.println("Not a word of " + wordLength + " length");
        }

        return result;
    }

    List<MD5WordSearcher> getEncryptWordSearchers(
            int wordLength, String encryptWord, int threadCount, long combinationCount) {

        List<MD5WordSearcher> searchers = new ArrayList<>(threadCount);

        long remainCombCount = combinationCount;
        int combPerThread = getCombinationsPerThread(threadCount, combinationCount);

        int startIndex = 0;
        while (remainCombCount > 0) {
            int combToTake = (int) getCombinationNumberToTake(combPerThread, remainCombCount);
            int endIndex = combToTake + startIndex;

            searchers.add(new MD5WordSearcher(encryptWord, wordLength, startIndex, endIndex, possChars, isDecrypted));

            startIndex += combToTake;
            remainCombCount -= combToTake;
        }

        return searchers;
    }

    private long getCombinationNumberToTake(int combPerThread, long remainCombCount) {
        return combPerThread <= remainCombCount ? combPerThread : remainCombCount;
    }

    int getCombinationsPerThread(int threadCount, long combinationCount) {
        double d = (double) combinationCount / threadCount;
        double ceil = Math.ceil(d);
        return (int) ceil;
    }

    int getOptimalThreadsCount() {
        return App.OPTIMAL_THREAD_COUNT;
    }

    private void cleanupPool() {
        pool.shutdown();
        try {
            if (!pool.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                pool.shutdownNow();
            }
        } catch (InterruptedException e) {
            pool.shutdownNow();
        }
    }

    private boolean isResultPresent(String result) {
        return !result.equals(NO_RESULT);
    }
}
