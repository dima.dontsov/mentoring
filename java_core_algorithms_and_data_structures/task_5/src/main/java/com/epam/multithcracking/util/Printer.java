package com.epam.multithcracking.util;

public class Printer {
    public static void printExecutionTime(double msTime) {
        System.out.println("Duration: " + msTime + " sec");
    }

    public static void printDecryptionResult(String decryptResult) {
        System.out.println("\nResult: " + decryptResult);
    }
}
