package com.epam.multithcracking.decrypting.md5;

import com.epam.multithcracking.exception.NoMatchFoundException;
import org.apache.commons.lang3.mutable.MutableBoolean;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public class MD5WordSearcher implements Callable<String> {

    private String encryptWord;
    private int wordLength;
    private int startIndex;
    private int endIndex;

    /**
     * TODO don't like that every object has same char [], we're just polluting memory
     * */
    private char [] possChars;
    private AtomicBoolean isAlreadyDecrypted;

    MD5WordSearcher(String encryptWord,
                    int wordLength,
                    int startIndex,
                    int endIndex,
                    char [] possChars,
                    AtomicBoolean isAlreadyDecrypted) {

        this.encryptWord = encryptWord;
        this.wordLength = wordLength;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.possChars = possChars;
        this.isAlreadyDecrypted = isAlreadyDecrypted;
    }

    @Override
    public String call() throws NoMatchFoundException {

        for (int combIndex = startIndex; combIndex < endIndex; combIndex++) {

            if (isAlreadyDecrypted.get()) {
                break;
            }

            String resultingWord = MD5Util.getResultingWord(combIndex, possChars, wordLength);

            String encryptResult = MD5Util.encrypt(resultingWord);

            if (MD5Util.compareHashes(encryptResult, encryptWord)) {
                return resultingWord;
            }
        }

        throw new NoMatchFoundException();
    }
}
