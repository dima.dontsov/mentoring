package com.epam.multithcracking.exception;

import java.util.concurrent.ExecutionException;

public class NoMatchFoundException extends ExecutionException {

    public NoMatchFoundException() {
        super();
    }
}
