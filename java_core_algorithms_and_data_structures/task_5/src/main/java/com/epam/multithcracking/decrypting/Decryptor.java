package com.epam.multithcracking.decrypting;

public interface Decryptor {
    String decrypt(String enc);
}
