package com.epam.multithcracking.util;

@FunctionalInterface
public interface Timer {

    /**
     * @return body execution time in seconds.
     * */
    static double getExecTime(Timer t) {

        long startTime = System.currentTimeMillis();
        t.execBody();
        long endTime = System.currentTimeMillis();

        long duration = (endTime - startTime);

        return (double) duration / 1000;
    }

    void execBody();
}
