package com.epam.training.calculator.operation.impl;

import com.epam.training.calculator.operation.BiOperation;

public class PlusOperation implements BiOperation {

    @Override
    public int execute(int a, int b) {
        return a + b;
    }

    @Override
    public int getPriority() {
        return 2;
    }
}
