package com.epam.training.calculator.util;

import com.epam.training.calculator.operation.BiOperation;
import com.epam.training.calculator.operation.impl.DivideOperation;
import com.epam.training.calculator.operation.impl.MinusOperation;
import com.epam.training.calculator.operation.impl.MultiplyOperation;
import com.epam.training.calculator.operation.impl.PlusOperation;

import java.util.HashMap;
import java.util.Map;

public interface CalculatorUtil {

    Map<Character, BiOperation> OPERATION_MAP = new HashMap<Character, BiOperation>() {{
        put('+', new PlusOperation());
        put('-', new MinusOperation());
        put('*', new MultiplyOperation());
        put('/', new DivideOperation());
    }};

}
