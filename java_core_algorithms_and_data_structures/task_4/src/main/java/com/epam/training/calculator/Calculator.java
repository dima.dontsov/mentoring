package com.epam.training.calculator;

import com.epam.training.calculator.operation.BiOperation;

import java.util.Map;

public abstract class Calculator {

    protected Map<Character, BiOperation> operationMap;

    public Calculator(Map<Character, BiOperation> operationMap){
        this.operationMap = operationMap;
    }

    public abstract int calculate(String s);
}
