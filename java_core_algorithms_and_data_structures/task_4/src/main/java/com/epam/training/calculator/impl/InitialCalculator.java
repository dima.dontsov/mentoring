package com.epam.training.calculator.impl;

import com.epam.training.calculator.Calculator;
import com.epam.training.calculator.operation.BiOperation;

import java.util.Map;
import java.util.regex.Pattern;

public class InitialCalculator extends Calculator {

    public InitialCalculator(Map<Character, BiOperation> operationMap) {
        super(operationMap);
    }

    @Override
    public int calculate(String s) {
        int[] operands = getConvertedOperands(getOperands(s));
        String[] operations = getOperations(s);

        int length = operations.length;
        int index = 1;
        while (index < length) {
            if ("*".equals(operations[index])) { // NEM!!! "*" == operationMap[i]
                operands[index - 1] = operands[index - 1] * operands[index];
                for (int j = index; j < length - 1; j++) {
                    operands[j] = operands[j + 1];
                    operations[j] = operations[j + 1];
                }
                length--;
            } else if ("/".equals(operations[index])) { // NEM!!! "/" == operationMap[i]
                operands[index - 1] = operands[index - 1] / operands[index];
                for (int j = index; j < length - 1; j++) {
                    operands[j] = operands[j + 1];
                    operations[j] = operations[j + 1];
                }
                length--;
            } else {
                index++;
            }
        }
        index = 1;
        while (index < length) {
            if ("+".equals(operations[index])) { // NEM!!! "+" == operationMap[i]
                operands[index - 1] = operands[index - 1] + operands[index];
                for (int j = index; j < length - 1; j++) {
                    operands[j] = operands[j + 1];
                    operations[j] = operations[j + 1];
                }
                length--;
            } else if ("-".equals(operations[index])) { // NEM!!! "-" == operationMap[i]
                operands[index - 1] = operands[index - 1] - operands[index];
                for (int j = index; j < length - 1; j++) {
                    operands[j] = operands[j + 1];
                    operations[j] = operations[j + 1];
                }
                length--;
            } else {
                index++;
            }
        }
        return operands[0];
    }

    private String[] getOperations(String expression) {
        return expression.split("[0-9]+");
    }

    private String[] getOperands(String expression) {
        return expression.split("[" + Pattern.quote("+-*/") + "]");
    }

    private int[] getConvertedOperands(String[] numbers) {
        int[] numbersConverted = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            numbersConverted[i] = Integer.valueOf(numbers[i]);
        }
        return numbersConverted;
    }
}

