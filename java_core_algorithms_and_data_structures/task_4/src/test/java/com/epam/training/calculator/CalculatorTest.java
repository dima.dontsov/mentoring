package com.epam.training.calculator;

import com.epam.training.calculator.impl.TwoStackCalculator;

import com.epam.training.calculator.util.CalculatorUtil;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    private final Calculator calculator = new TwoStackCalculator(CalculatorUtil.OPERATION_MAP);

    @Test(expected = IllegalArgumentException.class)
    public void calculate_throwsWhenWonNumberAsOperandPassed() {
        String expression = "a+b";
        calculator.calculate(expression);
    }

    @Test
    public void calculate_minusOperation() {
        String expression = "1-11";

        int expected = -10;
        int result = calculator.calculate(expression);

        assertEquals(expected, result);
    }

    @Test
    public void calculate_plusOperation() {
        String expression = "1+11";

        int expected = 12;
        int result = calculator.calculate(expression);

        assertEquals(expected, result);
    }

    @Test
    public void calculate_multiplyOperation() {
        String expression = "1+11";

        int expected = 12;
        int result = calculator.calculate(expression);

        assertEquals(expected, result);
    }

    @Test
    public void calculate_operationsDivision() {
        String expression = "121/11";

        int expected = 11;
        int result = calculator.calculate(expression);

        assertEquals(expected, result);
    }

    @Test
    public void calculate_operationsCombination() {
        String expression = "1+11*11/11-11";

        int expected = 1;
        int result = calculator.calculate(expression);

        assertEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculate_throwsIfInvalidOperationProvided() {
        String expression = "1^2";
        calculator.calculate(expression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculate_throwsIfInvalidExpressionProvided() {
        String expression = "1+++2";
        calculator.calculate(expression);
    }
}