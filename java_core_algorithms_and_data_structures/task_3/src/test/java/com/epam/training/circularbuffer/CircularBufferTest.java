package com.epam.training.circularbuffer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CircularBufferTest {

    private CircularBuffer<Integer> preparedBuffer;
    private CircularBuffer<Integer> rawBuffer;

    @Before
    public void prepare() {
        preparedBuffer = new CircularBuffer<>(5);

        preparedBuffer.put(1);
        preparedBuffer.put(2);
        preparedBuffer.put(3);
        preparedBuffer.put(4);
        preparedBuffer.get();
        preparedBuffer.get();
        preparedBuffer.put(5);
        preparedBuffer.put(6);
    }

    @After
    public void cleanup() {
        rawBuffer = null;
    }

    @Test(expected = RuntimeException.class)
    public void put_ThrowsIfBufferFull() {
        rawBuffer = new CircularBuffer<>(1);
        rawBuffer.put(1);
        rawBuffer.put(2);
    }

    @Test(expected = RuntimeException.class)
    public void get_ThrowsIfBufferEmpty() {
        rawBuffer = new CircularBuffer<>(1);
        rawBuffer.get();
    }

    @Test
    public void toObjectArray() {
        Object[] expected = {3, 4, 5, 6};
        Object[] result = preparedBuffer.toObjectArray();

        assertArrayEquals(expected, result);
    }

    @Test
    public void toArray() {
        Integer[] expected = {3, 4, 5, 6};
        Integer[] result = preparedBuffer.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    public void asList() {
        List<Integer> expected = Arrays.asList(3, 4, 5, 6);
        List<Integer> result = preparedBuffer.asList();

        assertThat(expected, is(result));
    }

    @Test
    public void addAll() {
        rawBuffer = new CircularBuffer<>(5);

        rawBuffer.put(1);
        rawBuffer.put(2);
        rawBuffer.put(3);
        rawBuffer.put(4);
        rawBuffer.get();
        rawBuffer.get();

        rawBuffer.addAll(Arrays.asList(5, 6, 7));

        Integer[] expected = {3, 4, 5, 6, 7};
        Integer[] result = rawBuffer.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    public void addAll_addToEmptyBuffer() {
        rawBuffer = new CircularBuffer<>(3);

        rawBuffer.addAll(Arrays.asList(5, 6, 7));

        Integer[] expected = {5, 6, 7};
        Integer[] result = rawBuffer.toArray();

        assertArrayEquals(expected, result);
    }


    @Test
    public void sort() {
        rawBuffer = new CircularBuffer<>(3);
        rawBuffer.addAll(Arrays.asList(8, 3, 5));

        rawBuffer.sort(Integer::compareTo);

        Integer[] expected = {3, 5, 8};
        Integer[] result = rawBuffer.toArray();

        assertArrayEquals(expected, result);
    }

    @Test(expected = NullPointerException.class)
    public void sort_throwsIfBufferContainsNulls() {
        rawBuffer = new CircularBuffer<>(4);
        rawBuffer.addAll(Arrays.asList(8, 3, 5));

        rawBuffer.sort(Integer::compareTo);
    }


    @Test
    public void isEmpty_returnTrueIfNoItemsPresent() {
        rawBuffer = new CircularBuffer<>(1);
        assertTrue(rawBuffer.isEmpty());
    }

    @Test
    public void isEmpty_returnFalseIfItemsPresent() {
        rawBuffer = new CircularBuffer<>(1);
        rawBuffer.put(1);
        assertFalse(rawBuffer.isEmpty());
    }
}