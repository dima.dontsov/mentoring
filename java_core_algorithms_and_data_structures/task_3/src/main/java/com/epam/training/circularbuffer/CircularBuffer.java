package com.epam.training.circularbuffer;

import com.epam.training.circularbuffer.exception.BufferOverflowException;
import com.epam.training.circularbuffer.exception.BufferUnderflowException;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CircularBuffer<T> {

    private final int capacity;

    private Object[] elements;

    private int size = 0;

    /**
     * Pointer to the next position to read
     */
    private int tail = 0;

    /**
     * Pointer to the next position to write
     */
    private int head = 0;

    public CircularBuffer(int capacity) {
        if (capacity < 1) {
            throw new IllegalArgumentException("Circular Buffer capacity can not be less than 1");
        }

        this.capacity = capacity;
        this.elements = new Object[capacity];
    }

    /**
     * Inserts item into the buffer at the position of the head.
     * After the insertion, the head points to the next index in the buffer.
     * The put method throws RUNTIME exception if the buffer is full.
     * The buffer is full when the head and the tail points to the same index, but the buffer is not empty.
     */
    public void put(T item) {
        checkForFullness();

        elements[head] = item;

        head = getNextIndex(head);

        size++;
    }

    private void checkForFullness() {
        if (tail == head && size > 0) {
            throw new BufferOverflowException();
        }
    }

    /**
     * Gets the value at the tail of the buffer.
     * After that, the tail points to the next index in the buffer.
     * The get method throws RUNTIME exception if the buffer is empty.
     * The buffer is empty if the head and the tail points to the same index, and the buffer is not full.
     */
    @SuppressWarnings("unchecked")
    public T get() {
        checkForEmptiness();

        Object item = elements[tail];

        tail = getNextIndex(tail);

        size--;

        return (T) item;
    }

    private void checkForEmptiness() {
        if (tail == head && isEmpty()) {
            throw new BufferUnderflowException();
        }
    }

    /**
     * This method returns the buffer as an Object array.
     * The capacity of the array should be equal to the current number of actual elements in the buffer. 
     * The first element should be the tail.
     */
    public Object[] toObjectArray() {
        Object[] bufferItems = new Object[size];

        int next = tail;

        for (int i = 0; i < bufferItems.length; i++) {
            bufferItems[i] = elements[next];
            next = getNextIndex(next);
        }

        return bufferItems;
    }

    /**
     * This method returns the buffer as a clazz T array. 
     * The capacity of the array should be equal to the current number of actual elements in the buffer. 
     * The first element should be the tail.
     */
    @SuppressWarnings("unchecked")
    public T[] toArray() {
        checkForEmptiness();

        T[] arr = (T[]) Array.newInstance(elements[tail].getClass(), 0);

        Object[] objArr = toObjectArray();

        return (T[]) Arrays.copyOf(toObjectArray(), objArr.length, arr.getClass());
    }

    /**
     * This method returns the buffer as a list with clazz T. 
     * The capacity of the list should be equal to the current number of actual elements in the buffer.
     * The first element should be the tail.
     */
    public List<T> asList() {
        return Arrays.asList(toArray());
    }

    /**
     * This method adds all elements from a given list (which can be clazz T and T's subclasses) to the buffer.
     * It throws an exception if there is not enough free space in the buffer to add all elements.
     */
    public void addAll(List<? extends T> inList) throws BufferOverflowException {
        int inListSize = inList.size();

        checkForFullness(inListSize);

        for (T inListItem : inList) {
            elements[head] = inListItem;
            head = getNextIndex(head);
        }

        size += inListSize;
    }

    private void checkForFullness(int inCollectionSize) {
        int emptyPlaces = capacity - size;

        if (inCollectionSize > emptyPlaces) {
            throw new BufferOverflowException();
        }
    }

    /**
     * This method sorts the buffer with the given comparator.
     * As the order of the elements change in the buffer,
     * the positions of the head and the tail should be maintained as well.
     */
    @SuppressWarnings("unchecked")
    public void sort(Comparator<? super T> c) {
        Arrays.sort((T[]) elements, c);
    }

    /**
     * This method returns true if the buffer is empty, false otherwise.
     */
    public boolean isEmpty() {
        return size == 0;
    }

    private int getNextIndex(int current) {
        int next = current;
        return ++next < capacity ? next : 0;
    }
}

