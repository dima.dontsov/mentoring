package com.epam.restaurant;

import com.epam.restaurant.client.Client;
import com.epam.restaurant.client.Person;
import com.epam.restaurant.menu.Menu;
import com.epam.restaurant.restaurant.Order;
import com.epam.restaurant.restaurant.Restaurant;
import com.epam.restaurant.restaurant.RestaurantImpl;
import com.epam.restaurant.restaurant.cook.FoodStorageDependCookImpl;
import com.epam.restaurant.restaurant.cook.FoodStorageDependCook;
import com.epam.restaurant.restaurant.foodStorage.FoodStorage;
import com.epam.restaurant.restaurant.foodStorage.FoodStorageImpl;

import java.util.List;

public class App {

    public static void main(String[] args) {

        FoodStorage foodStorage = new FoodStorageImpl();
        Restaurant restaurant = new RestaurantImpl(foodStorage, 1);

        FoodStorageDependCook cook = new FoodStorageDependCookImpl();

        restaurant.hireCook(cook);
        restaurant.startWorking();

        Client client = new Person("Dan", 100);

        Menu menu = restaurant.getMenu();

        List<String> availableProducts = menu.getAvailableProductNames();
        String hotDog = availableProducts.get(0);
        String chips = availableProducts.get(1);

        List<String> availableExtras = menu.getAvailableExtraNames();
        String ketchup = availableExtras.get(0);
        String mustard = availableExtras.get(1);

        Order firstOrder = Order.newBuilder().setClient(client).setProduct(hotDog).addExtra(ketchup).build();
        Order secondOrder = Order.newBuilder().setClient(client).setProduct(hotDog).addExtra(mustard).build();
        Order thirdOrder = Order.newBuilder().setClient(client).setProduct(hotDog).addExtra(ketchup).addExtra(mustard).build();

        Order fourthOrder = Order.newBuilder().setClient(client).setProduct(chips).addExtra(ketchup).build();
        Order fifthOrder = Order.newBuilder().setClient(client).setProduct(chips).addExtra(mustard).build();
        Order sixthOrder = Order.newBuilder().setClient(client).setProduct(chips).addExtra(ketchup).addExtra(mustard).build();

        restaurant.takeOrder(firstOrder);
        restaurant.takeOrder(secondOrder);
        restaurant.takeOrder(thirdOrder);

        restaurant.takeOrder(fourthOrder);
        restaurant.takeOrder(fifthOrder);
        restaurant.takeOrder(sixthOrder);
    }
}
