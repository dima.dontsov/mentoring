package com.epam.restaurant.restaurant.product;

public interface Product {
    Integer getResultantHappinessRate(Integer initialHappinessRate);
}
