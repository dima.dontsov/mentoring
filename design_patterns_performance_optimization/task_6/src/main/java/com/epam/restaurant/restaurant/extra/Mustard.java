package com.epam.restaurant.restaurant.extra;

import com.epam.restaurant.restaurant.product.Product;

public class Mustard extends Extra {

    public Mustard(Product productToDecorate) {
        super(productToDecorate);
    }

    @Override
    public Integer getResultantHappinessRate(Integer initialHappinessRate) {
        return initialHappinessRate + 1;
    }
}
