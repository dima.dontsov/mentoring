package com.epam.restaurant.restaurant.extra.extraConstructor;

import com.epam.restaurant.restaurant.product.Product;

/**
 * FactoryMethod
 * */
public abstract class ExtraConstructor {
    public abstract Product createExtra(Product productToDecorate);
}
