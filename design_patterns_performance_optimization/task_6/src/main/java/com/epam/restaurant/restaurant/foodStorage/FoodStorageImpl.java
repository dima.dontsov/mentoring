package com.epam.restaurant.restaurant.foodStorage;

import com.epam.restaurant.restaurant.extra.extraConstructor.ExtraConstructor;
import com.epam.restaurant.restaurant.extra.extraConstructor.impl.KetchupConstructor;
import com.epam.restaurant.restaurant.extra.extraConstructor.impl.MustardConstructor;
import com.epam.restaurant.restaurant.product.Product;
import com.epam.restaurant.restaurant.product.productConstructor.ProductConstructor;
import com.epam.restaurant.restaurant.product.productConstructor.impl.ChipsConstructor;
import com.epam.restaurant.restaurant.product.productConstructor.impl.HotDogConstructor;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FoodStorageImpl implements FoodStorage {

    private Map<String, ProductConstructor> products = new LinkedHashMap<>();
    private Map<String, ExtraConstructor> extras = new LinkedHashMap<>();

    {
        this.products.put("hot-dog", new HotDogConstructor());
        this.products.put("chips", new ChipsConstructor());
    }

    {
        this.extras.put("ketchup", new KetchupConstructor());
        this.extras.put("mustard", new MustardConstructor());
    }

    @Override
    public List<String> getAvailableProductNames() {
        return new ArrayList<>(this.products.keySet());
    }

    @Override
    public List<String> getAvailableExtraNames() {
        return new ArrayList<>(this.extras.keySet());
    }

    @Override
    public ProductConstructor getProductConstructor(String productName) {
        return this.products.get(productName);
    }

    @Override
    public ExtraConstructor getExtraConstructor(String extraName) {
        return this.extras.get(extraName);
    }
}
