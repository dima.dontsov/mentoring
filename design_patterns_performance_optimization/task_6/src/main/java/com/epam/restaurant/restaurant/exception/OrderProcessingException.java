package com.epam.restaurant.restaurant.exception;

public class OrderProcessingException extends RuntimeException {

    public OrderProcessingException(Throwable cause) {
        super(cause);
    }
}
