package com.epam.restaurant.restaurant;

import com.epam.restaurant.menu.Menu;
import com.epam.restaurant.restaurant.cook.FoodStorageDependCook;

/**
 * Facade
 */
public interface Restaurant {
    void takeOrder(Order order);

    Menu getMenu();

    void hireCook(FoodStorageDependCook cook);

    void startWorking();
}
