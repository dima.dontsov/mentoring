package com.epam.restaurant.restaurant.extra.extraConstructor.impl;

import com.epam.restaurant.restaurant.extra.Ketchup;
import com.epam.restaurant.restaurant.extra.extraConstructor.ExtraConstructor;
import com.epam.restaurant.restaurant.product.Product;

public class KetchupConstructor extends ExtraConstructor {

    @Override
    public Product createExtra(Product productToDecorate) {
        return new Ketchup(productToDecorate);
    }
}
