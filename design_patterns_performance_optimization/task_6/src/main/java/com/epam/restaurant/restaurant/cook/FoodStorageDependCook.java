package com.epam.restaurant.restaurant.cook;

import com.epam.restaurant.restaurant.Order;
import com.epam.restaurant.restaurant.extra.extraConstructor.ExtraConstructor;
import com.epam.restaurant.restaurant.foodStorage.FoodStorage;
import com.epam.restaurant.restaurant.product.Product;
import com.epam.restaurant.restaurant.product.productConstructor.ProductConstructor;

public abstract class FoodStorageDependCook implements Cook{
    private FoodStorage foodStorage;

    public void setFoodStorage(FoodStorage foodStorage) {
        this.foodStorage = foodStorage;
    }

    protected Product cookOrderProduct(Order order) {
        ProductConstructor productConstructor = this.foodStorage.getProductConstructor(order.getProductName());
        Product resultingProduct = productConstructor.createProduct();

        for (String extraName : order.getExtraNames()) {
            ExtraConstructor extraConstructor = this.foodStorage.getExtraConstructor(extraName);
            resultingProduct = extraConstructor.createExtra(resultingProduct);
        }

        return resultingProduct;
    }
}
