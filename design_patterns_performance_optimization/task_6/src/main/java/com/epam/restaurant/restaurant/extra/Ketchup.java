package com.epam.restaurant.restaurant.extra;

import com.epam.restaurant.restaurant.product.Product;


public class Ketchup extends Extra {

    public Ketchup(Product productToDecorate) {
        super(productToDecorate);
    }

    @Override
    public Integer getResultantHappinessRate(Integer initialHappinessRate) {
        int diff = this.product.getResultantHappinessRate(initialHappinessRate) - initialHappinessRate;
        return initialHappinessRate + diff * 2;
    }
}
