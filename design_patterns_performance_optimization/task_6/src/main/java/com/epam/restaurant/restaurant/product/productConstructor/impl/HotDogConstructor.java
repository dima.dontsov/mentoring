package com.epam.restaurant.restaurant.product.productConstructor.impl;

import com.epam.restaurant.restaurant.product.HotDog;
import com.epam.restaurant.restaurant.product.Product;
import com.epam.restaurant.restaurant.product.productConstructor.ProductConstructor;

public class HotDogConstructor extends ProductConstructor {

    @Override
    public Product createProduct() {
        return new HotDog();
    }
}
