package com.epam.restaurant.restaurant;

import com.epam.restaurant.client.Client;

import java.util.*;

public class Order extends Observable {

    private Client client;
    private String productName;
    private Set<String> extraNames = new LinkedHashSet<>();

    private Order() {

    }

    public static Builder newBuilder() {
        return new Order().new Builder();
    }

    public Client getClient() {
        return client;
    }

    public String getProductName() {
        return productName;
    }

    public List<String> getExtraNames() {
        return new ArrayList<>(extraNames);
    }

    public void setReady(){
        setChanged();
    }

    /**
     * Builder
     */
    public class Builder {

        private Builder() {

        }

        public Builder setClient(Client client) {
            Order.this.client = client;
            return this;
        }

        public Builder setProduct(String productName) {
            Order.this.productName = productName;
            return this;
        }

        public Builder addExtra(String extraName) {
            Order.this.extraNames.add(extraName);
            return this;
        }

        public Order build() {
            return Order.this;
        }
    }
}
