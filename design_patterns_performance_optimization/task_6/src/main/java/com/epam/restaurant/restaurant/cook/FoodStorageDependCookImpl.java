package com.epam.restaurant.restaurant.cook;

import com.epam.restaurant.restaurant.Order;
import com.epam.restaurant.restaurant.product.Product;

public class FoodStorageDependCookImpl extends FoodStorageDependCook {

    public void prepareProduct(Order order) {
        Product product = cookOrderProduct(order);
        order.setReady();
        order.notifyObservers(product);
    }
}
