package com.epam.restaurant.client;

import com.epam.restaurant.restaurant.product.Product;

public class Person implements Client {
    private String name;
    private Integer happinessRate;

    public Person(String name, Integer happinessRate) {
        this.name = name;
        this.happinessRate = happinessRate;
    }

    @Override
    public Integer getHappinessRate() {
        return happinessRate;
    }


    @Override
    public void consume(Product product) {
        System.out.println(this.name + " obtained ordered product having happiness - " + this.happinessRate);

        this.happinessRate = product.getResultantHappinessRate(this.getHappinessRate());

        System.out.println(this.name + " consumed an ordered product and now has happiness: " + this.happinessRate);
    }
}
