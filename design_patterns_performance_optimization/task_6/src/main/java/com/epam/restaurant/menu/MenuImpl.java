package com.epam.restaurant.menu;

import com.epam.restaurant.restaurant.foodStorage.FoodStorage;

import java.util.*;

public class MenuImpl implements Menu {

    private List<String> availableProducts = new ArrayList<>();
    private List<String> availableExtras = new ArrayList<>();

    public MenuImpl(FoodStorage foodStorage) {
        this.availableProducts.addAll(foodStorage.getAvailableProductNames());
        this.availableExtras.addAll(foodStorage.getAvailableExtraNames());
    }

    @Override
    public List<String> getAvailableProductNames() {
        return new ArrayList<>(this.availableProducts);
    }

    @Override
    public List<String> getAvailableExtraNames() {
        return new ArrayList<>(this.availableExtras);
    }
}
