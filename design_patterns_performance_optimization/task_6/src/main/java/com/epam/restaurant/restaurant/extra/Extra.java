package com.epam.restaurant.restaurant.extra;

import com.epam.restaurant.restaurant.product.Product;

/**
 * Decorator
 * */
public abstract class Extra implements Product {

    protected Product product;

    public Extra(Product product) {
        this.product = product;
    }
}
