package com.epam.restaurant.restaurant.foodStorage;

import com.epam.restaurant.restaurant.extra.extraConstructor.ExtraConstructor;
import com.epam.restaurant.restaurant.product.Product;
import com.epam.restaurant.restaurant.product.productConstructor.ProductConstructor;

import java.util.List;

public interface FoodStorage {
    List<String> getAvailableProductNames();

    List<String> getAvailableExtraNames();

    ProductConstructor getProductConstructor(String productName);

    ExtraConstructor getExtraConstructor(String extraName);
}
