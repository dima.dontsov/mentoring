package com.epam.restaurant.restaurant.product;

public class HotDog implements Product {

    @Override
    public Integer getResultantHappinessRate(Integer initialHappinessRate) {
        return initialHappinessRate + 2;
    }
}
