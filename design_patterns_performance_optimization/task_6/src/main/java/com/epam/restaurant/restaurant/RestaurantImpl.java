package com.epam.restaurant.restaurant;

import com.epam.restaurant.menu.Menu;
import com.epam.restaurant.menu.MenuImpl;
import com.epam.restaurant.restaurant.cook.FoodStorageDependCook;
import com.epam.restaurant.restaurant.exception.OrderProcessingException;
import com.epam.restaurant.restaurant.foodStorage.FoodStorage;
import com.epam.restaurant.restaurant.notification.OrderStateController;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RestaurantImpl implements Restaurant {

    private FoodStorage foodStorage;
    private FoodStorageDependCook cook;
    private Menu menu;

    private ExecutorService cooksPool;
    private OrderStateController orderStateController = new OrderStateController();
    private BlockingQueue<Order> orderQueue = new ArrayBlockingQueue<>(100);

    public RestaurantImpl(FoodStorage foodStorage, int cooksNumber) {
        this.foodStorage = foodStorage;

        this.cooksPool = Executors.newFixedThreadPool(cooksNumber);
        this.menu = new MenuImpl(foodStorage);

        startWorking();
    }

    @Override
    public void takeOrder(Order order) {
        try {

            order.addObserver(orderStateController);
            orderQueue.put(order);

        } catch (InterruptedException e) {
           throw new OrderProcessingException(e);
        }
    }

    @Override
    public Menu getMenu() {
        return this.menu;
    }

    @Override
    public void hireCook(FoodStorageDependCook cook) {
        this.cook = cook;
        this.cook.setFoodStorage(this.foodStorage);
    }

    @Override
    public void startWorking() {
        this.cooksPool.submit(this::startOrderQueueProcessing);
    }

    private void startOrderQueueProcessing() {
        try {

            while (!Thread.interrupted()) {
                Order order = orderQueue.take();
                cook.prepareProduct(order);
            }

        } catch (InterruptedException e) {
            throw new OrderProcessingException(e);
        }
    }
}
