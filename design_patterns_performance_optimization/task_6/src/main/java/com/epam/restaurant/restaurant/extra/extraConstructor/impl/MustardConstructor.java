package com.epam.restaurant.restaurant.extra.extraConstructor.impl;

import com.epam.restaurant.restaurant.extra.Mustard;
import com.epam.restaurant.restaurant.extra.extraConstructor.ExtraConstructor;
import com.epam.restaurant.restaurant.product.Product;

public class MustardConstructor extends ExtraConstructor {

    @Override
    public Product createExtra(Product productToDecorate) {
        return new Mustard(productToDecorate);
    }
}
