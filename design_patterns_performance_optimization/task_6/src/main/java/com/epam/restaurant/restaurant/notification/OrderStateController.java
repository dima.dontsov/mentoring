package com.epam.restaurant.restaurant.notification;

import com.epam.restaurant.restaurant.Order;
import com.epam.restaurant.restaurant.product.Product;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Observer
 */
public class OrderStateController implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        Order order = (Order) o;
        Product product = (Product) arg;
        order.getClient().consume(product);
    }
}
