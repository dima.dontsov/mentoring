package com.epam.restaurant.restaurant.cook;

import com.epam.restaurant.restaurant.Order;
import com.epam.restaurant.restaurant.foodStorage.FoodStorage;
import com.epam.restaurant.restaurant.notification.OrderStateController;
import com.epam.restaurant.restaurant.product.productConstructor.ProductConstructor;

public interface Cook  {
    void prepareProduct(Order order);
}
