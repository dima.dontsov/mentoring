package com.epam.restaurant.restaurant.product.productConstructor;

import com.epam.restaurant.restaurant.product.Product;

/**
 * FactoryMethod
 * */
public abstract class ProductConstructor {

    public abstract Product createProduct();
}
