package com.epam.restaurant.restaurant.product.productConstructor.impl;

import com.epam.restaurant.restaurant.product.Chips;
import com.epam.restaurant.restaurant.product.Product;
import com.epam.restaurant.restaurant.product.productConstructor.ProductConstructor;

public class ChipsConstructor extends ProductConstructor {

    @Override
    public Product createProduct() {
        return new Chips();
    }
}
