package com.epam.restaurant.menu;

import java.util.List;

public interface Menu {
     List<String> getAvailableProductNames();

     List<String> getAvailableExtraNames();
}
