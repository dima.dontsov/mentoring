package com.epam.restaurant.client;

import com.epam.restaurant.restaurant.notification.OrderExpectant;

public interface Client extends OrderExpectant {
    Integer getHappinessRate();
}
