package com.epam.restaurant.restaurant.notification;

import com.epam.restaurant.restaurant.product.Product;

public interface OrderExpectant {
    void consume(Product orderProduct);
}
