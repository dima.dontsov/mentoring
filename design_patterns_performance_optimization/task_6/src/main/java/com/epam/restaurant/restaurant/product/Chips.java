package com.epam.restaurant.restaurant.product;

public class Chips implements Product {

    @Override
    public Integer getResultantHappinessRate(Integer initialHappinessRate) {
        return initialHappinessRate + initialHappinessRate * 5 / 100;
    }
}
