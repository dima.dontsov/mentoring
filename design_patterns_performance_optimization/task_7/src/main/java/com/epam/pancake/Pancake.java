package com.epam.pancake;

import java.util.Objects;

public class Pancake {

    private static final double MIN_DIFF = 0.1;

    private final String name;

    private final double diameter;

    public Pancake(String name, double diameter) {
        super();
        this.name = name;
        this.diameter = diameter;
    }

    public String getName() {
        return name;
    }

    public double getDiameter() {
        return diameter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pancake)) return false;
        Pancake pancake = (Pancake) o;
        return Math.round(pancake.diameter / MIN_DIFF) == Math.round(this.diameter / MIN_DIFF) &&
                Objects.equals(name, pancake.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, Math.round(this.diameter / MIN_DIFF));
    }
}
