package com.epam.training.exercise1.refactored.util;

/**
 * Utility class for calculations.
 */
public final class Calculations {
    private Calculations() {
    }

    /**
     * gets is given x is in width bound.
     *
     * @param x     x
     * @param width bound
     * @return is given x is in width bound
     */
    public static boolean isXInRange(int x, int width) {
        return x >= 0 && x <= width;
    }

    /**
     * gets is given y is in height bound.
     *
     * @param y      y
     * @param height bound
     * @return is given y is in height bound
     */
    public static boolean isYInRange(int y, int height) {
        return y >= 0 && y <= height;
    }
}
