package com.epam.training.exercise3.refactored;

import com.epam.training.exercise3.refactored.domain.EightQueensSolver;
import com.epam.training.exercise3.refactored.domain.SolutionKeeper;
import com.epam.training.exercise3.refactored.view.BoardPrinter;
import com.epam.training.exercise3.refactored.view.SolutionPrinter;

/**
 * Main application class.
 */
public final class App {

    private static final EightQueensSolver SOLVER = new EightQueensSolver();
    private static final BoardPrinter BOARD_PRINTER = new BoardPrinter();
    private static final SolutionPrinter SOLUTION_PRINTER = new SolutionPrinter(BOARD_PRINTER);

    private App() {

    }

    /**
     * Main method.
     *
     * @param args args
     */
    public static void main(String[] args) {
        SolutionKeeper solutionKeeper = SOLVER.solve(AppConfig.DEFAULT_BOARD_SIZE);
        SOLUTION_PRINTER.print(solutionKeeper);
    }
}
