package com.epam.training.exercise1.refactored;

import com.epam.training.exercise1.refactored.domain.ConsoleImageTransformer;
import com.epam.training.exercise1.refactored.domain.Image;

/**
 * Main application.
 */
public final class App {

    private static ConsoleImageTransformer consoleImageTransformer = new ConsoleImageTransformer();

    private App() {
    }

    /**
     * Main method.
     *
     * @param args args
     */
    public static void main(String[] args) {
        Image image = Image.fromUrl("pair_hiking.png");
        consoleImageTransformer.transform(image);
    }
}
