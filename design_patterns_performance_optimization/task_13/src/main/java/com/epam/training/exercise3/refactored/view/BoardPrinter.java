package com.epam.training.exercise3.refactored.view;

import com.epam.training.exercise3.refactored.domain.Board;
import com.epam.training.util.Printer;

/**
 * Board printer.
 * */
public class BoardPrinter implements Printer<Board> {

    @Override
    public void print(Board board) {
        for (char[] row : board.getInnerBoard()) {
            for (char cell : row) {
                System.out.print(cell);
            }
            System.out.println();
        }
    }
}
