package com.epam.training.exercise3.refactored.view;

import com.epam.training.exercise3.refactored.domain.Board;
import com.epam.training.exercise3.refactored.domain.SolutionKeeper;
import com.epam.training.util.Printer;

import java.util.List;

/**
 * Prints 8 queens solutions.
 * */
public class SolutionPrinter implements Printer<SolutionKeeper> {

    private BoardPrinter boardPrinter;

    public SolutionPrinter(BoardPrinter boardPrinter) {
        this.boardPrinter = boardPrinter;
    }

    @Override
    public void print(SolutionKeeper solutionKeeper) {
        List<Board> solutions = solutionKeeper.getSolutions();
        for (int i = 0; i < solutions.size(); i++) {
            System.out.println("\nSolution " + (i + 1));
            boardPrinter.print(solutions.get(i));
        }
    }
}

