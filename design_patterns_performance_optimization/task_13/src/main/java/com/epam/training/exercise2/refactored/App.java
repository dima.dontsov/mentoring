package com.epam.training.exercise2.refactored;

import com.epam.training.exercise2.refactored.domain.CustomDate;
import com.epam.training.exercise2.refactored.view.CustomDatePrinter;

import java.util.Date;


/**
 * Main application class.
 */
public final class App {

    private static final int TEST_YEAR = 2014;
    private static final int TEST_MONTH = 10;
    private static final int TEST_DAY = 10;
    private static final CustomDate CUSTOM_DATE = new CustomDate();
    private static final CustomDatePrinter PRINTER = new CustomDatePrinter();
    private App() {
    }

    /**
     * Main method.
     *
     * @param args args
     */
    public static void main(String[] args) {
        Date date = new Date();
        PRINTER.print(date);
        Date dayBefore = CUSTOM_DATE.getDayBefore(date);
        PRINTER.print(dayBefore);
        Date newDate = CUSTOM_DATE.getAsDate(TEST_YEAR, TEST_MONTH, TEST_DAY);
        PRINTER.print(newDate);
    }
}
