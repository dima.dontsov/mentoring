package com.epam.training.exercise3.refactored.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Keeper for Chess solutions.
 */
public class SolutionKeeper {
    private List<Board> solutions = new ArrayList<>();

    public List<Board> getSolutions() {
        return new ArrayList<>(solutions);
    }

    /**
     * add solution.
     *
     * @param board solution
     */
    public void add(Board board) {
        this.solutions.add(board);
    }
}
