package com.epam.training.exercise1.refactored.domain;


import java.awt.image.BufferedImage;

import com.epam.training.exercise1.refactored.util.Calculations;
import com.epam.training.exercise1.refactored.util.ImageLoader;
import com.google.common.base.Preconditions;

/**
 * Image.
 */
public final class Image {
    private static final int LAST_BYTE = 0xFF;
    private static final int BYTE = 8;
    private static final int TWO_BYTES = 16;

    private static ImageLoader imageLoader = new ImageLoader();
    private BufferedImage image;

    private Image(BufferedImage image) {
        this.image = image;
    }

    /**
     * get Image instance by url.
     *
     * @param url url
     * @return Image if found
     */
    public static Image fromUrl(String url) {
        BufferedImage bufferedImage = imageLoader.loadImage(url);
        return new Image(bufferedImage);
    }

    public int getHeight() {
        return image.getHeight();
    }

    public int getWidth() {
        return image.getWidth();
    }

    /**
     * gets Image coordinate intensity.
     *
     * @param coordinate coordinate
     * @return intensity value
     */
    public int getIntensity(Coordinate coordinate) {
        return getRed(coordinate) + getBlue(coordinate) + getGreen(coordinate);
    }

    private int getRed(Coordinate coordinate) {
        int rgbValue = getRGB(coordinate);
        return (rgbValue >> TWO_BYTES) & LAST_BYTE;
    }

    private int getBlue(Coordinate coordinate) {
        int rgbValue = getRGB(coordinate);
        return rgbValue & LAST_BYTE;
    }

    private int getGreen(Coordinate coordinate) {
        int rgbValue = getRGB(coordinate);
        return (rgbValue >> BYTE) & LAST_BYTE;
    }

    private int getRGB(Coordinate coordinate) {
        int x = coordinate.getX();
        int y = coordinate.getY();

        int imageWidth = image.getWidth();
        int imageHeight = image.getHeight();

        Preconditions.checkArgument(Calculations.isXInRange(x, imageWidth),
            "Coordinate x of value %s us out of range: 0.." + imageWidth, x);
        Preconditions.checkArgument(Calculations.isYInRange(y, imageHeight),
            "Coordinate y of value %s is out of range: 0.." + imageHeight, y);

        return image.getRGB(x, y);
    }
}
