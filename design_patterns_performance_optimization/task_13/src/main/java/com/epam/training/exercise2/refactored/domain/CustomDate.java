package com.epam.training.exercise2.refactored.domain;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * Task targeted custom date.
 */
public class CustomDate {

    /**
     * gets day before the given date.
     *
     * @param date given date
     * @return day before
     */
    public Date getDayBefore(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    /**
     * gets date object by given params.
     *
     * @param year  year
     * @param month month
     * @param day   day
     * @return date
     */
    public Date getAsDate(int year, int month, int day) {
        return asDate(LocalDate.of(year, month, day));
    }

    private Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
}
