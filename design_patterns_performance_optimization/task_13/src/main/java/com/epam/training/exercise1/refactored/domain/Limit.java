package com.epam.training.exercise1.refactored.domain;

/**
 * Limit with min and max.
 */
public final class Limit {
    private int min;
    private int max;

    private Limit() {

    }

    /**
     * gets new instance.
     *
     * @return builder
     */
    public static Builder newInstance() {
        return new Limit().new Builder();
    }

    /**
     * gets limit's min.
     *
     * @return min
     */
    public int getMin() {
        return min;
    }

    /**
     * gets limit's max.
     *
     * @return max
     */
    public int getMax() {
        return max;
    }

    /**
     * Limit builder.
     */
    public class Builder {
        /**
         * sets min.
         *
         * @param min min
         * @return builder
         */
        public Builder from(int min) {
            Limit.this.min = min;
            return this;
        }

        /**
         * sets max.
         *
         * @param max max
         * @return builder
         */
        public Builder to(int max) {
            Limit.this.max = max;
            return this;
        }

        /**
         * builds Limit instance.
         *
         * @return limit
         */
        public Limit build() {
            return Limit.this;
        }
    }
}
