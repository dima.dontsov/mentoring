package com.epam.training.util;

/**
 * Printer interface.
 */
public interface Printer<T> {

    void print(T t);
}
