package com.epam.training.exercise2.refactored.view;

import com.epam.training.util.Printer;

import java.util.Date;

/**
 * Custom date printer.
 * */
public class CustomDatePrinter implements Printer<Date> {

    @Override
    public void print(Date date) {
        System.out.println(date);
    }
}
