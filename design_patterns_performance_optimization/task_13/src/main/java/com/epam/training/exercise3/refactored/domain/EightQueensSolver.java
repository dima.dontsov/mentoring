package com.epam.training.exercise3.refactored.domain;

import com.epam.training.exercise3.refactored.AppConfig;

/**
 * Solver of 8 queens problem.
 */
public class EightQueensSolver {

    private SolutionKeeper solutionKeeper = new SolutionKeeper();

    /**
     * Solves 8 queens problem wth given board dimension.
     *
     * @param dimension board dimension
     * @return solutions
     */
    public SolutionKeeper solve(int dimension) {
        Board board = Board.withDimension(dimension);
        solveQueens(board, solutionKeeper);
        return solutionKeeper;
    }

    private void solveQueens(Board board, SolutionKeeper solutionKeeper) {
        solveQueens(board, 0, solutionKeeper);
    }

    private void solveQueens(Board board, int col, SolutionKeeper solutionKeeper) {
        if (isLastColumn(col, board)) {
            processLastColumn(board.asCharArray(), solutionKeeper);
        } else {
            canQueenBeSafe(board.asCharArray(), col, solutionKeeper);
        }
    }

    private void processLastColumn(char[][] board, SolutionKeeper solutions) {
        int boardLength = board.length;

        char[][] copy = new char[boardLength][boardLength];
        for (int i = 0; i < boardLength; i++) {
            System.arraycopy(board[i], 0, copy[i], 0, boardLength);
        }

        solutions.add(Board.asBoard(copy));
    }

    private void canQueenBeSafe(char[][] board, int col, SolutionKeeper solutions) {
        for (int row = 0; row < board.length; row++) {
            board[row][col] = AppConfig.QUEEN_CELL;

            boolean queenIsSafe = isThereOtherQueensOnBoard(board);
            queenIsSafe = isOffsetFree(board, queenIsSafe);
            queenIsSafe = isReverseOffsetFree(board, queenIsSafe);

            if (queenIsSafe) {
                solveQueens(Board.asBoard(board), col + 1, solutions);
            }
            board[row][col] = AppConfig.EMPTY_CELL;
        }
    }

    private boolean isReverseOffsetFree(char[][] board, boolean initialIsQueenSafe) {
        boolean isQueenSafe = initialIsQueenSafe;

        for (int offset = -board.length; offset < board.length; offset++) {
            boolean found = false;
            for (int i = 0; i < board.length; i++) {
                if (isInBoardBounds(i, board.length - offset - i - 1, board) && board[i][board.length - offset - i - 1] == AppConfig.QUEEN_CELL) {
                    if (found) {
                        isQueenSafe = false;
                    }
                    found = true;
                }
            }
        }

        return isQueenSafe;
    }

    private boolean isOffsetFree(char[][] board, boolean initialIsQueenSafe) {
        boolean isQueenSafe = initialIsQueenSafe;

        for (int offset = -board.length; offset < board.length; offset++) {
            boolean found = false;
            for (int i = 0; i < board.length; i++) {
                if (isInBoardBounds(i, i + offset, board)) {
                    if (board[i][i + offset] == AppConfig.QUEEN_CELL) {
                        if (found) {
                            isQueenSafe = false;
                        }
                        found = true;
                    }
                }
            }
        }

        return isQueenSafe;
    }

    private boolean isThereOtherQueensOnBoard(char[][] board) {
        boolean isQueenSafe = true;

        for (int i = 0; i < board.length; i++) {
            boolean found = false;
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == AppConfig.QUEEN_CELL) {
                    if (found) {
                        isQueenSafe = false;
                    }
                    found = true;
                }
            }
        }

        return isQueenSafe;
    }

    private boolean isInBoardBounds(int row, int col, char[][] board) {
        return row >= 0 && row < board.length && col >= 0 && col < board[0].length;
    }

    private boolean isLastColumn(int col, Board board) {
        return col == board.getDimension();
    }
}
