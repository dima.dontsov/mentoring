package com.epam.training.exercise3.refactored.domain;

import com.epam.training.exercise3.refactored.AppConfig;

import java.util.Arrays;

/**
 * Chess Board.
 */
public final class Board {
    private final char[][] boardMatrix;

    private Board(char[][] boardMatrix) {
        this.boardMatrix = boardMatrix;
    }

    /**
     * Gets board instance by given dimension.
     *
     * @param dimension dimension
     * @return board
     */
    public static Board withDimension(int dimension) {
        char[][] result = new char[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            Arrays.fill(result[i], AppConfig.EMPTY_CELL);
        }
        return new Board(result);
    }

    /**
     * Gets board instance by given board matrix.
     *
     * @param boardMatrix boardMatrix
     * @return board
     */
    public static Board asBoard(char[][] boardMatrix) {
        return new Board(boardMatrix);
    }

    public char[][] getInnerBoard() {
        return boardMatrix;
    }

    /**
     * Gets matrix dimension.
     *
     * @return dimension
     */
    public int getDimension() {
        return boardMatrix.length;
    }

    /**
     * Gets board as matrix.
     *
     * @return board matrix
     */
    public char[][] asCharArray() {
        return this.getInnerBoard();
    }
}
