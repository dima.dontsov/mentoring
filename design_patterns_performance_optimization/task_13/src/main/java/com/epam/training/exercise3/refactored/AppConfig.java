package com.epam.training.exercise3.refactored;

/**
 * Main application config.
 */
public final class AppConfig {
    public static final char EMPTY_CELL = '.';
    public static final char QUEEN_CELL = 'q';
    public static final char DEFAULT_BOARD_SIZE = 8;
    private AppConfig() {

    }
}
