package com.epam.training.exercise1.refactored.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;


/**
 * Image loader.
 */
public class ImageLoader {

    /**
     * Loads image by url.
     *
     * @param url image url
     * @return BufferedImage if image exists, null otherwise
     */
    public BufferedImage loadImage(String url) {
        try {
            URL resource = getClass().getClassLoader().getResource(url);
            return ImageIO.read(resource);
        } catch (IOException exception) {
            throw new RuntimeException("File not found!", exception);
        }
    }
}
