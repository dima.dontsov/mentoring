package com.epam.training.exercise1.refactored.domain;

/**
 * Image to console transformer.
 * */
public class ConsoleImageTransformer {

    private static final char[] CHARS_BY_DARKNESS = {'#', '@', 'X', 'L', 'I', ':', '.', ' '};
    private static final int MIN_BOUND = 0;
    private static final int MAX_BOUND = 255 * 3;
    private static final int WIDTH_DELIMITER = 200;
    private static final int HEIGHT_DELIMITER = 60;

    /**
     * transforms given image to console.
     * @param image image
     * */
    public void transform(Image image) {
        Limit limit = Limit.newInstance().from(MIN_BOUND).to(MAX_BOUND).build();

        int stepWidth = image.getWidth() / WIDTH_DELIMITER;
        int stepHeight = image.getHeight() / HEIGHT_DELIMITER;

        Step step = Step.newInstance().ofWidth(stepWidth).ofHeight(stepHeight).build();

        transformToConsole(image, step, limit);
    }

    private void transformToConsole(Image image, Step step, Limit limit) {
        Limit workingLimit = limit;
        int stepWidth = step.getWidth();
        int stepHeight = step.getHeight();

        for (int y = 0; y < image.getHeight() - stepHeight; y += stepHeight) {
            for (int x = 0; x < image.getWidth() - stepWidth; x += stepWidth) {
                int sum = getResultingSum(image, step, new Coordinate(x, y));
                workingLimit = getNewLimit(workingLimit, sum);
                System.out.print(getChar(workingLimit, sum));
            }
            System.out.println();
        }
    }

    private int getResultingSum(Image image, Step step, Coordinate coordinate) {
        int sum = getSum(image, step, coordinate);
        return getSumStepCorrected(step, sum);
    }

    private char getChar(Limit limit, int sum) {
        int max = limit.getMax();
        int min = limit.getMin();
        int index = (sum - min) * CHARS_BY_DARKNESS.length / (max - min + 1);

        return CHARS_BY_DARKNESS[index];
    }

    private int getSum(Image image, Step step, Coordinate coordinate) {
        int sum = 0;
        for (int avgy = 0; avgy < step.getHeight(); avgy++) {
            for (int avgx = 0; avgx < step.getWidth(); avgx++) {
                sum = sum + (image.getIntensity(coordinate));
            }
        }
        return sum;
    }

    private int getSumStepCorrected(Step step, int initSum) {
        return initSum / step.getWidth() / step.getHeight();
    }

    private Limit getNewLimit(Limit limit, int sum) {
        int newMax = limit.getMax() < sum  ? sum : limit.getMax();
        int newMin = limit.getMin() > sum  ? sum : limit.getMin();

        return Limit.newInstance().from(newMin).to(newMax).build();
    }
}
