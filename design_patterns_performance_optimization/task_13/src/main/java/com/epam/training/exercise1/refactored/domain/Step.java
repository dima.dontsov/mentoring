package com.epam.training.exercise1.refactored.domain;

/**
 * Step with height and weight.
 */
public final class Step {
    private int width;
    private int height;

    private Step() {

    }

    /**
     * gets new instance.
     *
     * @return builder
     */
    public static Step.Builder newInstance() {
        return new Step().new Builder();
    }

    /**
     * gets step width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * gets step height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Step builder.
     */
    public class Builder {
        /**
         * sets width.
         *
         * @param width width
         * @return builder
         */
        public Step.Builder ofWidth(int width) {
            Step.this.width = width;
            return this;
        }

        /**
         * sets height.
         *
         * @param height height
         * @return builder
         */
        public Step.Builder ofHeight(int height) {
            Step.this.height = height;
            return this;
        }

        /**
         * builds Step instance.
         *
         * @return step
         */
        public Step build() {
            return Step.this;
        }
    }
}

